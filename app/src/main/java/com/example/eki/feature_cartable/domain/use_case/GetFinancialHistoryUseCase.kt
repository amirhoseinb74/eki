package com.example.eki.feature_cartable.domain.use_case

import com.example.eki.feature_main.domain.repository.RescuerRepository
import javax.inject.Inject

class GetFinancialHistoryUrlUseCase @Inject constructor(
    private val rescuerRepository: RescuerRepository,
) {
    suspend operator fun invoke(startDate: String, endDate: String): String {
        val encryptedPersonalId = rescuerRepository.getEncryptedPersonalIdFromDataStore()
        return "https://ets.096440.com/ords/f?p=145:2::NO:::P2_ENCRYPT,P2_FROM_DATE,P2_TO_DATE:" +
                "$encryptedPersonalId,${convertDateType(startDate)},${convertDateType(endDate)}"
    }

    private fun convertDateType(date: String) =
        date.split(" ")[0].replace("-", "/")

}