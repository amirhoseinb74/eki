package com.example.eki.feature_cartable.presentation

sealed class CartableScreens(val route: String) {
    object Cartable : CartableScreens("cartable_screen")
    object RescuerHistory : CartableScreens("rescuer_history_screen")
    object ShiftHistory : CartableScreens("shift_history_screen")
    object FinancialHistory : CartableScreens("financial_history_screen")
    object SaleHistory : CartableScreens("sale_history_screen")
    object FunctionalityHistory : CartableScreens("functionality_history_screen")
}
