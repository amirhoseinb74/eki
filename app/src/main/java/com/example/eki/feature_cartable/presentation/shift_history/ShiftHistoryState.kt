package com.example.eki.feature_cartable.presentation.shift_history

import com.example.eki.feature_cartable.domain.model.ShiftHistoryRes

data class ShiftHistoryState(
    val loading: Boolean = false,
    val data: List<ShiftHistoryRes>? = null,
)
