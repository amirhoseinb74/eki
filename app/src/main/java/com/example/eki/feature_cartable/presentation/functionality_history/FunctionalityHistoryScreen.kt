package com.example.eki.feature_cartable.presentation.functionality_history

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.eki.core.presentation.components.WebViewComposable
import com.example.eki.feature_cartable.presentation.common.CartableScreensCommon
import com.example.eki.ui.spacing

@Composable
fun FunctionalityHistoryScreen(
    navController: NavController,
    viewModel: FunctionalityHistoryViewModel = hiltViewModel(),
) {
    CartableScreensCommon(
        navController = navController,
        hasEndDate = false,
        onDateChanged = { date, _ -> viewModel.updateFunctionalityHistoryUrl(date) }) {
        WebViewComposable(
            modifier = Modifier
                .fillMaxSize()
                .padding(MaterialTheme.spacing.xxSmall),
            url = viewModel.url.value
        )
    }
}