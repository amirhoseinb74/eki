package com.example.eki.feature_cartable.presentation.sale_history

import androidx.lifecycle.LifecycleOwner

sealed class SaleHistoryEvent {
    data class DownloadFactor(
        val url: String,
        val id: String,
        val lifecycleOwner: LifecycleOwner,
    ) : SaleHistoryEvent()
}
