package com.example.eki.feature_cartable.domain.use_case

import com.example.eki.feature_main.domain.repository.RescuerRepository
import javax.inject.Inject

class GetFunctionalityHistoryUrlUseCase @Inject constructor(
    private val rescuerRepository: RescuerRepository,
) {
    suspend operator fun invoke(date: String): String {
        val encryptedPersonalId = rescuerRepository.getEncryptedPersonalIdFromDataStore()
        return "https://ets.096440.com/ords/f?p=145:4::NO:::P4_ENCRYPT,P4_FROM_DATE:" +
                "$encryptedPersonalId,$date"
    }
}