package com.example.eki.feature_cartable.presentation.sale_history

import android.net.Uri
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.core.net.toUri
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.R
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.Strings
import com.example.eki.core.util.downloader.DownloadFile
import com.example.eki.core.util.downloader.Downloader
import com.example.eki.core.util.downloader.FileType
import com.example.eki.feature_cartable.data.remote.dto.toSaleHistoryRes
import com.example.eki.feature_cartable.domain.use_case.GetSaleHistoryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SaleHistoryViewModel @Inject constructor(
    private val getSaleHistoryUseCase: GetSaleHistoryUseCase,
) : ViewModel() {

    private val _saleHistoryState = mutableStateOf(SaleHistoryState())
    val saleHistoryState: State<SaleHistoryState> = _saleHistoryState

    sealed class SaleHistoryUiEvent {
        data class ShowError(val message: String) : SaleHistoryUiEvent()
        data class OpenDownloadedFile(val downloadFile: DownloadFile, val uri: Uri) :
            SaleHistoryUiEvent()
    }

    private val _eventFlow = MutableSharedFlow<SaleHistoryUiEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private val _isDownloadingState = mutableStateOf(false)
    val isDownloadingState: State<Boolean> = _isDownloadingState

    fun onEvent(event: SaleHistoryEvent) {
        when (event) {
            is SaleHistoryEvent.DownloadFactor -> {
                downloadFile(event.url, event.id, event.lifecycleOwner)
            }
        }
    }

    private fun downloadFile(url: String, id: String, lifecycleOwner: LifecycleOwner) {
        val file = DownloadFile("Sale-Factor$id", FileType.PDF, url)
        Downloader.downloadFile(
            file = file,
            lifecycleOwner = lifecycleOwner,
            success = { uriString ->
                _isDownloadingState.value = false
                viewModelScope.launch {
                    _eventFlow.emit(SaleHistoryUiEvent.OpenDownloadedFile(file, uriString.toUri()))
                }
            }, failed = {
                _isDownloadingState.value = false
                viewModelScope.launch {
                    showErrorInUI(Strings.get(R.string.downloading_failed))
                }
            }, running = {
                _isDownloadingState.value = true
            }
        )
    }

    fun getSaleHistory(startDate: String, endDate: String) {
        viewModelScope.launch {
            getSaleHistoryUseCase(startDate, endDate).onEach { result ->
                _saleHistoryState.value = when (result) {
                    is ResponseHandler.Loading -> SaleHistoryState(true)

                    is ResponseHandler.Success -> {
                        SaleHistoryState(data = result.data?.map { it.toSaleHistoryRes() })
                    }

                    is ResponseHandler.Error -> {
                        showErrorInUI(result.message)
                        SaleHistoryState()
                    }
                }
            }.launchIn(viewModelScope)
        }
    }

    private suspend fun showErrorInUI(message: String?) {
        _eventFlow.emit(
            SaleHistoryUiEvent.ShowError(
                message ?: ResponseHandler.generalErrorMessage
            )
        )
    }
}