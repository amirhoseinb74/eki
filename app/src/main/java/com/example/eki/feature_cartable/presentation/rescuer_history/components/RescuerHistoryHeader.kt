package com.example.eki.feature_cartable.presentation.rescuer_history.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.outlined.KeyboardArrowDown
import androidx.compose.material.icons.outlined.KeyboardArrowUp
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
fun RescuerHistoryHeader(
    modifier: Modifier = Modifier,
    header: String,
    expanded: Boolean = false,
) {
    Box(
        modifier = modifier
            .fillMaxWidth()
            .background(MaterialTheme.colorScheme.surface.copy(blue = 0.45f))
            .padding(
                horizontal = MaterialTheme.spacing.small,
                vertical = MaterialTheme.spacing.xSmall
            )
    ) {
        val contentColor = MaterialTheme.colorScheme.primaryContainer
        Text(
            modifier = Modifier.align(Alignment.Center),
            text = header,
            style = MaterialTheme.typography.headlineSmall.copy(contentColor),
            textAlign = TextAlign.Center
        )
        val imageVector =
            if (expanded) Icons.Outlined.KeyboardArrowUp else Icons.Outlined.KeyboardArrowDown
        Icon(
            modifier = Modifier.align(Alignment.CenterEnd),
            imageVector = imageVector,
            contentDescription = null,
            tint = contentColor
        )
    }
    VerticalSpacer(height = 10)
}

@Preview
@Composable
fun RescuerHistoryHeaderPreview() {
    EKITheme {
        RescuerHistoryHeader(header = "بالینی")
    }
}

