package com.example.eki.feature_cartable.presentation.common

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.eki.core.presentation.components.ScaffoldState
import com.example.eki.core.util.calender.CalenderUtils
import com.example.eki.core.util.calender.CalenderUtils.updateController
import com.example.eki.feature_cartable.presentation.common.bottom_navigation.CartableBottomNavigation
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme
import com.razaghimahdi.compose_persian_date.core.PersianDatePickerController

@Preview
@Composable
fun CartableScreensCommon(
    navController: NavController = rememberNavController(),
    hasEndDate: Boolean = true,
    onDateChanged: (startDate: String, endDate: String) -> Unit = { _, _ -> },
    content: @Composable (showMessage: (String) -> Unit) -> Unit = {},
) {
    EKITheme {
        var showDatePicker by rememberSaveable {
            mutableStateOf(false)
        }
        var isStartDate by rememberSaveable {
            mutableStateOf(true)
        }

        val startController by rememberSaveable(saver = CalenderUtils.persianDatePickerSaver) {
            mutableStateOf(PersianDatePickerController())
        }
        val endController by rememberSaveable(saver = CalenderUtils.persianDatePickerSaver) {
            mutableStateOf(PersianDatePickerController())
        }

        val startDateToShow = remember {
            mutableStateOf(startController.getPersianFullDate())
        }
        val endDateToShow = remember {
            mutableStateOf(endController.getPersianFullDate())
        }

        var startDateToPass by remember {
            mutableStateOf(CalenderUtils.getDateAndTime(startController))
        }

        var endDateToPass by remember {
            mutableStateOf(CalenderUtils.getDateAndTime(endController, "23:59"))
        }
        val backgroundColor = MaterialTheme.colorScheme.surface

        LaunchedEffect(key1 = startDateToPass, key2 = endDateToPass) {
            onDateChanged(startDateToPass, endDateToPass)
        }

        ScaffoldState(bottomBar = { CartableBottomNavigation(navController = navController) }) { showSnackbar ->
            val verticalGradient = Brush.verticalGradient(
                listOf(
                    backgroundColor.copy(blue = 0.9f),
                    backgroundColor.copy(blue = 0.6f)
                )
            )
            Box(
                modifier = Modifier
                    .fillMaxSize()
                    .background(brush = verticalGradient)
            ) {
                Column {
                    DateSelector(
                        modifier = Modifier.padding(MaterialTheme.spacing.small),
                        selectedStartDate = startDateToShow,
                        hasEndDate = hasEndDate,
                        selectedEndDate = endDateToShow,
                        onStartSelect = {
                            showDatePicker = true
                            isStartDate = true
                        },
                        onEndSelect = {
                            showDatePicker = true
                            isStartDate = false
                        }
                    )
                    content { message -> showSnackbar(message) }
                }
                if (showDatePicker) {
                    if (isStartDate) {
                        PersianDatePicker(
                            controller = startController,
                            onSubmit = { controller ->
                                showDatePicker = false
                                startController.updateController(controller)
                                startDateToShow.value = startController.getPersianFullDate()
                                startDateToPass = CalenderUtils.getDateAndTime(startController)
                            }) { year, month, day ->

                            startController.updateDate(year, month, day)

                        }

                    } else {
                        PersianDatePicker(
                            controller = endController,
                            onSubmit = { controller ->
                                showDatePicker = false
                                endController.updateController(controller)
                                endDateToShow.value = endController.getPersianFullDate()
                                endDateToPass = CalenderUtils.getDateAndTime(
                                    endController,
                                    "23:59"
                                )
                            }
                        ) { year, month, day ->
                            endController.updateDate(year, month, day)
                        }
                    }
                }
            }
        }
    }
}