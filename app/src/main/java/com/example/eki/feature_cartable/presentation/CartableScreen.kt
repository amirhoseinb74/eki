/*
package com.example.eki.feature_cartable.presentation

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import com.example.eki.core.presentation.components.ScaffoldState
import com.example.eki.core.util.calender.CalenderUtils
import com.example.eki.core.util.calender.CalenderUtils.updateController
import com.example.eki.feature_cartable.presentation.common.DateSelector
import com.example.eki.feature_cartable.presentation.common.PersianDatePicker
import com.example.eki.feature_cartable.presentation.common.bottom_navigation.CartableBottomNavItem
import com.example.eki.feature_cartable.presentation.common.bottom_navigation.CartableBottomNavigation
import com.example.eki.ui.spacing
import com.razaghimahdi.compose_persian_date.core.rememberPersianDatePicker

@Composable
fun CartableScreen(navController: NavController) {

}


@Preview
@Composable
fun CartableScreenContent() {
    var hasEndDate by remember {
        mutableStateOf(true)
    }
    var navItem by remember {
        mutableStateOf<CartableBottomNavItem>(CartableBottomNavItem.RescuerHistory)
    }.also { hasEndDate = !it.equals(CartableBottomNavItem.Functionality) }

    var showDatePicker by remember {
        mutableStateOf(false)
    }

    var isStartDate by remember {
        mutableStateOf(true)
    }

    val startController = rememberPersianDatePicker()
    val endController = rememberPersianDatePicker()

    val startDateToShow = rememberSaveable {
        mutableStateOf(startController.getPersianFullDate())
    }
    val endDateToShow = rememberSaveable {
        mutableStateOf(endController.getPersianFullDate())
    }

    var startDateToPass by rememberSaveable {
        mutableStateOf(CalenderUtils.getDateAndTime(startController))
    }

    var endDateToPass by rememberSaveable {
        mutableStateOf(CalenderUtils.getDateAndTime(endController, "23:59"))
    }
    val backgroundColor = MaterialTheme.colorScheme.surface
    ScaffoldState(bottomBar = {
        CartableBottomNavigation(
            handleOnclick = true
        ) { bottomNavItem ->
            navItem = bottomNavItem
        }
    }) {
        Box(
            modifier = Modifier
                .fillMaxSize()
                .background(
                    brush = Brush.verticalGradient(
                        listOf(
                            backgroundColor.copy(blue = 0.6f),
                            backgroundColor.copy(blue = 0.4f)
                        )
                    )
                )
        ) {
            Column {
                DateSelector(
                    modifier = Modifier.padding(MaterialTheme.spacing.small),
                    selectedStartDate = startDateToShow,
                    hasEndDate = hasEndDate,
                    selectedEndDate = endDateToShow,
                    onStartSelect = {
                        showDatePicker = true
                        isStartDate = true
                    },
                    onEndSelect = {
                        showDatePicker = true
                        isStartDate = false
                    }
                )
                when (navItem) {
                    CartableBottomNavItem.RescuerHistory -> {

                    }

                    CartableBottomNavItem.Financial -> {

                    }

                    CartableBottomNavItem.Functionality -> {

                    }

                    CartableBottomNavItem.Home -> {

                    }

                    CartableBottomNavItem.Sale -> {

                    }

                    CartableBottomNavItem.Shift -> {

                    }
                }
            }
            if (showDatePicker) {
                PersianDatePicker(
                    controller =  rememberPersianDatePicker(),
                    onSubmit = {
                        showDatePicker = false
                        if (isStartDate) {
                            startDateToShow.value = startController.getPersianFullDate()
                            startDateToPass = CalenderUtils.getDateAndTime(startController)
                        } else {
                            endDateToShow.value = endController.getPersianFullDate()
                            endDateToPass = CalenderUtils.getDateAndTime(endController, "23:59")
                        }
                    }) { year, month, day ->
                    if (isStartDate) {
                        startController.updateDate(year, month, day)
                    } else {
                        endController.updateDate(year, month, day)
                    }
                }
            }
        }

    }
}

*/
