package com.example.eki.feature_cartable.presentation.common

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.eki.R
import com.example.eki.core.presentation.components.HorizontalSpacer
import com.example.eki.core.presentation.components.RowOrColumn
import com.example.eki.core.presentation.components.WindowInfo
import com.example.eki.core.presentation.components.rememberWindowInfo
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme
import com.razaghimahdi.compose_persian_date.core.rememberPersianDatePicker

@Composable
@Preview(device = Devices.PHONE)
fun DateSelector(
    modifier: Modifier = Modifier,
    hasEndDate: Boolean = true,
    selectedStartDate: State<String>? = null,
    selectedEndDate: State<String>? = null,
    onStartSelect: () -> Unit = {},
    onEndSelect: () -> Unit = {},
) {

    EKITheme {
        val isVertical = rememberWindowInfo().screenWidthInfo == WindowInfo.WindowType.Compact
        val currentDate = rememberPersianDatePicker().getPersianFullDate()
        var today = stringResource(id = R.string.today)
        today += "  ($currentDate)"
        val showTodayForStart = selectedStartDate?.value?.let { it == currentDate } ?: false
        val showTodayForEnd = selectedEndDate?.value?.let { it == currentDate } ?: false


        RowOrColumn(
            isVertical = isVertical,
            modifier = modifier
                .fillMaxWidth()
                .background(
                    MaterialTheme.colorScheme.primaryContainer,
                    shape = MaterialTheme.shapes.extraLarge
                )
                .padding(MaterialTheme.spacing.small)
                .padding(start = MaterialTheme.spacing.medium, end = MaterialTheme.spacing.medium)
                .height(IntrinsicSize.Min),
            horizontalArrangement = if (hasEndDate) Arrangement.SpaceBetween else Arrangement.Start,
            verticalAlignment = Alignment.CenterVertically,
            verticalArrangement = Arrangement.SpaceEvenly,
            horizontalAlignment = Alignment.Start
        ) {
            Row(modifier = Modifier.clickable { onStartSelect() }) {
                Icon(
                    painter = painterResource(id = R.drawable.ic_calendar),
                    contentDescription = "calender",
                    tint = MaterialTheme.colorScheme.onPrimaryContainer
                )
                HorizontalSpacer(width = 8)
                Text(
                    text = if (hasEndDate) stringResource(R.string.form_date)
                    else stringResource(id = R.string.date),
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.onPrimaryContainer
                )
                HorizontalSpacer(width = 8)
                Text(
                    text = if (!showTodayForStart) selectedStartDate?.value ?: today else today,
                    style = MaterialTheme.typography.bodyMedium,
                    color = MaterialTheme.colorScheme.onPrimaryContainer
                )
            }
            if (hasEndDate) {

                Divider(
                    modifier = if (isVertical) {
                        Modifier.padding(vertical = MaterialTheme.spacing.medium)
                    } else {
                        Modifier
                            .width(1.dp)
                            .fillMaxHeight()
                    }, color = MaterialTheme.colorScheme.onPrimaryContainer
                )

                Row(modifier = Modifier.clickable { onEndSelect() }) {
                    Icon(
                        painter = painterResource(id = R.drawable.ic_calendar),
                        contentDescription = "calender",
                        tint = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                    HorizontalSpacer(width = 8)
                    Text(
                        text = stringResource(id = R.string.to_date),
                        style = MaterialTheme.typography.bodyMedium,
                        color = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                    HorizontalSpacer(width = 8)
                    Text(
                        text = if (!showTodayForEnd) selectedEndDate?.value ?: today else today,
                        style = MaterialTheme.typography.bodyMedium,
                        color = MaterialTheme.colorScheme.onPrimaryContainer
                    )
                }
            }
        }

    }
}
