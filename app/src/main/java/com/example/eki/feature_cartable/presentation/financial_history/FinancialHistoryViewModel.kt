package com.example.eki.feature_cartable.presentation.financial_history

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.feature_cartable.domain.use_case.GetFinancialHistoryUrlUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FinancialHistoryViewModel @Inject constructor(
    private val getFinancialHistoryUrlUseCase: GetFinancialHistoryUrlUseCase,
) : ViewModel() {

    private val _url = mutableStateOf("")
    val url: State<String> = _url

    fun updateFinancialHistoryUrl(startDate: String, endDate: String) {
        viewModelScope.launch {
            _url.value = getFinancialHistoryUrlUseCase(startDate, endDate)
        }
    }
}