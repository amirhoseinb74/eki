package com.example.eki.feature_cartable.domain.repository

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.dto.InputDataConfig
import com.example.eki.feature_cartable.data.remote.dto.RescuerHistoryResDto
import com.example.eki.feature_cartable.data.remote.dto.SaleHistoryResDto
import com.example.eki.feature_cartable.data.remote.dto.ShiftHistoryResDto

interface CartableRepository {

    suspend fun getRescuerHistory(
        inputDataConfig: InputDataConfig,
        token: String,
    ): BaseResponse<RescuerHistoryResDto>

    suspend fun getShiftHistory(
        inputDataConfig: InputDataConfig,
        token: String,
    ): BaseResponse<ShiftHistoryResDto>

    suspend fun getSaleHistory(
        inputDataConfig: InputDataConfig,
        token: String,
    ): BaseResponse<SaleHistoryResDto>
}