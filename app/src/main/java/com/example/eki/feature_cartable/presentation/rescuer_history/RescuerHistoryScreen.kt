package com.example.eki.feature_cartable.presentation.rescuer_history

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.eki.core.presentation.components.animation.LoadingAnimation
import com.example.eki.core.presentation.components.animation.NoDataAnimation
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_cartable.domain.model.RescuerHistoryRes
import com.example.eki.feature_cartable.presentation.common.CartableScreensCommon
import com.example.eki.feature_cartable.presentation.rescuer_history.components.GroupedRescuerHistoryItems
import kotlinx.coroutines.flow.collectLatest

@Composable
fun RescuerHistoryScreen(
    navController: NavController,
    viewModel: RescuerHistoryViewModel = hiltViewModel(),
) {
    RescuerHistoryScreenContent(
        navController = navController,
        onDateChanged = viewModel::getRescuerHistory,
        rescuerHistoryState = viewModel.rescuerHistoryState.value
    ) { showMessage ->
        LaunchedEffect(key1 = true) {
            viewModel.eventFlow.collectLatest { rescuerHistoryUIEvent ->
                when (rescuerHistoryUIEvent) {
                    is RescuerHistoryViewModel.RescuerHistoryUIEvent.ShowError -> {
                        showMessage(rescuerHistoryUIEvent.message)
                    }
                }.exhaustive
            }
        }
    }
}

@Preview(device = Devices.PIXEL_3)
@Composable
fun RescuerHistoryScreenContent(
    navController: NavController = rememberNavController(),
    onDateChanged: (startDate: String, endDate: String) -> Unit = { _, _ -> },
    rescuerHistoryState: RescuerHistoryState = RescuerHistoryState(
        loading = false,
        data = RescuerHistoryRes.testData
    ),
    showSnackbar: @Composable (launch: (String) -> Unit) -> Unit = {},
) {
    CartableScreensCommon(
        navController = navController, hasEndDate = true, onDateChanged = onDateChanged
    ) { showMessage ->
        rescuerHistoryState.data?.let { rescuerHistoryResList ->
            GroupedRescuerHistoryItems(rescuerHistoryResList = rescuerHistoryResList)
        }
        showSnackbar(showMessage)
    }

    if (rescuerHistoryState.loading) {
        LoadingAnimation()
    }
    if (rescuerHistoryState.data?.isEmpty() == true) {
        NoDataAnimation()
    }
}