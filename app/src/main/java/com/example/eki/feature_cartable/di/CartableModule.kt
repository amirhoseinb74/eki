package com.example.eki.feature_cartable.di

import com.example.eki.core.di.FakeRepository
import com.example.eki.feature_cartable.data.remote.CartableApi
import com.example.eki.feature_cartable.data.repository.CartableRepositoryImpl
import com.example.eki.feature_cartable.data.repository.fake.FakeCartableRepo
import com.example.eki.feature_cartable.domain.repository.CartableRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object CartableModule {

    @Provides
    @Singleton
    fun provideCartableApi(retrofit: Retrofit): CartableApi =
        retrofit.create(CartableApi::class.java)

    @Provides
    @Singleton
    fun provideCartableRepository(cartableApi: CartableApi): CartableRepository =
        CartableRepositoryImpl(cartableApi)

    @FakeRepository
    @Provides
    @Singleton
    fun provideFakeCartableRepository(): CartableRepository = FakeCartableRepo()
}