package com.example.eki.feature_cartable.presentation.rescuer_history.components

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateMapOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.feature_cartable.domain.model.RescuerHistoryRes
import com.example.eki.ui.spacing

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun GroupedRescuerHistoryItems(
    rescuerHistoryResList: List<RescuerHistoryRes>,
) {
    val groupExpandedState = remember { mutableStateMapOf<String, Boolean>() }

    val groupedItems = rescuerHistoryResList.groupBy { it.rescuerWorkOrderType }

    LazyColumn(
        modifier = Modifier
            .wrapContentSize()
            .padding(MaterialTheme.spacing.small)
    ) {
        groupedItems.forEach { (type, rescuerHistoryList) ->
            val isGroupExpanded = groupExpandedState.getOrPut(type.name) { true }

            stickyHeader {
                RescuerHistoryHeader(
                    modifier = Modifier.clickable {
                        groupExpandedState[type.name] = !isGroupExpanded
                    },
                    header = type.pName,
                    expanded = isGroupExpanded
                )
            }

            if (isGroupExpanded) {
                items(rescuerHistoryList) { rescuerHistory ->
                    RescuerHistoryItem(item = rescuerHistory)
                    VerticalSpacer(10)
                }
            }
        }
    }
}
