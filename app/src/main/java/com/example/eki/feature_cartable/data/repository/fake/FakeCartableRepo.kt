package com.example.eki.feature_cartable.data.repository.fake

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.Settings
import com.example.eki.core.data.remote.dto.InputDataConfig
import com.example.eki.feature_cartable.data.remote.dto.RescuerHistoryResDto
import com.example.eki.feature_cartable.data.remote.dto.SaleHistoryResDto
import com.example.eki.feature_cartable.data.remote.dto.ShiftHistoryResDto
import com.example.eki.feature_cartable.domain.repository.CartableRepository

class FakeCartableRepo : CartableRepository {
    override suspend fun getRescuerHistory(
        inputDataConfig: InputDataConfig,
        token: String,
    ): BaseResponse<RescuerHistoryResDto> {
        return BaseResponse(
            data = listOf(
                RescuerHistoryResDto(
                    "",
                    "",
                    "",
                    "سفيد",
                    "ايران81 393ب37",
                    "هايما S7 اتوماتيك توربو شارژ",
                    "-",
                    "1",
                    "خاتمه يافته",
                    "امداد (خاتمه يافته)",
                    "768080808080808180o",
                    "c4163fdc7d86157976o",
                    "1402/06/05 15:11:08",
                    "484QT6B001061A",
                    "0",
                    "",
                    "ندارد",
                    "1",
                    "-",
                    "33583709",
                    "بلي",
                    "54425",
                    "4451e01b023488a883o",
                    "0",
                    "0",
                    "ايران81 393ب37",
                    "",
                    "30342025",
                    "5a2883127214eebb2bo",
                    "اقدام شده",
                    "1402/06/05 15:09:22",
                    "77a8f38ae9d410495co",
                    "امدادسیار اربعین",
                    "2",
                    "0",
                    "1402/06/05 15:09:45",
                    "2",
                    "1398/02/19",
                    "فعال",
                    "9605219784",
                    "ایلام, ایلام, مرکزی, ایلام, خ. شهدا، نرسیده به م. خیام، خ. بسطامی، ک. جیران علیدادی",
                    "معتمدی  معتمدی",
                    "NAAD391Z8HY685312",
                    "09128632213",
                    "51801",
                    "1402/06/05 15:11:08",
                    "",
                    "[]",
                    "1402/06/05 15:09:45",
                    "-",
                    1,
                    "-"
                )
            ),
            settings = Settings("ok", "1")
        )
    }

    override suspend fun getShiftHistory(
        inputDataConfig: InputDataConfig,
        token: String,
    ): BaseResponse<ShiftHistoryResDto> {
        return BaseResponse(data = emptyList(), Settings("ok", "1"))
    }

    override suspend fun getSaleHistory(
        inputDataConfig: InputDataConfig,
        token: String,
    ): BaseResponse<SaleHistoryResDto> {
        return BaseResponse(data = emptyList(), Settings("ok", "1"))
    }
}