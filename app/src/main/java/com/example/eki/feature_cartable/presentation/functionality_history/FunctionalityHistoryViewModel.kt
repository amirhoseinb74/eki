package com.example.eki.feature_cartable.presentation.functionality_history

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.feature_cartable.domain.use_case.GetFunctionalityHistoryUrlUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class FunctionalityHistoryViewModel @Inject constructor(
    private val getFunctionalityHistoryUrlUseCase: GetFunctionalityHistoryUrlUseCase,
) : ViewModel() {
    private val _url = mutableStateOf("")
    val url: State<String> = _url

    fun updateFunctionalityHistoryUrl(date: String) {
        viewModelScope.launch {
            _url.value = getFunctionalityHistoryUrlUseCase(date)
        }
    }
}