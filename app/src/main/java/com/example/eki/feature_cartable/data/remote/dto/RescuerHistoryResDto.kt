package com.example.eki.feature_cartable.data.remote.dto

import com.example.eki.core.util.safeLet
import com.example.eki.feature_cartable.domain.model.RescuerHistoryRes
import com.example.eki.feature_cartable.domain.model.RescuerWorkOrderType
import com.example.eki.feature_main.presentation.home.WorkOrderStatus
import com.google.gson.annotations.SerializedName

data class RescuerHistoryResDto(
    val agent: Any,
    val agentLargeId: Any,
    val amountDamagePrice: Any,
    val carColor: String?,
    val carPlaque: String?,
    val carType: String?,
    val description: String?,
    val emdadKind: String?,
    val emdadStatus: String?,
    val emdadType: String?,
    val emdadTypeId: String?,
    val emdadgarId: String?,
    val endTime: String?,
    val enginNo: String?,
    val epoll: String?,
    val financeStatus: String?,
    val guaranteeStatus: String?,
    @SerializedName("hasdoc")
    val hasDoc: String?,
    val helpPrice: String?,
    val id: String?,
    val isSubscribe: String?,
    val kilometer: String?,
    val largeId: String?,
    val lat: String?,
    val lng: String?,
    val pelakNo: String?,
    val repairTime: Any,
    val rokhdad: String?,
    val rokhdadLargeId: String?,
    val rokhdadStatus: String?,
    val sendDate: String?,
    val serviceId: String?,
    val serviceLevel: String?,
    val serviceType: String?,
    val specialServiceId: String?,
    val startTime: String?,
    val status: String?,
    val subscribeExpireDate: String?,
    val subscribeStatus: String?,
    val subscriber: String?,
    val subscriberAddress: String?,
    val subscriberName: String?,
    val subscriberShasi: String?,
    val subscriberTel: String?,
    val tipId: String?,
    val workEndDate: String?,
    val workImages: Any,
    val workImagesStr: String?,
    val workStartDate: String?,
    val workTypeIcon: String?,
    val workTypeId: Int?,
    val workTypeName: String?,
)

/**
 * Converts a [RescuerHistoryResDto] object to a [RescuerHistoryRes] object
 * @param hasDoc indicates that the work order can has a button for finance production
 * @param epoll the flag indicating whether the survey is done, 1 for done and 0 for not done
 * @return a RescuerHistoryRes object with the corresponding fields
 */
fun RescuerHistoryResDto.toRescuerHistoryRes() =
    RescuerHistoryRes(
        eventCarId = rokhdad,
        helpId = id,
        workTypeId = workTypeId,
        workOrderDate = workStartDate ?: sendDate,
        workTypeName = workTypeName,
        emdadType = emdadType,
        lastKilometer = kilometer,
        startDateTime = startTime ?: workStartDate ?: sendDate,
        endDateTime = endTime,
        description = description,
        pieces = "",
        wages = "",
        details = helpPrice,
        showFinanceButton = safeLet(status, hasDoc) { status, hasDoc ->
            val workOrderStatus = WorkOrderStatus.fromString(status)
            hasDoc == "1" && (workOrderStatus == WorkOrderStatus.FINISHED ||
                    workOrderStatus == WorkOrderStatus.CANCELED)
        } ?: false,
        showSurveyButton = epoll?.let { it == "0" } ?: false,
        rescuerWorkOrderType = workTypeId?.let {
            if (it == 8) RescuerWorkOrderType.CARRY else RescuerWorkOrderType.CLINICAL
        } ?: RescuerWorkOrderType.CLINICAL
    )