package com.example.eki.feature_cartable.presentation.rescuer_history.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.core.presentation.components.HorizontalSpacer
import com.example.eki.core.presentation.components.RowOrColumn
import com.example.eki.core.presentation.components.TitleValueItem
import com.example.eki.core.presentation.components.WindowInfo
import com.example.eki.core.presentation.components.rememberWindowInfo
import com.example.eki.feature_cartable.domain.model.RescuerHistoryRes
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RescuerHistoryItem(
    item: RescuerHistoryRes,
    onClickInquiry: () -> Unit = {},
    onClickDebtRegistration: () -> Unit = {},
    onClickSurvey: () -> Unit = {},
    onClickProduceFinancialDocument: () -> Unit = {},
) {
    var referenceNumber: Long? by rememberSaveable {
        mutableStateOf(null)
    }
    val isCompactWidth = rememberWindowInfo().screenWidthInfo == WindowInfo.WindowType.Compact
    ElevatedCard(
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        )
    ) {
        val containerColor = MaterialTheme.colorScheme.surface
        val contentColor = MaterialTheme.colorScheme.onSurface
        item.apply {
            TitleValueItem(title = stringResource(R.string.event_car_id), value = eventCarId)
            TitleValueItem(title = stringResource(R.string.help_id), value = helpId)
            TitleValueItem(title = stringResource(R.string.emdad_type), value = emdadType)
            TitleValueItem(title = stringResource(R.string.work_order_date), value = workOrderDate)
            TitleValueItem(title = stringResource(R.string.last_kilometer), value = lastKilometer)
            TitleValueItem(title = stringResource(R.string.start_date_time), value = startDateTime)
            TitleValueItem(title = stringResource(R.string.end_date_time), value = endDateTime)
            TitleValueItem(title = stringResource(R.string.description), value = description)
            TitleValueItem(title = stringResource(R.string.pieces), value = pieces)
            TitleValueItem(title = stringResource(R.string.wages), value = wages)
            TitleValueItem(title = stringResource(R.string.details), value = details)
        }
        RowOrColumn(isVertical = isCompactWidth, verticalAlignment = Alignment.CenterVertically) {
            LazyRow(
                Modifier
                    .padding(
                        horizontal = MaterialTheme.spacing.small,
                        vertical = MaterialTheme.spacing.medium
                    ), verticalAlignment = Alignment.CenterVertically,
                horizontalArrangement = Arrangement.Start
            ) {
                item {
                    CustomButton(
                        text = stringResource(R.string.inquiry),
                        containerColor = containerColor,
                        contentColor = contentColor,
                        onClick = onClickInquiry
                    )
                    HorizontalSpacer(width = 10)
                    val refBgColor = MaterialTheme.colorScheme.background
                    val refContentColor = MaterialTheme.colorScheme.onBackground
                    OutlinedTextField(
                        modifier = Modifier
                            .height(MaterialTheme.spacing.xLarge)
                            .width(MaterialTheme.spacing.xxxLarge),
                        value = referenceNumber?.toString().orEmpty(),
                        onValueChange = { input ->
                            if (input.length <= 15) {
                                referenceNumber = input.toLongOrNull()
                            }
                        },
                        label = {
                            Text(
                                text = stringResource(id = R.string.reference_number),
                                style = MaterialTheme.typography.bodyMedium,
                                color = refContentColor
                            )
                        },
                        shape = MaterialTheme.shapes.small,
                        textStyle = MaterialTheme.typography.bodyMedium.copy(color = refContentColor),
                        colors = TextFieldDefaults.outlinedTextFieldColors(
                            containerColor = refBgColor,
                            textColor = refContentColor,
                            placeholderColor = refContentColor,
                            unfocusedLabelColor = refContentColor,
                            focusedLabelColor = refContentColor,
                            cursorColor = refContentColor,
                            unfocusedBorderColor = refBgColor,
                            focusedBorderColor = refContentColor
                        ),
                        keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
                    )

                }
            }
            LazyRow(
                modifier = Modifier
                    .padding(horizontal = MaterialTheme.spacing.small),
                verticalAlignment = Alignment.CenterVertically
            ) {

                item {
                    if (item.showFinanceButton) {
                        CustomButton(
                            text = stringResource(id = R.string.produce_financial_document),
                            containerColor = containerColor,
                            contentColor = contentColor,
                            onClick = onClickProduceFinancialDocument
                        )
                        HorizontalSpacer(width = 7)
                    }
                    CustomButton(
                        text = stringResource(id = R.string.debt_registration),
                        containerColor = containerColor,
                        contentColor = contentColor,
                        onClick = onClickDebtRegistration
                    )
                    if (item.showSurveyButton) {
                        HorizontalSpacer(width = 7)
                        CustomButton(
                            text = stringResource(id = R.string.survey),
                            containerColor = containerColor,
                            contentColor = contentColor,
                            onClick = onClickSurvey
                        )
                    }
                }
            }

        }

    }
}


@Preview
@Composable
fun RescuerHistoryItemPreview() {
    EKITheme {
        RescuerHistoryItem(
            RescuerHistoryRes(
                "30342025",
                "33583709",
                1,
                "1402/06/05 15:09:45",
                "-",
                "امداد (خاتمه يافته)",
                "54425",
                "1402/06/05 15:09:45",
                "1402/06/05 15:11:08",
                "-",
                "-",
                "-",
                "",
                showFinanceButton = true,
                showSurveyButton = false,
            )
        )
    }
}