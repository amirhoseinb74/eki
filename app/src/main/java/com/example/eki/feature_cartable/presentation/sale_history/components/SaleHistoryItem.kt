package com.example.eki.feature_cartable.presentation.sale_history.components

import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountBox
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.core.presentation.components.RowOrColumn
import com.example.eki.core.presentation.components.TitleValueItem
import com.example.eki.core.presentation.components.WindowInfo
import com.example.eki.core.presentation.components.rememberWindowInfo
import com.example.eki.core.util.Constants
import com.example.eki.feature_cartable.domain.model.SaleHistoryRes
import com.example.eki.ui.theme.EKITheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun SaleHistoryItem(
    modifier: Modifier = Modifier,
    item: SaleHistoryRes,
    onClickFactor: (url: String, id: String) -> Unit = { _, _ -> },
) {
    val isCompactWidth = rememberWindowInfo().screenWidthInfo == WindowInfo.WindowType.Compact
    ElevatedCard(
        modifier = modifier,
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.primaryContainer
        )
    ) {
        val containerColor = MaterialTheme.colorScheme.surface
        val contentColor = MaterialTheme.colorScheme.onSurface
        val icon = Icons.Rounded.AccountBox
        item.apply {

            RowOrColumn(
                isVertical = isCompactWidth,
                modifier = Modifier.height(IntrinsicSize.Min)
            ) {
                TitleValueItem(
                    modifier = Modifier.weight(1f),
                    title = stringResource(R.string.customer_name),
                    icon = icon,
                    value = customerName
                )
                TitleValueItem(
                    modifier = Modifier.weight(1f),
                    title = stringResource(R.string.factor_number), icon = icon,
                    value = factorNumber.toString()
                )
            }
            RowOrColumn(
                isVertical = isCompactWidth,
                modifier = Modifier.height(IntrinsicSize.Min)
            ) {
                TitleValueItem(
                    modifier = Modifier.weight(1f),
                    title = stringResource(R.string.total_price), icon = icon,
                    value = totalPrice.toString()
                )

                TitleValueItem(
                    modifier = Modifier.weight(1f),
                    title = stringResource(R.string.sale_date),
                    icon = icon,
                    value = saleDate
                )
            }
            factorUrl?.let { url ->
                CustomButton(
                    modifier = Modifier
                        .fillMaxWidth()
                        .wrapContentSize(),
                    text = stringResource(id = R.string.factor),
                    containerColor = containerColor,
                    contentColor = contentColor,
                    onClick = { onClickFactor(url, item.factorNumber.toString()) })
            }
        }
    }
}

@Preview(device = Devices.PHONE)
@Composable
fun PreviewSaleHistoryItem() {
    EKITheme {
        SaleHistoryItem(
            item = SaleHistoryRes(
                "رضا انصاری",
                12345,
                678910,
                "1402/23/23",
                "${Constants.BASE_URL}app/printSaleFactor?id=tra7FWIe9+e8j8fjRzicHw=="
            )
        )
    }
}