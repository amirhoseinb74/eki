package com.example.eki.feature_cartable.data.remote.dto

import com.example.eki.feature_cartable.domain.model.AbsenceType
import com.example.eki.feature_cartable.domain.model.ShiftHistoryRes

data class ShiftHistoryResDto(
    val absenceType: String?,
    val fromTime: String?,
    val helperId: String?,
    val helperLargeId: String?,
    val id: String?,
    val presence: String?,
    val regionName: String?,
    val shiftDate: String?,
    val shiftType: String?,
    val toTime: String?,
)

fun ShiftHistoryResDto.toShiftHistroyRes() = ShiftHistoryRes(
    turnTime = "$fromTime - $toTime",
    shiftType = shiftType ?: "",
    presence = presence ?: "",
    regionName = regionName ?: "",
    shiftDate = shiftDate ?: "",
    absenceType = absenceType?.let { absenceType ->
        when (absenceType) {
            "1" -> {
                AbsenceType.ABSENCE
            }

            "2" -> {
                AbsenceType.IN_VACATION
            }

            else -> {
                AbsenceType.NOT_ABSENCE
            }
        }
    } ?: AbsenceType.NOT_ABSENCE
)