package com.example.eki.feature_cartable.presentation.shift_history

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_cartable.data.remote.dto.toShiftHistroyRes
import com.example.eki.feature_cartable.domain.use_case.GetShiftHistoryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShiftHistoryViewModel @Inject constructor(
    private val getShiftHistoryUseCase: GetShiftHistoryUseCase,
) : ViewModel() {
    sealed class ShiftHistoryUIEvent {
        data class ShowError(val message: String) : ShiftHistoryUIEvent()
    }

    private val _eventFlow = MutableSharedFlow<ShiftHistoryUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private val _shiftHistoryState = mutableStateOf(ShiftHistoryState())
    val shiftHistoryState: State<ShiftHistoryState> = _shiftHistoryState

    fun getShiftHistory(startDate: String, endDate: String) {
        viewModelScope.launch {
            getShiftHistoryUseCase(startDate, endDate).onEach { result ->
                _shiftHistoryState.value = when (result) {
                    is ResponseHandler.Loading ->
                        ShiftHistoryState(loading = true)

                    is ResponseHandler.Success -> {
                        ShiftHistoryState(data = result.data?.map { it.toShiftHistroyRes() })
                    }

                    is ResponseHandler.Error -> {
                        _eventFlow.emit(
                            ShiftHistoryUIEvent.ShowError(
                                result.message ?: ResponseHandler.generalErrorMessage
                            )
                        )
                        ShiftHistoryState()
                    }
                }.exhaustive
            }.launchIn(viewModelScope)
        }
    }
}