package com.example.eki.feature_cartable.domain.model

data class SaleHistoryRes(
    val customerName: String?,
    val factorNumber: Long?,
    val totalPrice: Long?,
    val saleDate: String?,
    val factorUrl: String?,
)
