package com.example.eki.feature_cartable.presentation.common

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.window.DialogProperties
import com.example.eki.R
import com.example.eki.ui.theme.EKITheme
import com.razaghimahdi.compose_persian_date.PersianDatePickerDialog
import com.razaghimahdi.compose_persian_date.core.PersianDatePickerController
import com.razaghimahdi.compose_persian_date.core.rememberPersianDatePicker

@Composable
fun PersianDatePicker(
    modifier: Modifier = Modifier,
    controller: PersianDatePickerController,
    onSubmit: (controller: PersianDatePickerController) -> Unit = {},
    onDateChanged: (year: Int, month: Int, day: Int) -> Unit,
) {

    Box(Modifier.fillMaxSize()) {
        PersianDatePickerDialog(
            controller,
            modifier = modifier.fillMaxWidth(),
            textButtonStyle = MaterialTheme.typography.bodyMedium,
            backgroundColor = MaterialTheme.colorScheme.primaryContainer,
            contentColor = MaterialTheme.colorScheme.onPrimaryContainer,
            shape = MaterialTheme.shapes.extraLarge,
            onDismissRequest = {
                onSubmit(controller)
            },
            properties = DialogProperties(
                dismissOnBackPress = false,
                dismissOnClickOutside = false
            ),
            dismissTitle = stringResource(id = R.string.confirm),
            onDateChanged = onDateChanged
        )
    }
}


@Preview
@Composable
fun PersianDatePickerPreview() {
    EKITheme {
        Box(modifier = Modifier.fillMaxSize()) {
            PersianDatePicker(
                controller = rememberPersianDatePicker(),
                onSubmit = { },
                onDateChanged = { _, _, _ -> }
            )
        }
    }
}
