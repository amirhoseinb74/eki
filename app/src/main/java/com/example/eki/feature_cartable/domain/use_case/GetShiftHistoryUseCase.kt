package com.example.eki.feature_cartable.domain.use_case

import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.data.remote.dto.InputDataConfig
import com.example.eki.core.data.remote.dto.RequestFilter
import com.example.eki.feature_cartable.data.remote.dto.ShiftHistoryResDto
import com.example.eki.feature_cartable.domain.repository.CartableRepository
import com.example.eki.feature_main.domain.repository.RescuerRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetShiftHistoryUseCase @Inject constructor(
    private val cartableRepository: CartableRepository,
    private val rescuerRepository: RescuerRepository,
) {
    suspend operator fun invoke(
        startDateTime: String,
        endDateTime: String,
    ): Flow<ResponseHandler<List<ShiftHistoryResDto>>> {
        val token = rescuerRepository.getToken()
        val filters = arrayListOf(
            RequestFilter("fromDate", startDateTime),
            RequestFilter("toDate", endDateTime)
        )
        val inputDataConfig = InputDataConfig(filters, token = token)

        return ResponseHandler.responseHandlerFlow {
            cartableRepository.getShiftHistory(inputDataConfig, token)
        }
    }
}