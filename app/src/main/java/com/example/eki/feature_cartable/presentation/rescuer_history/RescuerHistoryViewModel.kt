package com.example.eki.feature_cartable.presentation.rescuer_history

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_cartable.data.remote.dto.toRescuerHistoryRes
import com.example.eki.feature_cartable.domain.use_case.GetRescuerHistoryUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RescuerHistoryViewModel @Inject constructor(
    private val getRescuerHistoryUseCase: GetRescuerHistoryUseCase,
) : ViewModel() {

    sealed class RescuerHistoryUIEvent {
        data class ShowError(val message: String) : RescuerHistoryUIEvent()
    }

    private val _eventFlow = MutableSharedFlow<RescuerHistoryUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private val _rescuerHistoryState = mutableStateOf(RescuerHistoryState())
    val rescuerHistoryState: State<RescuerHistoryState> = _rescuerHistoryState

    fun getRescuerHistory(startDate: String, endData: String) {
        viewModelScope.launch {
            getRescuerHistoryUseCase(startDate, endData).onEach { result ->
                _rescuerHistoryState.value = when (result) {
                    is ResponseHandler.Loading -> {
                        RescuerHistoryState(true)
                    }

                    is ResponseHandler.Error -> {
                        _eventFlow.emit(
                            RescuerHistoryUIEvent.ShowError(
                                result.message ?: ResponseHandler.generalErrorMessage
                            )
                        )
                        RescuerHistoryState()
                    }

                    is ResponseHandler.Success -> {
                        RescuerHistoryState(data = result.data?.map { it.toRescuerHistoryRes() })
                    }
                }.exhaustive
            }.launchIn(viewModelScope)
        }
    }
}