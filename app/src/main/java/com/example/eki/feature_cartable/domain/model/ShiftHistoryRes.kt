package com.example.eki.feature_cartable.domain.model

data class ShiftHistoryRes(
    val turnTime: String,
    val shiftType: String,
    val presence: String,
    val regionName: String,
    val shiftDate: String,
    val absenceType: AbsenceType,
)

enum class AbsenceType(val pName: String) {
    NOT_ABSENCE("عدم غیبت"),
    ABSENCE("غیبت"),
    IN_VACATION("مرخصی")
}