package com.example.eki.feature_cartable.presentation.rescuer_history

import com.example.eki.feature_cartable.domain.model.RescuerHistoryRes

data class RescuerHistoryState(
    val loading: Boolean = false,
    val data: List<RescuerHistoryRes>? = null,
)
