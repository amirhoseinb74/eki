package com.example.eki.feature_cartable.presentation.sale_history

import com.example.eki.feature_cartable.domain.model.SaleHistoryRes

data class SaleHistoryState(
    val loading: Boolean = false,
    val data: List<SaleHistoryRes>? = null,
)
