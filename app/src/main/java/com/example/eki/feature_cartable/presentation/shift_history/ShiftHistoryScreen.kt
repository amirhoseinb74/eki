package com.example.eki.feature_cartable.presentation.shift_history

import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.eki.core.presentation.components.animation.LoadingAnimation
import com.example.eki.core.presentation.components.animation.NoDataAnimation
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_cartable.domain.model.AbsenceType
import com.example.eki.feature_cartable.domain.model.ShiftHistoryRes
import com.example.eki.feature_cartable.presentation.common.CartableScreensCommon
import com.example.eki.feature_cartable.presentation.shift_history.components.ShiftHistoryItem
import com.example.eki.ui.spacing
import kotlinx.coroutines.flow.collectLatest

@Composable
fun ShiftHistoryScreen(
    navController: NavController,
    viewModel: ShiftHistoryViewModel = hiltViewModel(),
) {
    ShiftHistoryScreenContent(
        navController,
        viewModel.shiftHistoryState.value,
        onDateChanged = viewModel::getShiftHistory,
        showSnackbar = { showMessage ->
            LaunchedEffect(key1 = true) {
                viewModel.eventFlow.collectLatest { shiftHistoryUIEvent ->
                    when (shiftHistoryUIEvent) {
                        is ShiftHistoryViewModel.ShiftHistoryUIEvent.ShowError -> {
                            showMessage(shiftHistoryUIEvent.message)
                        }
                    }.exhaustive
                }
            }
        })

}

@Preview
@Composable
private fun ShiftHistoryScreenContent(
    navController: NavController = rememberNavController(),
    shiftHistoryState: ShiftHistoryState = ShiftHistoryState(
        loading = false,
        data = listOf(
            ShiftHistoryRes(
                "11:29 - 18:00",
                "عادی",
                "استقرار",
                "12",
                shiftDate = "102/06/11",
                AbsenceType.ABSENCE
            ),
            ShiftHistoryRes(
                "11:29 - 18:00",
                "عادی",
                "استقرار",
                "12",
                shiftDate = "102/06/11",
                AbsenceType.NOT_ABSENCE
            ),
            ShiftHistoryRes(
                "11:29 - 18:00",
                "عادی",
                "استقرار",
                "12",
                shiftDate = "102/06/11",
                AbsenceType.IN_VACATION
            )
        )
    ),
    onDateChanged: (startDate: String, endDate: String) -> Unit = { _, _ -> },
    showSnackbar: @Composable (launch: (String) -> Unit) -> Unit = {},
) {
    CartableScreensCommon(
        navController = navController,
        onDateChanged = onDateChanged
    ) { showMessage ->
        if (shiftHistoryState.loading) {
            LoadingAnimation()
        }
        shiftHistoryState.data?.let { shiftHistoryResList ->
            if (shiftHistoryResList.isEmpty()) {
                NoDataAnimation()
            } else {
                LazyColumn(
                    modifier = Modifier
                        .wrapContentSize()
                        .padding(MaterialTheme.spacing.small),
                ) {
                    items(shiftHistoryResList) { item: ShiftHistoryRes ->
                        ShiftHistoryItem(
                            modifier = Modifier.padding(MaterialTheme.spacing.small),
                            item = item
                        )
                    }
                }
            }
        }
        showSnackbar(showMessage)
    }
}