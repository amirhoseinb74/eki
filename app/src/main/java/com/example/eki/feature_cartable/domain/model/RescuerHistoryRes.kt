package com.example.eki.feature_cartable.domain.model

data class RescuerHistoryRes(
    val eventCarId: String?,
    val helpId: String?,
    val workTypeId: Int?,
    val workOrderDate: String?,
    val workTypeName: String?,
    val emdadType: String?,
    val lastKilometer: String?,
    val startDateTime: String?,
    val endDateTime: String?,
    val description: String?,
    val pieces: String?,
    val wages: String?,
    val details: String?,
    val showFinanceButton: Boolean,
    val showSurveyButton: Boolean,
    val rescuerWorkOrderType: RescuerWorkOrderType = RescuerWorkOrderType.CLINICAL,
) {
    companion object {
        val testData = listOf(
            RescuerHistoryRes(
                "30342025",
                "33583709",
                1,
                "1402/06/05 15:09:45",
                "-",
                "امداد (خاتمه يافته)",
                "54425",
                "1402/06/05 15:09:45",
                "1402/06/05 15:11:08",
                "-",
                "-",
                "-",
                "",
                showFinanceButton = true,
                showSurveyButton = false,
            ), RescuerHistoryRes(
                "30342025",
                "33583709",
                1,
                "1402/06/05 15:09:45",
                "-",
                "امداد (خاتمه يافته)",
                "54425",
                "1402/06/05 15:09:45",
                "1402/06/05 15:11:08",
                "-",
                "-",
                "-",
                "",
                showFinanceButton = true,
                showSurveyButton = false,
                rescuerWorkOrderType = RescuerWorkOrderType.CARRY
            )
        )
    }
}

enum class RescuerWorkOrderType(val pName: String) {
    CLINICAL("بالینی"),
    CARRY("حمل")
}