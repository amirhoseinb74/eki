package com.example.eki.feature_cartable.presentation.common.bottom_navigation

import com.example.eki.R
import com.example.eki.feature_cartable.presentation.CartableScreens
import com.example.eki.feature_main.presentation.MainScreens

sealed class CartableBottomNavItem(
    var title: String? = null,
    var icon: Int? = null,
    var selected: Boolean = false,
    val direction: String,
) {
    object Home : CartableBottomNavItem(
        icon = R.drawable.ic_home,
        direction = MainScreens.Home.route
    )

    object RescuerHistory : CartableBottomNavItem(
        title = "امداد",
        icon = R.drawable.ic_technician,
        direction = CartableScreens.RescuerHistory.route
    )

    object Shift : CartableBottomNavItem(
        title = "شیفت",
        selected = true,
        icon = R.drawable.ic_shift,
        direction = CartableScreens.ShiftHistory.route
    )

    object Financial : CartableBottomNavItem(
        title = "مالی",
        icon = R.drawable.ic_financial,
        direction = CartableScreens.FinancialHistory.route
    )

    object Sale : CartableBottomNavItem(
        title = "فروش",
        icon = R.drawable.business,
        direction = CartableScreens.SaleHistory.route
    )

    object Functionality : CartableBottomNavItem(
        title = "عملکرد",
        icon = R.drawable.performance_report,
        direction = CartableScreens.FunctionalityHistory.route
    )
}