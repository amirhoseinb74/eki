package com.example.eki.feature_cartable.presentation.shift_history.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.RowOrColumn
import com.example.eki.core.presentation.components.TitleValueItem
import com.example.eki.core.presentation.components.WindowInfo
import com.example.eki.core.presentation.components.rememberWindowInfo
import com.example.eki.feature_cartable.domain.model.AbsenceType
import com.example.eki.feature_cartable.domain.model.ShiftHistoryRes
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ShiftHistoryItem(
    modifier: Modifier = Modifier,
    item: ShiftHistoryRes,
) {

    val isCompactWidth = rememberWindowInfo().screenWidthInfo == WindowInfo.WindowType.Compact

    val backgroundColor = when (item.absenceType) {
        AbsenceType.NOT_ABSENCE -> MaterialTheme.colorScheme.background
        AbsenceType.ABSENCE -> Color.Red
        AbsenceType.IN_VACATION -> MaterialTheme.colorScheme.primaryContainer
    }

    val contentColor = when (item.absenceType) {
        AbsenceType.NOT_ABSENCE -> MaterialTheme.colorScheme.onBackground
        AbsenceType.ABSENCE -> Color.White
        AbsenceType.IN_VACATION -> MaterialTheme.colorScheme.onPrimaryContainer
    }

    ElevatedCard(modifier) {
        Column(Modifier.background(backgroundColor, shape = MaterialTheme.shapes.extraSmall)) {
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        horizontal = MaterialTheme.spacing.medium,
                        vertical = MaterialTheme.spacing.xSmall
                    ),
                horizontalArrangement = Arrangement.SpaceBetween
            ) {
                Text(
                    text = item.shiftDate,
                    style = MaterialTheme.typography.bodyMedium,
                    color = contentColor
                )
                Text(
                    text = item.turnTime,
                    style = MaterialTheme.typography.bodyMedium,
                    color = contentColor
                )
            }
            Row {
                TitleValueItem(
                    modifier = Modifier.weight(1f),
                    containerColor = backgroundColor,
                    contentColor = contentColor,
                    title = stringResource(R.string.shift_type),
                    value = item.shiftType
                )
                TitleValueItem(
                    modifier = Modifier.weight(1f),
                    containerColor = backgroundColor,
                    contentColor = contentColor,
                    title = stringResource(R.string.presence_type),
                    value = item.presence
                )
            }
            RowOrColumn(
                modifier = Modifier
                    .fillMaxWidth()
                    .height(IntrinsicSize.Min),
                isVertical = isCompactWidth,
                verticalAlignment = Alignment.CenterVertically
            ) {
                TitleValueItem(
                    modifier = Modifier.weight(1f),
                    containerColor = backgroundColor,
                    contentColor = contentColor,
                    title = stringResource(R.string.region_position),
                    value = item.regionName
                )
                if (item.absenceType != AbsenceType.NOT_ABSENCE) {
                    TitleValueItem(
                        modifier = Modifier.weight(1f),
                        containerColor = backgroundColor,
                        contentColor = contentColor,
                        title = stringResource(R.string.absence_reason),
                        value = item.absenceType.pName
                    )
                }
            }
        }
    }
}


@Preview(device = Devices.PIXEL)
@Composable
fun ShiftHistoryItemPreview() {
    EKITheme {
        ShiftHistoryItem(
            item = ShiftHistoryRes(
                "11:29 - 18:00",
                "عادی",
                "استقرار",
                "12",
                shiftDate = "102/06/11",
                AbsenceType.ABSENCE
            )
        )
    }
}