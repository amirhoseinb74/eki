package com.example.eki.feature_cartable.data.remote.dto

import com.example.eki.core.util.Constants
import com.example.eki.feature_cartable.domain.model.SaleHistoryRes

data class SaleHistoryResDto(
    val commercialCode: String?,
    val createdAt: String?,
    val createdAtShamsi: String?,
    val createdBy: String?,
    val docNo: Any?,
    val encryptedId: String?,
    val firstName: String?,
    val id: Long?,
    val lastName: String?,
    val latitude: String?,
    val longitude: String?,
    val mobile: String?,
    val modifiedAt: String?,
    val modifiedBy: Any?,
    val nationalCode: String?,
    val parts: Any?,
    val paymentStatus: Int?,
    val totalDiscount: Int?,
    val totalFinalPrice: Int?,
    val totalPrice: Long?,
    val totalTax: Int?,
)

fun SaleHistoryResDto.toSaleHistoryRes() = SaleHistoryRes(
    customerName = "$firstName $lastName",
    factorNumber = id,
    totalPrice = totalPrice,
    saleDate = createdAtShamsi,
    factorUrl = encryptedId?.let { "${Constants.BASE_URL}app/printSaleFactor?id=$it" }
)