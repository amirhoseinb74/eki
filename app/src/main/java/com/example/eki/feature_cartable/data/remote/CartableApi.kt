package com.example.eki.feature_cartable.data.remote

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.dto.InputDataConfig
import com.example.eki.feature_cartable.data.remote.dto.RescuerHistoryResDto
import com.example.eki.feature_cartable.data.remote.dto.SaleHistoryResDto
import com.example.eki.feature_cartable.data.remote.dto.ShiftHistoryResDto
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface CartableApi {

    @POST("rescuer/history/relief")
    suspend fun getRescuerHistory(
        @Body inputDataConfig: InputDataConfig,
        @Query("access_token") token: String,
    ): BaseResponse<RescuerHistoryResDto>

    @POST("rescuer/history/turn")
    suspend fun getRescuerShiftHistory(
        @Body inputDataConfig: InputDataConfig,
        @Query("access_token") token: String,
    ): BaseResponse<ShiftHistoryResDto>

    @POST("app/sale/list")
    suspend fun getSaleHistory(
        @Body inputDataConfig: InputDataConfig,
        @Query("access_token") token: String,
    ): BaseResponse<SaleHistoryResDto>
}