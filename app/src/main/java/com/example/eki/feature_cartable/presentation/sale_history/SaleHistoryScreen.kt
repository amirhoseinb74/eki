package com.example.eki.feature_cartable.presentation.sale_history

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.eki.core.presentation.components.animation.LoadingAnimation
import com.example.eki.core.presentation.components.animation.NoDataAnimation
import com.example.eki.core.util.Constants
import com.example.eki.core.util.downloader.open
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_cartable.domain.model.SaleHistoryRes
import com.example.eki.feature_cartable.presentation.common.CartableScreensCommon
import com.example.eki.feature_cartable.presentation.sale_history.components.SaleHistoryItem
import com.example.eki.feature_main.presentation.splash.components.DownloadLoadingIndicator
import com.example.eki.ui.spacing
import kotlinx.coroutines.flow.collectLatest

@Composable
fun SaleHistoryScreen(
    navController: NavController,
    viewModel: SaleHistoryViewModel = hiltViewModel(),
) {
    val context = LocalContext.current

    SaleHistoryScreenContent(
        navController,
        viewModel.saleHistoryState.value,
        isDownloading = viewModel.isDownloadingState.value,
        onDateChanged = viewModel::getSaleHistory,
        showSnackbar = { showMessage ->
            LaunchedEffect(key1 = true) {
                viewModel.eventFlow.collectLatest { event ->
                    when (event) {
                        is SaleHistoryViewModel.SaleHistoryUiEvent.ShowError -> {
                            showMessage(event.message)
                        }

                        is SaleHistoryViewModel.SaleHistoryUiEvent.OpenDownloadedFile -> {
                            event.downloadFile.open(context, event.uri)
                        }
                    }.exhaustive
                }
            }
        },
        sendViewModelEvent = viewModel::onEvent
    )
}

@Preview
@Composable
fun SaleHistoryScreenContent(
    navController: NavController = rememberNavController(),
    saleHistoryState: SaleHistoryState = SaleHistoryState(
        data = listOf(
            SaleHistoryRes(
                "رضا انصاری",
                12345,
                678910,
                "1402/23/23",
                "${Constants.BASE_URL}app/printSaleFactor?id=tra7FWIe9+e8j8fjRzicHw=="
            ),
            SaleHistoryRes(
                "رضا انصاری2",
                654321,
                2121323265,
                "1402/23/23",
                "${Constants.BASE_URL}app/printSaleFactor?id=tra7FWIe9+e8j8fjRzicHw=="
            )
        )
    ),
    isDownloading: Boolean = false,
    onDateChanged: (startDate: String, endDate: String) -> Unit = { _, _ -> },
    showSnackbar: @Composable (launch: (String) -> Unit) -> Unit = {},
    sendViewModelEvent: (SaleHistoryEvent) -> Unit = {},
) {
    val lifecycleOwner = LocalLifecycleOwner.current
    CartableScreensCommon(
        navController = navController,
        onDateChanged = onDateChanged
    ) { showMessage ->
        if (saleHistoryState.loading) {
            LoadingAnimation()
        }
        saleHistoryState.data?.let { saleHistoryResList ->
            if (saleHistoryResList.isEmpty()) {
                NoDataAnimation()
                return@let
            }
            Box(modifier = Modifier.fillMaxSize()) {
                LazyColumn(
                    modifier = Modifier
                        .wrapContentSize()
                        .padding(MaterialTheme.spacing.small),
                ) {
                    items(saleHistoryResList) { item: SaleHistoryRes ->
                        SaleHistoryItem(
                            modifier = Modifier.padding(MaterialTheme.spacing.small),
                            item = item,
                            onClickFactor = { url, id ->
                                sendViewModelEvent(
                                    SaleHistoryEvent.DownloadFactor(url, id, lifecycleOwner)
                                )
                            }
                        )
                    }
                }
                if (isDownloading) {
                    DownloadLoadingIndicator()
                }
            }
        }
        showSnackbar(showMessage)
    }
}
