package com.example.eki.feature_cartable.presentation.common.bottom_navigation

import androidx.compose.foundation.layout.height
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment.Companion.CenterVertically
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.eki.feature_cartable.presentation.CartableScreens
import com.example.eki.ui.theme.EKITheme

@Composable
fun CartableBottomNavigation(
    navController: NavController = rememberNavController(),
    handleOnclick: Boolean = false,
    onItemClick: (item: CartableBottomNavItem) -> Unit = {},
) {

    val items = listOf(
        CartableBottomNavItem.Home,
        CartableBottomNavItem.RescuerHistory,
        CartableBottomNavItem.Shift,
        CartableBottomNavItem.Financial,
        CartableBottomNavItem.Sale,
        CartableBottomNavItem.Functionality
    )

    val screenHeight = LocalConfiguration.current.screenHeightDp.dp
    val containerColor = MaterialTheme.colorScheme.onPrimaryContainer
    val contentColor = MaterialTheme.colorScheme.primaryContainer

    BottomAppBar(
        containerColor = containerColor,
        contentColor = contentColor,
        modifier = Modifier.height(screenHeight.div(11))
    ) {
        items.forEach { item ->
            NavigationBarItem(
                modifier = Modifier.align(CenterVertically),
                icon = {
                    item.icon?.let { itemIcon ->
                        if (item.title != null && !item.selected)
                            return@let
                        Icon(
                            painterResource(id = itemIcon),
                            contentDescription = item.title
                        )
                    }
                },
                label = {
                    item.title?.let { itemTitle ->
                        if (item.icon != null && item.selected)
                            return@let
                        Text(
                            text = itemTitle,
                            style = MaterialTheme.typography.bodySmall,
                            color = contentColor
                        )
                    }
                },
                alwaysShowLabel = item.title != null && !item.selected,
                colors = NavigationBarItemDefaults.colors(
                    selectedIconColor = contentColor,
                    selectedTextColor = contentColor,
                    unselectedIconColor = contentColor.copy(0.4f),
                    unselectedTextColor = Color.White
                ),
                selected = item.selected,
                onClick = {
                    if (!handleOnclick) {
                        if (!item.selected) {
                            if (item.direction == CartableBottomNavItem.Home.direction) {
                                navController.navigate(item.direction) {
                                    popUpTo(CartableScreens.Cartable.route) {
                                        inclusive = true
                                    }
                                }
                            } else {
                                navController.navigate(item.direction)
                            }
                        }
                    } else {
                        onItemClick(item)
                    }
                }
            )
        }
    }

    navController.addOnDestinationChangedListener { _, destination, _ ->
        items.forEach { item ->
            item.selected = item.direction == destination.route
        }
    }
}

@Preview(device = Devices.PHONE)
@Composable
fun CartableBottomNavigationPreview() {
    EKITheme {
        CartableBottomNavigation(rememberNavController())
    }
}