package com.example.eki.feature_news.domain.use_case

import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.example.eki.feature_news.data.remote.dto.NewsDetailResDto
import com.example.eki.feature_news.data.remote.dto.SendNewsReqDto
import com.example.eki.feature_news.domain.repository.NewsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class SendNewsUseCase @Inject constructor(
    private val newsRepository: NewsRepository,
    private val rescuerRepository: RescuerRepository,
) {
    operator fun invoke(message: String): Flow<ResponseHandler<List<NewsDetailResDto>>> {
        val sendNewsReqDto = SendNewsReqDto(message)
        return ResponseHandler.responseHandlerFlow(dataNeeded = false) {
            newsRepository.sendNews(sendNewsReqDto, rescuerRepository.getToken())
        }
    }
}