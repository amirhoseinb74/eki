package com.example.eki.feature_news.data.remote

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.feature_news.data.remote.dto.AllNewsReqDto
import com.example.eki.feature_news.data.remote.dto.AllNewsResDto
import com.example.eki.feature_news.data.remote.dto.NewsDetailReqDto
import com.example.eki.feature_news.data.remote.dto.NewsDetailResDto
import com.example.eki.feature_news.data.remote.dto.NewsTypeResDto
import com.example.eki.feature_news.data.remote.dto.SendNewsReqDto
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface NewsApi {

    @POST("all_news")
    suspend fun getAllNews(
        @Body allNewsReqDto: AllNewsReqDto,
        @Query("access_token") token: String,
    ): BaseResponse<AllNewsResDto>

    @POST("news_type")
    suspend fun getNewsType(
        @Query("access_token") token: String,
    ): BaseResponse<NewsTypeResDto>

    @POST("send_news")
    suspend fun sendNews(
        @Body sendNewsReqDto: SendNewsReqDto,
        @Query("access_token") token: String,
    ): BaseResponse<NewsDetailResDto>

    @POST("news_details")
    suspend fun getNewsDetail(
        @Body newsDetailReqDto: NewsDetailReqDto,
        @Query("access_token") token: String,
    ): BaseResponse<NewsDetailResDto>
}