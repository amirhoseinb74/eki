package com.example.eki.feature_news.data.remote.dto

data class NewsDetailReqDto(
    private val newsId: String,
)
