package com.example.eki.feature_news.presentation.news_detail

import com.example.eki.feature_news.domain.model.NewsDetailRes

data class NewsDetailState(
    val loading: Boolean = false,
    val data: List<NewsDetailRes>? = null,
)
