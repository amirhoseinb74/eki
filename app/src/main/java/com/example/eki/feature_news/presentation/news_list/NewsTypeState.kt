package com.example.eki.feature_news.presentation.news_list

import com.example.eki.feature_news.data.remote.dto.NewsTypeResDto

data class NewsTypeState(
    val loading: Boolean = false,
    val data: List<NewsTypeResDto>? = null,
)