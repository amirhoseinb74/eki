package com.example.eki.feature_news.presentation.news_list

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_news.data.remote.dto.toAllNewsRes
import com.example.eki.feature_news.domain.use_case.GetAllNewsUseCase
import com.example.eki.feature_news.domain.use_case.GetNewsTypeUseCase
import com.example.eki.feature_news.domain.use_case.SendNewsUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class NewsListViewModel @Inject constructor(
    private val getAllNewsUseCase: GetAllNewsUseCase,
    private val getNewsTypeUseCase: GetNewsTypeUseCase,
    private val sendNewsUseCase: SendNewsUseCase,
) : ViewModel() {

    private val _newsListState = mutableStateOf(NewsListState())
    val newsListState: State<NewsListState> = _newsListState

    private val _newsTypeState = mutableStateOf(NewsTypeState())
    val newsTypeState: State<NewsTypeState> = _newsTypeState

    private val _sendNewsState = mutableStateOf(SendNewsState())
    val sendNewsState: State<SendNewsState> = _sendNewsState

    private val _eventFlow = MutableSharedFlow<NewsListUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    sealed class NewsListUIEvent {
        data class ShowError(val message: String) : NewsListUIEvent()
    }

    private var getAllNewsJob: Job? = null
    private var getNewsTypeJob: Job? = null
    private var sendNewsJob: Job? = null

    init {
        getNewsType()
        getAllNews()
    }

    fun onEvent(event: NewsListEvent) {
        when (event) {
            is NewsListEvent.UpdateAllNews -> event.run { getAllNews(newsTypeId, searchedPhrase) }
            is NewsListEvent.SendNews -> sendNews(event.message)
        }.exhaustive
    }

    private fun getAllNews(
        newsTypeId: String? = null,
        searchPhrase: String = "",
    ) {
        getAllNewsJob?.cancel()
        getAllNewsJob = getAllNewsUseCase(newsTypeId ?: "0", searchPhrase).onEach { result ->
            _newsListState.value = when (result) {
                is ResponseHandler.Loading -> NewsListState(true)
                is ResponseHandler.Success ->
                    NewsListState(data = result.data?.map { it.toAllNewsRes() })

                is ResponseHandler.Error -> {
                    _eventFlow.emit(
                        NewsListUIEvent.ShowError(
                            result.message ?: ResponseHandler.generalErrorMessage
                        )
                    )
                    NewsListState()
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun getNewsType() {
        getNewsTypeJob?.cancel()
        getNewsTypeJob = getNewsTypeUseCase().onEach { result ->
            _newsTypeState.value = when (result) {
                is ResponseHandler.Loading -> NewsTypeState(true)
                is ResponseHandler.Success ->
                    NewsTypeState(data = result.data)

                is ResponseHandler.Error -> {
                    _eventFlow.emit(
                        NewsListUIEvent.ShowError(
                            result.message ?: ResponseHandler.generalErrorMessage
                        )
                    )
                    NewsTypeState()
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun sendNews(message: String) {
        sendNewsJob?.cancel()
        sendNewsJob = sendNewsUseCase(message).onEach { result ->
            _sendNewsState.value = when (result) {
                is ResponseHandler.Loading -> SendNewsState(true)
                is ResponseHandler.Success ->
                    SendNewsState(success = true, data = result.data)

                is ResponseHandler.Error -> {
                    _eventFlow.emit(
                        NewsListUIEvent.ShowError(
                            result.message ?: ResponseHandler.generalErrorMessage
                        )
                    )
                    SendNewsState()
                }
            }
        }.launchIn(viewModelScope)
    }

    override fun onCleared() {
        getAllNewsJob?.cancel()
        getNewsTypeJob?.cancel()
        sendNewsJob?.cancel()
        super.onCleared()
    }
}