package com.example.eki.feature_news.presentation.news_list

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.eki.core.presentation.components.CustomSearchView
import com.example.eki.core.presentation.components.ScaffoldState
import com.example.eki.core.presentation.components.animation.LoadingAnimation
import com.example.eki.core.presentation.components.animation.NoDataAnimation
import com.example.eki.feature_news.data.remote.dto.NewsTypeResDto
import com.example.eki.feature_news.domain.model.AllNewsRes
import com.example.eki.feature_news.presentation.NewsScreens
import com.example.eki.feature_news.presentation.news_list.components.DropdownWithType
import com.example.eki.feature_news.presentation.news_list.components.NewsListItem
import com.example.eki.feature_news.presentation.news_list.components.SendMessageToNews
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.BlueBespor
import com.example.eki.ui.theme.EKITheme
import kotlinx.coroutines.flow.collectLatest

@Composable
fun NewsListScreen(
    navController: NavController,
    viewModel: NewsListViewModel = hiltViewModel(),
) {
    ScaffoldState { showSnackBar ->
        NewsListScreenContent(
            newsListState = viewModel.newsListState.value,
            newsTypeState = viewModel.newsTypeState.value,
            sendNewsState = viewModel.sendNewsState.value,
            sendViewModelEvent = viewModel::onEvent,
            onClickNews = {
                navController.navigate("${NewsScreens.NewsDetail.route}/${it.id}")
            }
        )
        LaunchedEffect(key1 = true) {
            viewModel.eventFlow.collectLatest { event ->
                when (event) {
                    is NewsListViewModel.NewsListUIEvent.ShowError -> showSnackBar(event.message)
                }
            }
        }
    }
}

@OptIn(ExperimentalFoundationApi::class)
@Composable
fun NewsListScreenContent(
    newsListState: NewsListState,
    newsTypeState: NewsTypeState,
    sendNewsState: SendNewsState,
    sendViewModelEvent: (NewsListEvent) -> Unit,
    onClickNews: (AllNewsRes) -> Unit,
) {
    var searchQuery by remember { mutableStateOf("") }
    var selectedNewsType by remember { mutableStateOf<NewsTypeResDto?>(null) }

    val bgColor = BlueBespor
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(bgColor)
    ) {
        LazyColumn(
            Modifier.fillMaxSize(),
            state = rememberLazyListState()
        ) {
            stickyHeader {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .height(IntrinsicSize.Min)
                        .padding(MaterialTheme.spacing.medium),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    CustomSearchView(
                        modifier = Modifier.weight(2f),
                        onSearchTextChanged = { searchQuery = it },
                        onSearchClick = {
                            sendViewModelEvent(updateAllNews(selectedNewsType, searchQuery))
                        }
                    )

                    DropdownWithType(
                        modifier = Modifier
                            .fillMaxSize()
                            .weight(1f)
                            .padding(horizontal = MaterialTheme.spacing.small),
                        typesState = newsTypeState,
                        onItemSelected = {
                            selectedNewsType = it
                            sendViewModelEvent(updateAllNews(selectedNewsType, searchQuery))
                        }
                    )
                }
            }
            newsListState.data?.let { newsList ->
                if (newsList.isEmpty()) {
                    item {
                        NoDataAnimation()
                    }
                    return@let
                }
                items(newsList) { item: AllNewsRes ->
                    NewsListItem(newsItem = item, onItemClick = onClickNews)
                }
            }
        }

        selectedNewsType?.id?.let { selectedNewsId ->
            if (selectedNewsId == "1") //پیام عادی
                SendMessageToNews(
                    modifier = Modifier
                        .fillMaxWidth()
                        .background(bgColor)
                        .padding(
                            horizontal = MaterialTheme.spacing.small,
                            vertical = MaterialTheme.spacing.xSmall
                        )
                        .align(Alignment.BottomCenter),
                    loading = sendNewsState.loading,
                    onSendClick = { message -> sendViewModelEvent(NewsListEvent.SendNews(message)) }
                )
        }
        if (newsListState.loading) {
            LoadingAnimation()
        }
        LaunchedEffect(sendNewsState.success) {
            sendViewModelEvent(updateAllNews(selectedNewsType, searchQuery))
        }
    }
}

private fun updateAllNews(
    selectedNewsType: NewsTypeResDto?,
    searchQuery: String,
) = NewsListEvent.UpdateAllNews(
    selectedNewsType?.id,
    searchQuery
)

@Preview
@Composable
fun NewsListScreenPreview() {
    EKITheme {
        NewsListScreenContent(newsListState = NewsListState(data = AllNewsRes.testData),
            newsTypeState = NewsTypeState(loading = false, NewsTypeResDto.testData),
            sendNewsState = SendNewsState(),
            sendViewModelEvent = {},
            onClickNews = {})
    }
}