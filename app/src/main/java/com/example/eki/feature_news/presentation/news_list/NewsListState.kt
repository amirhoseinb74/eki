package com.example.eki.feature_news.presentation.news_list

import com.example.eki.feature_news.domain.model.AllNewsRes

data class NewsListState(
    val loading: Boolean = false,
    val data: List<AllNewsRes>? = null,
)