package com.example.eki.feature_news.data.remote.dto

import com.google.gson.annotations.SerializedName

data class NewsTypeResDto(
    @SerializedName("newsTypeId") val id: String? = null,
    val title: String? = null,
) {
    companion object {
        val testData = listOf(
            NewsTypeResDto(id = "1", title = "عادی"),
            NewsTypeResDto(id = "2", title = "سازمانی"),
            NewsTypeResDto(id = "3", title = "کنترل خدمات"),
            NewsTypeResDto(id = "4", title = "ابلاغیه"),
            NewsTypeResDto(id = "5", title = "روش اجرایی"),
        )
    }
}

