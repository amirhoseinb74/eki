package com.example.eki.feature_news.data.remote.dto

import com.example.eki.feature_news.domain.model.NewsDetailRes
import com.google.gson.annotations.SerializedName

data class NewsDetailResDto(
    @SerializedName("newsId") var id: String?,
    @SerializedName("dtAddAt") val date: String?,
    @SerializedName("newsType") val type: String?,
    val title: String?,
    @SerializedName("messageType") val fileType: String?,
    @SerializedName("message") val attachedFile: String?,
    val messageContent: String?,
    val fileName: String?,
    val fileId: String?,
)

fun NewsDetailResDto.toNewsRes() = NewsDetailRes(
    title = title ?: "",
    content = messageContent ?: "",
    hasAttachedFile = fileType?.let { it != "html" } ?: false,
    attachedFileUrl = attachedFile?.takeIf { it.contains("http") } ?: "https://$attachedFile",
//    fileName = "${fileName?.split("/")?.last()?.substringBeforeLast(".pdf")}$fileId",
    fileName = fileId ?: "News-${System.currentTimeMillis()}",
)