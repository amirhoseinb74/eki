package com.example.eki.feature_news.domain.model

data class NewsDetailRes(
    val title: String,
    val content: String,
    val hasAttachedFile: Boolean = false,
    val attachedFileUrl: String,
    val fileName: String,
) {
    companion object {
        val testData = NewsDetailRes(
            title = "انتشار اطلاعیه فنی66-1402عیب یابی نشت روغن از سرسیلندرموتور تارا",
//            content = "<p class=\\\"MsoNormal\\\" dir=\\\"RTL\\\" style=\\\"text-align:justify;direction:rtl;unicode-bidi:\\nembed\\\"><b><span lang=\\\"FA\\\" style=\\\"font-size:10.0pt;font-family:&quot;B Nazanin&quot;;\\nmso-ascii-font-family:Calibri;mso-fareast-font-family:Calibri;mso-hansi-font-family:\\nCalibri\\\">باسلام <o:p></o:p></span></b></p><p class=\\\"MsoNormal\\\" dir=\\\"RTL\\\" style=\\\"text-align:justify;direction:rtl;unicode-bidi:\\nembed\\\"><b><span lang=\\\"FA\\\" style=\\\"font-size:10.0pt;font-family:&quot;B Nazanin&quot;;\\nmso-ascii-font-family:Calibri;mso-fareast-font-family:Calibri;mso-hansi-font-family:\\nCalibri\\\">با توجه به الزام پلیس راهنمائی و رانندگی لطفا در اسرع &nbsp;با مراجعه به لینک زیر &nbsp;نسبت به ارسال شماره گواهینامه اقدام نمائید . <o:p></o:p></span></b></p><p class=\\\"MsoNormal\\\" dir=\\\"RTL\\\" style=\\\"text-align:justify;direction:rtl;unicode-bidi:\\nembed\\\"><b><span lang=\\\"FA\\\" style=\\\"font-size:10.0pt;font-family:&quot;B Nazanin&quot;;\\nmso-ascii-font-family:Calibri;mso-fareast-font-family:Calibri;mso-hansi-font-family:\\nCalibri\\\">مدیریت خدمات سیار <o:p></o:p></span></b></p><b><span style=\\\"font-size:10.0pt;font-family:&quot;Calibri&quot;,sans-serif;mso-fareast-font-family:\\nCalibri;mso-bidi-font-family:&quot;B Nazanin&quot;;mso-ansi-language:EN-US;mso-fareast-language:\\nEN-US;mso-bidi-language:FA\\\">https://docs.google.com/forms/d/e/1FAIpQLScPZ9ASTaoFMa7uyR9r0DihFMxQ0G-iqcexWWDWv66q7JjopA/viewform?usp=sf_link</span></b>",
            content = "<font color=\\\"#282828\\\" face=\\\"irsans\\\"><span style=\\\"font-size: 12px;\\\">پیمانکار خدمات امدادی&nbsp;</span></font><div style=\\\"color: rgb(40, 40, 40); font-family: irsans; font-size: 12px;\\\">با سلام و خداقوّت</div><div style=\\\"\\\"><font color=\\\"#282828\\\" face=\\\"irsans\\\"><span style=\\\"font-size: 12px;\\\">انتشار اطلاعیه فنی66-1402عیب یابی نشت روغن از سرسیلندرموتور تاراجهت بهره برداری به حضورتان ارسال می گردد .</span></font></div>",
            hasAttachedFile = true,
            attachedFileUrl = "096440.com/api/u/7tyoeb46pv",
            fileName = ""
        )
    }
}
