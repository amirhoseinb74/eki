package com.example.eki.feature_news.data.remote.dto

import com.google.gson.annotations.SerializedName

data class AllNewsReqDto(
    val personType: String,
    val newsTypeId: String,
    @SerializedName("title")
    val searchPhrase: String,
)
