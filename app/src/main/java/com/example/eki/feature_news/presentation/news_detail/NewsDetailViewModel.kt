package com.example.eki.feature_news.presentation.news_detail

import android.net.Uri
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.core.net.toUri
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.R
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.Strings
import com.example.eki.core.util.downloader.DownloadFile
import com.example.eki.core.util.downloader.Downloader
import com.example.eki.core.util.downloader.FileType
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_news.data.remote.dto.toNewsRes
import com.example.eki.feature_news.domain.use_case.GetNewsDetailUseCase
import com.example.eki.feature_news.presentation.NewsScreens
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NewsDetailViewModel @Inject constructor(
    private val getNewsDetailUseCase: GetNewsDetailUseCase,
    savedStateHandle: SavedStateHandle,
) : ViewModel() {

    private val _newsDetailState = mutableStateOf(NewsDetailState())
    val newsDetailState: State<NewsDetailState> = _newsDetailState

    private val _isDownloadingState = mutableStateOf(false)
    val isDownloadingState: State<Boolean> = _isDownloadingState

    private val _eventFlow = MutableSharedFlow<NewsDetailUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    sealed class NewsDetailUIEvent {
        data class ShowError(val message: String) : NewsDetailUIEvent()
        data class OpenDownloadedFile(val downloadedFile: DownloadFile, val uri: Uri) :
            NewsDetailUIEvent()
    }

    init {
        savedStateHandle.get<String>(NewsScreens.PARAM_NEWS_ID)
            ?.let { newsId -> getNewsDetail(newsId) }
    }

    fun onEvent(event: NewsDetailEvent) {
        when (event) {
            is NewsDetailEvent.DownloadAttachedFile -> {
                downloadFile(event.url, event.fileName, event.lifecycleOwner)
            }
        }.exhaustive
    }

    private fun downloadFile(url: String, fileName: String, lifecycleOwner: LifecycleOwner) {
        val file = DownloadFile(fileName, FileType.PDF, url)
        Downloader.downloadFile(
            file = file,
            lifecycleOwner = lifecycleOwner,
            success = { uriString ->
                _isDownloadingState.value = false
                viewModelScope.launch {
                    _eventFlow.emit(NewsDetailUIEvent.OpenDownloadedFile(file, uriString.toUri()))
                }
            }, failed = {
                _isDownloadingState.value = false
                viewModelScope.launch {
                    showErrorInUI(Strings.get(R.string.downloading_failed))
                }
            }, running = {
                _isDownloadingState.value = true
            }
        )
    }

    private var getNewsDetailJob: Job? = null

    private fun getNewsDetail(newsId: String) {
        getNewsDetailJob?.cancel()
        getNewsDetailJob = getNewsDetailUseCase(newsId).onEach { result ->
            _newsDetailState.value = when (result) {
                is ResponseHandler.Loading -> NewsDetailState(true)
                is ResponseHandler.Success -> NewsDetailState(data = result.data?.map { it.toNewsRes() })
                is ResponseHandler.Error -> {
                    showErrorInUI(result.message)
                    NewsDetailState()
                }
            }
        }.launchIn(viewModelScope)
    }

    private suspend fun showErrorInUI(message: String?) {
        _eventFlow.emit(NewsDetailUIEvent.ShowError(message ?: ResponseHandler.generalErrorMessage))
    }

    override fun onCleared() {
        getNewsDetailJob?.cancel()
        super.onCleared()
    }
}