package com.example.eki.feature_news.presentation.news_detail

import androidx.lifecycle.LifecycleOwner

sealed class NewsDetailEvent {
    data class DownloadAttachedFile(
        val url: String,
        val fileName: String,
        val lifecycleOwner: LifecycleOwner,
    ) : NewsDetailEvent()
}
