package com.example.eki.feature_news.domain.repository

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.feature_news.data.remote.dto.AllNewsReqDto
import com.example.eki.feature_news.data.remote.dto.AllNewsResDto
import com.example.eki.feature_news.data.remote.dto.NewsDetailReqDto
import com.example.eki.feature_news.data.remote.dto.NewsDetailResDto
import com.example.eki.feature_news.data.remote.dto.NewsTypeResDto
import com.example.eki.feature_news.data.remote.dto.SendNewsReqDto

interface NewsRepository {

    suspend fun getAllNews(
        allNewsReqDto: AllNewsReqDto,
        token: String,
    ): BaseResponse<AllNewsResDto>

    suspend fun getNewsType(token: String): BaseResponse<NewsTypeResDto>

    suspend fun sendNews(
        sendNewsReqDto: SendNewsReqDto,
        token: String,
    ): BaseResponse<NewsDetailResDto>

    suspend fun getNewsDetails(
        newsDetailReqDto: NewsDetailReqDto,
        token: String,
    ): BaseResponse<NewsDetailResDto>
}