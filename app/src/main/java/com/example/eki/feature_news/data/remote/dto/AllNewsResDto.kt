package com.example.eki.feature_news.data.remote.dto

import com.example.eki.feature_news.domain.model.AllNewsRes

data class AllNewsResDto(
    val id: String? = null,
    val newsId: String? = null,
    val newsType: String? = null,
    val title: String? = null,
    val summary: String? = null,
    val dtAddAt: String? = null,
    val receiverId: String? = null,
    val userName: String? = null,
    val seenTime: String? = null,
)

fun AllNewsResDto.toAllNewsRes() = AllNewsRes(
    id = id,
    title = title ?: "",
    summary = summary ?: "",
    newsType = newsType ?: "",
    date = dtAddAt ?: ""
)