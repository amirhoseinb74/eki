package com.example.eki.feature_news.presentation.news_list.components

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Send
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.rotate
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.animation.LinearLoadingAnimation
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.BlueBespor
import com.example.eki.ui.theme.EKITheme

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun SendMessageToNews(
    modifier: Modifier = Modifier,
    loading: Boolean = true,
    containerColor: Color = BlueBespor,
    contentColor: Color = Color.White,
    borderColor: Color = Color.Yellow,
    onSendClick: (String) -> Unit = {},
) {
    val keyboardController = LocalSoftwareKeyboardController.current
    var messageToSend by rememberSaveable {
        mutableStateOf("")
    }
    Box(modifier = modifier) {
        OutlinedTextField(
            modifier = modifier,
            keyboardOptions = KeyboardOptions(imeAction = ImeAction.Send),
            keyboardActions = KeyboardActions(onSend = {
                onSendClick(messageToSend)
                keyboardController?.hide()
            }),
            textStyle = MaterialTheme.typography.bodyMedium,
            value = messageToSend,
            onValueChange = { messageToSend = it },
            label = { Text(text = stringResource(R.string.please_enter_your_message)) },
            shape = CircleShape,
            colors = TextFieldDefaults.outlinedTextFieldColors(
                containerColor = containerColor,
                placeholderColor = contentColor,
                disabledTrailingIconColor = contentColor.copy(0.5f),
                unfocusedTrailingIconColor = contentColor,
                focusedTrailingIconColor = borderColor,
                textColor = contentColor,
                cursorColor = contentColor,
                unfocusedBorderColor = borderColor.copy(alpha = 0.8f),
                focusedBorderColor = borderColor,
                focusedLabelColor = borderColor,
            ),
            trailingIcon = {
                Icon(
                    modifier = Modifier
                        .clickable {
                            if (messageToSend.isNotEmpty()) {
                                onSendClick(messageToSend)
                                keyboardController?.hide()
                            }
                        }
                        .padding(end = MaterialTheme.spacing.medium)
                        .rotate(-45f),
                    imageVector = Icons.Default.Send,
                    contentDescription = null
                )
            }
        )
        if (loading)
            LinearLoadingAnimation(
                modifier
                    .height(MaterialTheme.spacing.xLarge)
                    .align(Alignment.Center)
            )
    }
}

@Preview
@Composable
fun SendMessageToNewsPreview() {
    EKITheme {
        SendMessageToNews()
    }
}