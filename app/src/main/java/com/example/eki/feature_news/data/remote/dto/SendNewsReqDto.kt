package com.example.eki.feature_news.data.remote.dto

data class SendNewsReqDto(
    private val message: String,
)
