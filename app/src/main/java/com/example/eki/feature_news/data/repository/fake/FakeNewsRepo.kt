package com.example.eki.feature_news.data.repository.fake

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.Settings
import com.example.eki.feature_news.data.remote.dto.AllNewsReqDto
import com.example.eki.feature_news.data.remote.dto.AllNewsResDto
import com.example.eki.feature_news.data.remote.dto.NewsDetailReqDto
import com.example.eki.feature_news.data.remote.dto.NewsDetailResDto
import com.example.eki.feature_news.data.remote.dto.NewsTypeResDto
import com.example.eki.feature_news.data.remote.dto.SendNewsReqDto
import com.example.eki.feature_news.domain.repository.NewsRepository

class FakeNewsRepo : NewsRepository {
    override suspend fun getAllNews(
        allNewsReqDto: AllNewsReqDto,
        token: String,
    ): BaseResponse<AllNewsResDto> {
        return BaseResponse(data = emptyList(), settings = Settings("ok", "1"))
    }

    override suspend fun getNewsType(token: String): BaseResponse<NewsTypeResDto> {
        return BaseResponse(data = emptyList(), settings = Settings("ok", "1"))
    }

    override suspend fun sendNews(
        sendNewsReqDto: SendNewsReqDto,
        token: String,
    ): BaseResponse<NewsDetailResDto> {
        return BaseResponse(data = emptyList(), settings = Settings("ok", "1"))
    }

    override suspend fun getNewsDetails(
        newsDetailReqDto: NewsDetailReqDto,
        token: String,
    ): BaseResponse<NewsDetailResDto> {
        return BaseResponse(data = emptyList(), settings = Settings("ok", "1"))
    }
}