package com.example.eki.feature_news.presentation.news_list.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.feature_news.domain.model.AllNewsRes
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
fun NewsListItem(newsItem: AllNewsRes, onItemClick: (AllNewsRes) -> Unit) {
    Card(
        modifier = Modifier
            .fillMaxWidth()
            .padding(MaterialTheme.spacing.small),
        elevation = CardDefaults.cardElevation(MaterialTheme.spacing.xSmall)
    ) {
        val bgColor = MaterialTheme.colorScheme.primaryContainer
        val onBgColor = MaterialTheme.colorScheme.onPrimaryContainer
        Column(
            modifier = Modifier
                .background(bgColor)
                .padding(
                    horizontal = MaterialTheme.spacing.medium,
                    vertical = MaterialTheme.spacing.xSmall
                )
        ) {
            Text(
                text = newsItem.title,
                style = MaterialTheme.typography.titleSmall,
                modifier = Modifier.padding(bottom = MaterialTheme.spacing.small),
                color = onBgColor,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                text = newsItem.summary,
                style = MaterialTheme.typography.bodyMedium,
                modifier = Modifier.padding(bottom = MaterialTheme.spacing.small),
                color = onBgColor
            )
            Row(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(bottom = MaterialTheme.spacing.small),
                horizontalArrangement = Arrangement.SpaceBetween,
                verticalAlignment = Alignment.Bottom
            ) {
                Text(
                    text = newsItem.date,
                    style = MaterialTheme.typography.bodyMedium,
                    color = onBgColor.copy(0.8f)
                )
                CustomButton(
                    text = stringResource(id = R.string.more),
                    containerColor = onBgColor,
                    contentColor = bgColor,
                    onClick = { onItemClick(newsItem) })
            }
        }
    }
}

@Preview
@Composable
fun NewsListItemPreview() {
    EKITheme {
        NewsListItem(
            newsItem = AllNewsRes.testData.first(), onItemClick = {}
        )
    }
}