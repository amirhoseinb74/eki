package com.example.eki.feature_news.data.repository

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.feature_news.data.remote.NewsApi
import com.example.eki.feature_news.data.remote.dto.AllNewsReqDto
import com.example.eki.feature_news.data.remote.dto.AllNewsResDto
import com.example.eki.feature_news.data.remote.dto.NewsDetailReqDto
import com.example.eki.feature_news.data.remote.dto.NewsDetailResDto
import com.example.eki.feature_news.data.remote.dto.NewsTypeResDto
import com.example.eki.feature_news.data.remote.dto.SendNewsReqDto
import com.example.eki.feature_news.domain.repository.NewsRepository

class NewsRepositoryImpl constructor(
    private val newsApi: NewsApi,
) : NewsRepository {

    override suspend fun getAllNews(
        allNewsReqDto: AllNewsReqDto,
        token: String,
    ): BaseResponse<AllNewsResDto> = newsApi.getAllNews(allNewsReqDto, token)

    override suspend fun getNewsType(
        token: String,
    ): BaseResponse<NewsTypeResDto> = newsApi.getNewsType(token)

    override suspend fun sendNews(
        sendNewsReqDto: SendNewsReqDto,
        token: String,
    ): BaseResponse<NewsDetailResDto> = newsApi.sendNews(sendNewsReqDto, token)

    override suspend fun getNewsDetails(
        newsDetailReqDto: NewsDetailReqDto,
        token: String,
    ): BaseResponse<NewsDetailResDto> = newsApi.getNewsDetail(newsDetailReqDto, token)
}