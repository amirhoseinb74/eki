package com.example.eki.feature_news.presentation.news_list

import com.example.eki.feature_news.data.remote.dto.NewsDetailResDto

data class SendNewsState(
    val loading: Boolean = false,
    val success: Boolean = false,
    val data: List<NewsDetailResDto>? = null,
)
