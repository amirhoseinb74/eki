package com.example.eki.feature_news.presentation.news_list.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowDropDown
import androidx.compose.material3.DropdownMenu
import androidx.compose.material3.DropdownMenuItem
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.eki.core.presentation.components.HorizontalSpacer
import com.example.eki.core.presentation.components.animation.LinearLoadingAnimation
import com.example.eki.feature_news.data.remote.dto.NewsTypeResDto
import com.example.eki.feature_news.presentation.news_list.NewsTypeState
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.BlueBespor
import com.example.eki.ui.theme.EKITheme

@Composable
fun DropdownWithType(
    modifier: Modifier = Modifier,
    typesState: NewsTypeState,
    onItemSelected: (NewsTypeResDto) -> Unit,
) {
    Box(
        modifier = modifier
            .background(BlueBespor, shape = MaterialTheme.shapes.small)
            .border(
                BorderStroke(MaterialTheme.spacing.xxSmall, Color.Yellow),
                shape = MaterialTheme.shapes.small
            )
    ) {
        typesState.data?.let { types ->
            var selectedType by remember { mutableStateOf(types.first()) }
            var expanded by remember { mutableStateOf(false) }
            Column {
                Row(modifier = Modifier
                    .clickable { expanded = true }
                    .padding(
                        horizontal = MaterialTheme.spacing.small,
                        vertical = MaterialTheme.spacing.small
                    ),
                    verticalAlignment = Alignment.CenterVertically,
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    selectedType.title?.let { title ->
                        Text(
                            text = title,
                            style = MaterialTheme.typography.headlineSmall.copy(fontSize = 15.sp)
                        )
                    }

                    HorizontalSpacer(width = 2)
                    Icon(
                        modifier = modifier.size(MaterialTheme.spacing.large),
                        imageVector = Icons.Default.ArrowDropDown,
                        contentDescription = null,
                        tint = Color.White,
                    )
                }

                DropdownMenu(
                    expanded = expanded,
                    onDismissRequest = { expanded = false }
                ) {
                    types.forEach { type ->
                        DropdownMenuItem(
                            text = {
                                Text(text = type.title ?: "no title")
                            },
                            onClick = {
                                selectedType = type
                                expanded = false
                                onItemSelected(type)
                            }
                        )
                    }
                }
            }
        }
        if (typesState.loading) {
            LinearLoadingAnimation(modifier.height(MaterialTheme.spacing.large))
        }
    }
}

@Preview
@Composable
fun DropdownWithTypeExample() {
    EKITheme {
        val types = NewsTypeResDto.testData
        DropdownWithType(
            typesState = NewsTypeState(true, types),
            onItemSelected = {})
    }
}
