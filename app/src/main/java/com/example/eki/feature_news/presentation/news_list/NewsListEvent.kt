package com.example.eki.feature_news.presentation.news_list

sealed class NewsListEvent {
    data class UpdateAllNews(val newsTypeId: String?, val searchedPhrase: String) : NewsListEvent()
    data class SendNews(val message: String) : NewsListEvent()
}
