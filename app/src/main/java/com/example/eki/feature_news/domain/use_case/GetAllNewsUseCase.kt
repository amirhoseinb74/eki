package com.example.eki.feature_news.domain.use_case

import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.example.eki.feature_news.data.remote.dto.AllNewsReqDto
import com.example.eki.feature_news.data.remote.dto.AllNewsResDto
import com.example.eki.feature_news.domain.repository.NewsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetAllNewsUseCase @Inject constructor(
    private val newsRepository: NewsRepository,
    private val rescuerRepository: RescuerRepository,
) {

    operator fun invoke(
        newsType: String = "",
        searchPhrase: String = "",
    ): Flow<ResponseHandler<List<AllNewsResDto>>> {
        val allNewsReqDto = AllNewsReqDto(
            personType = "Organization",
            newsTypeId = newsType,
            searchPhrase = searchPhrase
        )

        return ResponseHandler.responseHandlerFlow {
            newsRepository.getAllNews(allNewsReqDto, rescuerRepository.getToken())
        }
    }
}