package com.example.eki.feature_news.di

import com.example.eki.core.di.FakeRepository
import com.example.eki.feature_news.data.remote.NewsApi
import com.example.eki.feature_news.data.repository.NewsRepositoryImpl
import com.example.eki.feature_news.data.repository.fake.FakeNewsRepo
import com.example.eki.feature_news.domain.repository.NewsRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object NewsModule {

    @Provides
    @Singleton
    fun provideNewsApi(retrofit: Retrofit): NewsApi = retrofit.create(NewsApi::class.java)

    @Provides
    @Singleton
    fun provideNewsRepository(newsApi: NewsApi): NewsRepository = NewsRepositoryImpl(newsApi)

    @Provides
    @Singleton
    @FakeRepository
    fun provideFakeNewsRepository(): NewsRepository = FakeNewsRepo()
}