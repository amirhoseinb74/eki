package com.example.eki.feature_news.domain.model

data class AllNewsRes(
    val id: String?,
    val title: String,
    val summary: String,
    val newsType: String,
    val date: String,
) {
    companion object {
        val testData = listOf(
            AllNewsRes(
                "1",
                "عنوان تستی ۱",
                "خلاصه خبر تستی 1",
                "0",
                "2023-09-25 11:48:58.0",
            ),
            AllNewsRes(
                "2",
                "عنوان تستی 2",
                "خلاصه خبر تستی ۲",
                "1",
                "2023-09-25 11:48:58.0",
            ),
            AllNewsRes(
                "3",
                "عنوان تستی 3",
                "خلاصه خبر تستی 3",
                "2",
                "2023-09-25 11:48:58.0",
            )
        )
    }
}