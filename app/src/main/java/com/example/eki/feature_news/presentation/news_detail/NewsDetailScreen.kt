package com.example.eki.feature_news.presentation.news_detail

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.eki.R
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.core.presentation.components.HtmlViewer
import com.example.eki.core.presentation.components.ScaffoldState
import com.example.eki.core.util.downloader.open
import com.example.eki.feature_main.presentation.splash.components.DownloadLoadingIndicator
import com.example.eki.feature_news.domain.model.NewsDetailRes
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme
import kotlinx.coroutines.flow.collectLatest

@Composable
fun NewsDetailScreen(newsDetailViewModel: NewsDetailViewModel = hiltViewModel()) {

    val context = LocalContext.current

    ScaffoldState { showMessage ->
        LaunchedEffect(key1 = true) {
            newsDetailViewModel.eventFlow.collectLatest { event ->
                when (event) {
                    is NewsDetailViewModel.NewsDetailUIEvent.OpenDownloadedFile -> {
                        event.downloadedFile.open(context, event.uri)
                    }

                    is NewsDetailViewModel.NewsDetailUIEvent.ShowError -> {
                        showMessage(event.message)
                    }
                }
            }
        }
    }
    NewsDetailScreenContent(
        newsDetailState = newsDetailViewModel.newsDetailState.value,
        sendViewModelEvent = newsDetailViewModel::onEvent,
        isDownloading = newsDetailViewModel.isDownloadingState.value
    )
}

@Composable
fun NewsDetailScreenContent(
    newsDetailState: NewsDetailState,
    sendViewModelEvent: (NewsDetailEvent) -> Unit = {},
    isDownloading: Boolean = false,
) {
    val lifecycleOwner = LocalLifecycleOwner.current

    val bgColor = MaterialTheme.colorScheme.surface
    val contentColor = MaterialTheme.colorScheme.onSurface

    Column(
        Modifier
            .background(bgColor)
            .fillMaxSize(),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        newsDetailState.data?.getOrNull(0)?.let { newsDetail ->

            Text(
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(MaterialTheme.spacing.medium),
                text = newsDetail.title,
                color = contentColor,
                style = MaterialTheme.typography.headlineSmall
            )
            Divider(color = contentColor)
            HtmlViewer(
                modifier = Modifier
                    .padding(MaterialTheme.spacing.medium)
                    .weight(1f)
                    .verticalScroll(rememberScrollState()),
                content = newsDetail.content,
                textColor = contentColor,
                linkTextColor = MaterialTheme.colorScheme.primaryContainer
            )
            if (newsDetail.hasAttachedFile && newsDetail.attachedFileUrl.isNotEmpty()) {
                Divider(color = contentColor, thickness = MaterialTheme.spacing.xSmall)
                CustomButton(
                    modifier = Modifier.padding(vertical = MaterialTheme.spacing.small),
                    text = stringResource(id = R.string.attached_file)
                ) {
                    sendViewModelEvent(
                        NewsDetailEvent.DownloadAttachedFile(
                            newsDetail.attachedFileUrl,
                            newsDetail.fileName,
                            lifecycleOwner
                        )
                    )
                }
            }
        }
    }
    if (isDownloading) {
        DownloadLoadingIndicator(loadingColor = contentColor)
    }
}

@Preview
@Composable
fun NewsDetailScreenPreview() {
    EKITheme {
        NewsDetailScreenContent(
            newsDetailState = NewsDetailState(
                data = listOf(NewsDetailRes.testData)
            )
        )
    }
}