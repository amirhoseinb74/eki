package com.example.eki.feature_news.domain.use_case

import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.example.eki.feature_news.domain.repository.NewsRepository
import javax.inject.Inject

class GetNewsTypeUseCase @Inject constructor(
    private val newsRepository: NewsRepository,
    private val rescuerRepository: RescuerRepository,
) {
    operator fun invoke() = ResponseHandler.responseHandlerFlow {
        newsRepository.getNewsType(rescuerRepository.getToken())
    }
}