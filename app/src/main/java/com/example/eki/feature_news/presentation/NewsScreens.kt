package com.example.eki.feature_news.presentation

sealed class NewsScreens(val route: String) {
    object News : NewsScreens("news_screen")
    object AllNews : NewsScreens("all_news_screen")
    object NewsDetail : NewsScreens("news_detail_screen")

    companion object {
        const val PARAM_NEWS_ID = "newsId"
    }
}
