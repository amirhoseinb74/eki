package com.example.eki.feature_profile.di

import com.example.eki.core.di.FakeRepository
import com.example.eki.feature_profile.data.remote.ProfileApi
import com.example.eki.feature_profile.data.repository.ProfileRepositoryImpl
import com.example.eki.feature_profile.data.repository.fake.FakeProfileRepo
import com.example.eki.feature_profile.domain.repository.ProfileRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object ProfileModule {

    @Provides
    @Singleton
    fun provideProfileApi(retrofit: Retrofit): ProfileApi = retrofit.create(ProfileApi::class.java)

    @Provides
    @Singleton
    fun provideProfileRepository(profileApi: ProfileApi): ProfileRepository =
        ProfileRepositoryImpl(profileApi)

    @Provides
    @Singleton
    @FakeRepository
    fun provideFakeProfileRepository(): ProfileRepository = FakeProfileRepo()
}