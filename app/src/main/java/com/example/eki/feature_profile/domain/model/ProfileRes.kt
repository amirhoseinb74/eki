package com.example.eki.feature_profile.domain.model

data class ProfileRes(
    val address: String,
    val family: String,
    val marketerDesc: String,
    val name: String,
    val nationalCode: String,
    val phone: String,
    val postalCode: String,
    val profileImage: String,
)
