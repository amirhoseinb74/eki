package com.example.eki.feature_profile.domain.repository

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.feature_profile.data.remote.dto.ProfileReqDto
import com.example.eki.feature_profile.data.remote.dto.ProfileResDto

interface ProfileRepository {
    suspend fun getProfileInfo(
        profileReqDto: ProfileReqDto,
        token: String,
    ): BaseResponse<ProfileResDto>
}