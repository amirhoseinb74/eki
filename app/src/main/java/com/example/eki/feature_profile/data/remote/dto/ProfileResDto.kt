package com.example.eki.feature_profile.data.remote.dto

import com.example.eki.feature_profile.domain.model.ProfileRes

data class ProfileResDto(
    val address: String?,
    val family: String?,
    val id: String?,
    val marketerDesc: String?,
    val marketerId: String?,
    val name: String?,
    val nationalCode: String?,
    val personalId: Any?,
    val phone: String?,
    val postalCode: String?,
    val profileImage: String?,
)

fun ProfileResDto.toProfileRes() = ProfileRes(
    address = address.orEmpty(),
    family = family.orEmpty(),
    marketerDesc = marketerDesc.orEmpty(),
    name = name.orEmpty(),
    nationalCode = nationalCode.orEmpty(),
    phone = phone.orEmpty(),
    postalCode = postalCode.orEmpty(),
    profileImage = profileImage.orEmpty()
)