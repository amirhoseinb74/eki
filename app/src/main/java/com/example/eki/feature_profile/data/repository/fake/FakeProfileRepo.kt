package com.example.eki.feature_profile.data.repository.fake

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.Settings
import com.example.eki.feature_profile.data.remote.dto.ProfileReqDto
import com.example.eki.feature_profile.data.remote.dto.ProfileResDto
import com.example.eki.feature_profile.domain.repository.ProfileRepository

class FakeProfileRepo : ProfileRepository {
    override suspend fun getProfileInfo(
        profileReqDto: ProfileReqDto,
        token: String,
    ): BaseResponse<ProfileResDto> {
        return BaseResponse(
            listOf(
                ProfileResDto(
                    ": تهران – یوسف آباد – میدان کلانتری – خیابان ابن سینا – کوچه نسترن – پلاک 18.",
                    "مسعودی",
                    "۲",
                    "",
                    "",
                    "پیمان",
                    "001891454",
                    "354",
                    "0912542165897",
                    "14576852",
                    "https://secure.gravatar.com/avatar/e89584fc717a92922689cb33ab29dd93?s=160&d=https%3A%2F%2Fcss-tricks.com%2Fimages%2Fget-gravatar.png&r=PG"
                )
            ), Settings("ok", "1")
        )
    }
}