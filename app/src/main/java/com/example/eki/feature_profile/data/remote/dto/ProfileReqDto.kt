package com.example.eki.feature_profile.data.remote.dto

data class ProfileReqDto(
    val iOrgId: String,
)