package com.example.eki.feature_profile.presentation

sealed class ProfileScreens(val route: String) {
    object Profile : ProfileScreens("profile_screen")
}
