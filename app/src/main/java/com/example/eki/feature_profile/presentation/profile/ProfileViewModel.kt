package com.example.eki.feature_profile.presentation.profile

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.R
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.Strings
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_profile.data.remote.dto.toProfileRes
import com.example.eki.feature_profile.domain.use_case.GetProfileUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val getProfileUseCase: GetProfileUseCase,
) : ViewModel() {

    private val _profileState = mutableStateOf(ProfileState())
    val profileState: State<ProfileState> = _profileState

    private val _eventFlow = MutableSharedFlow<ProfileUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var getProfileJob: Job? = null

    sealed class ProfileUIEvent {
        data class ShowError(val message: String) : ProfileUIEvent()
    }

    init {
        viewModelScope.launch {
            getProfileData()
        }
    }

    private suspend fun getProfileData() {
        getProfileJob?.cancel()
        getProfileJob = getProfileUseCase().onEach { result ->
            when (result) {
                is ResponseHandler.Loading -> {
                    _profileState.value = ProfileState(isLoading = true)
                }

                is ResponseHandler.Success -> {
                    result.data?.getOrNull(0)?.toProfileRes()?.let { profileRes ->
                        _profileState.value =
                            ProfileState(profileRes = profileRes)
                    } ?: run {
                        _eventFlow.emit(ProfileUIEvent.ShowError(Strings.get(R.string.there_is_no_data)))
                    }
                }

                is ResponseHandler.Error -> {
                    _profileState.value = ProfileState()
                    _eventFlow.emit(
                        ProfileUIEvent.ShowError(
                            result.message ?: ResponseHandler.generalErrorMessage
                        )
                    )
                }
            }.exhaustive
        }.launchIn(viewModelScope)
    }

    override fun onCleared() {
        getProfileJob?.cancel()
        super.onCleared()
    }
}