package com.example.eki.feature_profile.presentation.profile.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.Color.Companion.Black
import androidx.compose.ui.text.font.FontWeight.Companion.Bold
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.ui.spacing

@Composable
fun ProfileItem(label: String, value: String) {

    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        Text(
            text = label,
            style = MaterialTheme.typography.bodyLarge,
            fontWeight = Bold,
            color = Color.Gray
        )
        VerticalSpacer(height = 4)
        Text(
            text = value,
            style = MaterialTheme.typography.bodyMedium,
            color = Black,
            modifier = Modifier.padding(horizontal = MaterialTheme.spacing.medium)
        )
        VerticalSpacer(height = 8)
    }
}