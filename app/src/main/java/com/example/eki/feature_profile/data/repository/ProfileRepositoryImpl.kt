package com.example.eki.feature_profile.data.repository

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.feature_profile.data.remote.ProfileApi
import com.example.eki.feature_profile.data.remote.dto.ProfileReqDto
import com.example.eki.feature_profile.data.remote.dto.ProfileResDto
import com.example.eki.feature_profile.domain.repository.ProfileRepository
import javax.inject.Inject

class ProfileRepositoryImpl @Inject constructor(
    private val profileApi: ProfileApi,
) : ProfileRepository {

    override suspend fun getProfileInfo(
        profileReqDto: ProfileReqDto,
        token: String,
    ): BaseResponse<ProfileResDto> {
        return profileApi.getProfileData(profileReqDto, token)
    }
}