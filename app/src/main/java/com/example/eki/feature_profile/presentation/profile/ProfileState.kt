package com.example.eki.feature_profile.presentation.profile

import com.example.eki.feature_profile.domain.model.ProfileRes

data class ProfileState(
    val isLoading: Boolean = false,
    val profileRes: ProfileRes? = null,
)
