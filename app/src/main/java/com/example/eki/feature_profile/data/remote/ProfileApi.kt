package com.example.eki.feature_profile.data.remote

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.feature_profile.data.remote.dto.ProfileReqDto
import com.example.eki.feature_profile.data.remote.dto.ProfileResDto
import retrofit2.http.Body
import retrofit2.http.POST
import retrofit2.http.Query

interface ProfileApi {

    @POST("org_profile")
    suspend fun getProfileData(
        @Body profileReqDto: ProfileReqDto,
        @Query("access_token") token: String,
    ): BaseResponse<ProfileResDto>
}