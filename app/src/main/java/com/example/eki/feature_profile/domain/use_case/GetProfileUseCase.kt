package com.example.eki.feature_profile.domain.use_case

import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.example.eki.feature_profile.data.remote.dto.ProfileReqDto
import com.example.eki.feature_profile.data.remote.dto.ProfileResDto
import com.example.eki.feature_profile.domain.repository.ProfileRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetProfileUseCase @Inject constructor(
    private val profileRepository: ProfileRepository,
    private val rescuerRepository: RescuerRepository,
) {
    suspend operator fun invoke(): Flow<ResponseHandler<List<ProfileResDto>>> =
        ResponseHandler.responseHandlerFlow {
            profileRepository.getProfileInfo(
                ProfileReqDto(rescuerRepository.getUserId()),
                rescuerRepository.getToken()
            )
        }
}

