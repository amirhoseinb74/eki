package com.example.eki.feature_profile.presentation.profile

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import coil.compose.AsyncImage
import com.example.eki.R
import com.example.eki.core.presentation.components.ScaffoldState
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.core.presentation.components.animation.LoadingAnimation
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_main.presentation.bottom_navigation.MainBottomNavigation
import com.example.eki.feature_profile.domain.model.ProfileRes
import com.example.eki.feature_profile.presentation.profile.components.ProfileItem
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme
import kotlinx.coroutines.flow.collectLatest

@Composable
fun ProfileScreen(
    navController: NavController,
    viewModel: ProfileViewModel = hiltViewModel(),
) {
    ScaffoldState(bottomBar = { MainBottomNavigation(navController) }) { showSnackbar ->
        ProfileScreenContent(viewModel.profileState.value)
        LaunchedEffect(key1 = true) {
            viewModel.eventFlow.collectLatest { profileUIEvent ->
                when (profileUIEvent) {
                    is ProfileViewModel.ProfileUIEvent.ShowError -> {
                        showSnackbar(profileUIEvent.message)
                    }
                }.exhaustive
            }
        }
    }
}

@OptIn(ExperimentalMaterial3Api::class)
@Preview(showBackground = true)
@Composable
fun ProfileScreenContent(
    profileState: ProfileState = ProfileState(
        profileRes = ProfileRes(
            "Tehran",
            "براتعلی",
            "Some Desc",
            "امیرحسین",
            "53698521",
            "09373108622",
            "111489148",
            ""
        )
    ),
) {
    EKITheme {
        val scrollState = rememberScrollState()
        Box(modifier = Modifier.fillMaxSize()) {

            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Top,
                modifier = Modifier
                    .padding(vertical = MaterialTheme.spacing.small)
                    .verticalScroll(scrollState)
                    .fillMaxWidth()
            ) {
                VerticalSpacer(height = 16)
                ElevatedCard(
                    elevation = CardDefaults.cardElevation(MaterialTheme.spacing.medium),
                    modifier = Modifier
                        .size(MaterialTheme.spacing.xxxLarge),
                    shape = CircleShape
                ) {
                    val profileImage: String? = profileState.profileRes?.profileImage
                    if (profileImage.isNullOrEmpty()) {
                        Image(
                            painter = painterResource(id = R.drawable.ic_boy),
                            contentDescription = "Profile image",
                            modifier = Modifier.fillMaxSize()
                        )
                    } else {
                        AsyncImage(
                            model = profileImage,
                            contentDescription = "Profile image",
                            modifier = Modifier.fillMaxSize()
                        )
                    }
                }
                profileState.profileRes?.apply {
                    VerticalSpacer(height = 10)

                    ProfileItem(label = stringResource(R.string.name), value = name)
                    ProfileItem(label = stringResource(R.string.last_name), value = family)
                    ProfileItem(
                        label = stringResource(R.string.national_code),
                        value = nationalCode
                    )
                    ProfileItem(label = stringResource(R.string.phone_number), value = phone)
                    ProfileItem(
                        label = stringResource(R.string.postal_code),
                        value = postalCode
                    )
                    ProfileItem(label = stringResource(R.string.address), value = address)
                    ProfileItem(
                        label = stringResource(id = R.string.description),
                        value = marketerDesc
                    )
                }
            }
        }
        if (profileState.isLoading) {
            LoadingAnimation()
        }
    }
}