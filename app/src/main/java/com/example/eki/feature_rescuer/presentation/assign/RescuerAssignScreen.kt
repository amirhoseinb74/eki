package com.example.eki.feature_rescuer.presentation.assign

import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.viewinterop.AndroidView
import com.example.eki.core.presentation.components.map.MapboxComposable
import com.example.eki.core.util.Constants
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style

@Composable
fun RescuerAssignScreen() {
    MapboxComposable(Modifier.fillMaxSize(), LatLng(35.72411752928713,51.43140570767241))
}