package com.example.eki.feature_rescuer.presentation.waiting

import com.example.eki.R
import com.example.eki.feature_cartable.presentation.CartableScreens

data class RescuerWaitingItem(
    val titleId: Int,
    val painterId: Int,
    val contentDescription: String? = null,
    val route: String? = null,
) {
    companion object {
        val rescuerWaitingItems = listOf(
            RescuerWaitingItem(
                titleId = R.string.piece_store,
                painterId = R.drawable.ic_store,
                contentDescription = "Cartable",
                route = CartableScreens.Cartable.route
            ),
            RescuerWaitingItem(
                titleId = R.string.cartable,
                painterId = R.drawable.ic_case,
                contentDescription = "Cartable",
                route = CartableScreens.Cartable.route
            ),
            RescuerWaitingItem(
                titleId = R.string.piece_barn,
                painterId = R.drawable.ic_barn,
                contentDescription = "Cartable",
                route = CartableScreens.Cartable.route
            ),
            RescuerWaitingItem(
                titleId = R.string.register_request,
                painterId = R.drawable.ic_registration,
                contentDescription = "Cartable",
                route = CartableScreens.Cartable.route
            ),
            RescuerWaitingItem(
                titleId = R.string.search,
                painterId = R.drawable.ic_search,
                contentDescription = "Cartable",
                route = CartableScreens.Cartable.route
            ),
        )
    }
}

