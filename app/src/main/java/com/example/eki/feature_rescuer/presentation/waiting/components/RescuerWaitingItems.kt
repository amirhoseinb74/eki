package com.example.eki.feature_rescuer.presentation.waiting.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.core.presentation.components.WindowInfo
import com.example.eki.core.presentation.components.rememberWindowInfo
import com.example.eki.feature_rescuer.presentation.waiting.RescuerWaitingItem
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
fun RescuerWaitingItems(
    modifier: Modifier = Modifier,
    waitingItems: List<RescuerWaitingItem>,
    onWaitingItemClick: (RescuerWaitingItem) -> Unit,
) {
    val windowInfo = rememberWindowInfo()

    val gridCells = if (windowInfo.screenWidthInfo == WindowInfo.WindowType.Expanded) 5 else 2
    LazyVerticalGrid(
        modifier = modifier
            .fillMaxWidth()
            .padding(horizontal = MaterialTheme.spacing.small),
        columns = GridCells.Fixed(gridCells),

        ) {
        items(waitingItems.size) { index ->
            val currentWaitingItem = waitingItems[index]
            CustomButton(
                modifier = Modifier.padding(horizontal = MaterialTheme.spacing.xSmall),
                text = stringResource(currentWaitingItem.titleId),
                imageResource = currentWaitingItem.painterId,
                onClick = { onWaitingItemClick(currentWaitingItem) }
            )

        }
    }
}

@Preview(device = Devices.PHONE)
@Composable
fun RescuerWaitingItemsPreview() {
    EKITheme {
        RescuerWaitingItems(
            waitingItems = RescuerWaitingItem.rescuerWaitingItems,
            onWaitingItemClick = {}
        )
    }
}