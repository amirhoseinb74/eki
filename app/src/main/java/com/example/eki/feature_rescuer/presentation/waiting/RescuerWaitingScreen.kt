package com.example.eki.feature_rescuer.presentation.waiting

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.navigation.NavController
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.core.presentation.components.animation.GoingCarAnimation
import com.example.eki.feature_rescuer.presentation.waiting.components.RescuerWaitingItems
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
fun RescuerWaitingScreen(
    navController: NavController,
) {
    RescuerWaitingScreenContent(onWaitingItemClick = { waitingItem ->
        waitingItem.route?.let { route ->
            navController.navigate(route)
        }
    })
}

@Composable
fun RescuerWaitingScreenContent(
    onWaitingItemClick: (RescuerWaitingItem) -> Unit,
) {

    val backgroundColor = MaterialTheme.colorScheme.surface
    val verticalGradient = Brush.verticalGradient(
        listOf(
            backgroundColor.copy(blue = 0.5f),
            backgroundColor.copy(blue = 0.5f),
            Color.Gray,
            backgroundColor.copy(blue = 0.5f),
            backgroundColor.copy(blue = 0.5f),
        )
    )
    val waitingItems = RescuerWaitingItem.rescuerWaitingItems
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(verticalGradient)
    ) {
        GoingCarAnimation(
            modifier = Modifier
                .align(Alignment.Center)
                .padding(horizontal = MaterialTheme.spacing.large)
        )
        VerticalSpacer(height = 10)
        RescuerWaitingItems(
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(bottom = MaterialTheme.spacing.small),
            waitingItems,
            onWaitingItemClick
        )
    }
}


@Preview
@Composable
fun RescuerWaitingScreenPreview() {
    EKITheme {
        RescuerWaitingScreenContent(onWaitingItemClick = {})
    }
}