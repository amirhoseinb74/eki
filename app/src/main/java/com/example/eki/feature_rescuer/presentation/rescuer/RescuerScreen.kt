package com.example.eki.feature_rescuer.presentation.rescuer

import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.eki.feature_main.presentation.home.WorkOrderStatus
import com.example.eki.feature_rescuer.presentation.assign.RescuerAssignScreen

@Composable
fun RescuerScreen(
    navController: NavController,
    rescuerViewModel: RescuerViewModel = hiltViewModel(),
) {

    when (rescuerViewModel.currentState.value) {
        WorkOrderStatus.NOT_STARTED,
        WorkOrderStatus.STARTED,
        -> {
            RescuerAssignScreen()
        }

        WorkOrderStatus.SIGN -> {
            Text(text = "sign", color = Color.Black)
        }

        WorkOrderStatus.PAYMENT -> {
            Text(text = "payment", color = Color.Black)
        }

        WorkOrderStatus.FINISHED,
        WorkOrderStatus.CANCELED,
        WorkOrderStatus.REJECTED,
        null,
        -> {
            RescuerAssignScreen()
//            RescuerWaitingScreen(navController = navController)
        }

    }
}