package com.example.eki.feature_rescuer.presentation

sealed class RescuerScreens(val route: String) {
    object Rescuer : RescuerScreens("rescuer_screen")

    companion object {
        const val PARAM_INITIAL_STATUS = "initialStatus"
    }
}