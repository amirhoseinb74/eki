package com.example.eki.feature_rescuer.presentation.rescuer

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.SavedStateHandle
import com.example.eki.feature_main.presentation.home.WorkOrderStatus
import com.example.eki.feature_main.presentation.last_work_order_service.LastWorkOrderService
import com.example.eki.feature_rescuer.presentation.RescuerScreens
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class RescuerViewModel @Inject constructor(
    private val application: Application,
    savedStateHandle: SavedStateHandle,
) : AndroidViewModel(application) {

    private val _currentState = mutableStateOf<WorkOrderStatus?>(null)
    val currentState: State<WorkOrderStatus?> = _currentState

    private val lastWorkOrderStatusReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            _currentState.value =
                intent?.getStringExtra(LastWorkOrderService.KEY_LAST_WORK_ORDER_STATUS)
                    ?.let { lastStatus ->
                        WorkOrderStatus.valueOf(lastStatus)
                    }
        }
    }

    init {
        savedStateHandle.get<String>(RescuerScreens.PARAM_INITIAL_STATUS)
            ?.takeIf { it != "null" }
            ?.let { initialState ->
                _currentState.value = WorkOrderStatus.valueOf(initialState)
            }

        val filter = IntentFilter(LastWorkOrderService.ACTION_BROADCAST_LAST_WORK_ORDER_DATA)
        ContextCompat.registerReceiver(
            application,
            lastWorkOrderStatusReceiver,
            filter,
            ContextCompat.RECEIVER_NOT_EXPORTED
        )
    }

    override fun onCleared() {
        application.unregisterReceiver(lastWorkOrderStatusReceiver)
        super.onCleared()
    }
}