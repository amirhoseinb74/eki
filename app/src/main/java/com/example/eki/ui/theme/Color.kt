package com.example.eki.ui.theme

import androidx.compose.ui.graphics.Color

val TextWhite = Color(0xffeeeeee)
val ButtonBlue = Color(0xFF0D47A1)
val OrangeYellow1 = Color(0xfff0bd28)

val DarkGray = Color(0xFF202020)
val MediumGray = Color(0xFF696666)
val LightGray = Color(0xFFA2A2A2)
val ColorPrimary = Color(0xFFFFC107)

val Green10 = Color(0xff003314)
val Green20 = Color(0xff006627)
val Green30 = Color(0xff00993b)
val Green40 = Color(0xff00cc4e)
val Green80 = Color(0xff99ffc0)
val Green90 = Color(0xffccffe0)

val DarkGreen10 = Color(0xff0d260d)
val DarkGreen20 = Color(0xff194d19)
val DarkGreen30 = Color(0xff267326)
val DarkGreen40 = Color(0xff339933)
val DarkGreen80 = Color(0xffb3e6b3)
val DarkGreen90 = Color(0xffd9f2d9)

val Violet10 = Color(0xff330033)
val Violet20 = Color(0xff660066)
val Violet30 = Color(0xff990099)
val Violet40 = Color(0xffcc00cc)
val Violet80 = Color(0xffff99ff)
val Violet90 = Color(0xffffccff)

val Red10 = Color(0xFF410001)
val Red20 = Color(0xFF680003)
val Red30 = Color(0xFF930006)
val Red40 = Color(0xFFBA1B1B)
val Red80 = Color(0xFFFFB4A9)
val Red90 = Color(0xFFFFDAD4)

val Grey10 = Color(0xFF191C1D)
val Grey20 = Color(0xFF2D3132)
val Grey90 = Color(0xFFE0E3E3)
val Grey95 = Color(0xFFEFF1F1)
val Grey99 = Color(0xFFFBFDFD)

val GreenGrey30 = Color(0xFF316847)
val GreenGrey50 = Color(0xFF52ad76)
val GreenGrey60 = Color(0xFF74be92)
val GreenGrey80 = Color(0xFFbadec8)
val GreenGrey90 = Color(0xFFdcefe4)

val Yellow30 = Color(0xFF999900)
val Yellow40 = Color(0xffcccc00)
val Yellow50 = Color(0xFFFFFF00)
val Yellow60 = Color(0xFFffff33)
val Yellow70 = Color(0xFFffff66)
val Yellow80 = Color(0xFFffff99)

val DarkerYellow50 = Color(0xffffcc00)

val Blue30 = Color(0xFF000099)
val Blue40 = Color(0xFF0000cc)
val Blue50 = Color(0xFF0000ff)
val Blue60 = Color(0xFF3333ff)
val Blue70 = Color(0xFF6666ff)
val Blue80 = Color(0xFF9999ff)

val BlueBespor = Color(0xFF194052)

val Orange = Color(0xFFF86F03)

val BlueSky = Color(0xFF75C2F6)
