package com.example.eki.ui.theme

import androidx.compose.material3.Typography
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.sp
import com.example.eki.R

// Set of Material typography styles to start with
val Typography = Typography(
    headlineLarge = TextStyle(
        fontWeight = FontWeight.Bold,
        fontSize = 30.sp,
        fontFamily = FontFamily(Font(R.font.iransans_medium)),
        color = TextWhite
    ),
    headlineMedium = TextStyle(
        fontWeight = FontWeight.SemiBold,
        fontSize = 24.sp,
        fontFamily = FontFamily(Font(R.font.iransans_medium)),
        color = TextWhite
    ),
    headlineSmall = TextStyle(
        fontWeight = FontWeight.Medium,
        fontSize = 20.sp,
        fontFamily = FontFamily(Font(R.font.iransans_medium)),
        color = TextWhite
    ),
    titleSmall = TextStyle(
        fontWeight = FontWeight.Medium,
        fontSize = 17.sp,
        fontFamily = FontFamily(Font(R.font.iransans_medium)),
        color = TextWhite
    ),
    bodyLarge = TextStyle(
        fontWeight = FontWeight.Medium,
        fontSize = 16.sp,
        fontFamily = FontFamily(Font(R.font.iransans_medium)),
        color = TextWhite
    ),
    bodyMedium = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 13.sp,
        fontFamily = FontFamily(Font(R.font.iransans_medium)),
        color = TextWhite
    ),
    bodySmall = TextStyle(
        fontWeight = FontWeight.Normal,
        fontSize = 10.sp,
        fontFamily = FontFamily(Font(R.font.iransans_medium)),
        color = TextWhite
    )
)