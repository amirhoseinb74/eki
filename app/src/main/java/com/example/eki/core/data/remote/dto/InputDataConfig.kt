package com.example.eki.core.data.remote.dto

data class InputDataConfig(
    val filters: List<RequestFilter>,
    val pagination: Int = 1,
    val maxResults: Int = 100,
    val token: String,
)
