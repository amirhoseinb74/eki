package com.example.eki.core.util.downloader

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.widget.Toast
import androidx.core.content.ContextCompat

data class DownloadFile(
    val name: String,
    val type: FileType,
    val url: String,
)

fun DownloadFile.open(context: Context, uri: Uri) {
    val intent = Intent(Intent.ACTION_VIEW)
    intent.setDataAndType(uri, type.value)
    intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
    try {
        ContextCompat.startActivity(context, intent, null)
    } catch (e: ActivityNotFoundException) {
        Toast.makeText(
            context,
            "Can't open ${type.name} File",
            Toast.LENGTH_SHORT
        ).show()
    }
}

