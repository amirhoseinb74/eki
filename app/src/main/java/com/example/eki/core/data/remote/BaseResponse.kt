package com.example.eki.core.data.remote

class BaseResponse<T>(
    val data: List<T>?,
    val settings: Settings?,
) {
    fun isSuccessful() =
        settings?.success?.let { success ->
            success == "1"
        } ?: false
}