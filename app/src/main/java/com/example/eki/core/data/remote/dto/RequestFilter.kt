package com.example.eki.core.data.remote.dto

data class RequestFilter(
    val field: String,
    val search: String,
    val type: String = "equal",
)
