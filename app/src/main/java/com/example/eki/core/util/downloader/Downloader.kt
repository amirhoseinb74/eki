package com.example.eki.core.util.downloader

import androidx.lifecycle.LifecycleOwner
import androidx.work.Constraints
import androidx.work.Data
import androidx.work.ExistingWorkPolicy
import androidx.work.NetworkType
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkInfo
import androidx.work.WorkManager
import com.example.eki.EKIApplication
import com.example.eki.core.util.downloader.DownloadWorker.Companion.KEY_FILE_NAME
import com.example.eki.core.util.downloader.DownloadWorker.Companion.KEY_FILE_TYPE
import com.example.eki.core.util.downloader.DownloadWorker.Companion.KEY_FILE_URI
import com.example.eki.core.util.downloader.DownloadWorker.Companion.KEY_FILE_URL

object Downloader {

    fun downloadFile(
        file: DownloadFile,
        lifecycleOwner: LifecycleOwner,
        success: (String) -> Unit,
        failed: () -> Unit,
        running: () -> Unit,
    ) {

        val data = Data.Builder()
        val workManager = WorkManager.getInstance(EKIApplication.instance)

        data.apply {
            putString(KEY_FILE_NAME, file.name)
            putString(KEY_FILE_URL, file.url)
            putString(KEY_FILE_TYPE, file.type.name)
        }

        val constraints =
            Constraints.Builder()
                .setRequiredNetworkType(NetworkType.CONNECTED)
                .setRequiresStorageNotLow(true)
                .setRequiresBatteryNotLow(true)
                .build()

        val downloadWorker =
            OneTimeWorkRequestBuilder<DownloadWorker>()
                .setConstraints(constraints)
                .setInputData(data.build())
                .build()

        val workInfoByIdLiveData = workManager.getWorkInfoByIdLiveData(downloadWorker.id)

        workManager.enqueueUniqueWork(
            "oneFileDownloadWork_${System.currentTimeMillis()}",
            ExistingWorkPolicy.KEEP,
            downloadWorker
        )

        workInfoByIdLiveData.observe(lifecycleOwner) { info ->
            info?.let { workInfo ->
                when (workInfo.state) {

                    WorkInfo.State.ENQUEUED, WorkInfo.State.RUNNING -> {
                        running()
                    }

                    WorkInfo.State.SUCCEEDED -> {
                        val uri = workInfo.outputData.getString(KEY_FILE_URI).orEmpty()
                        success(uri)
                    }

                    else -> failed()
                }
            }
        }
    }
}