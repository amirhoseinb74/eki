package com.example.eki.core.presentation.components

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.gestures.detectTapGestures
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Search
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.rememberUpdatedState
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.BlueBespor
import com.example.eki.ui.theme.EKITheme

@OptIn(ExperimentalMaterial3Api::class, ExperimentalComposeUiApi::class)
@Composable
fun CustomSearchView(
    modifier: Modifier = Modifier,
    containerColor: Color = BlueBespor,
    contentColor: Color = Color.White,
    borderColor: Color = Color.Yellow,
    onSearchTextChanged: (String) -> Unit = {},
    onSearchClick: (String) -> Unit = {},
) {

    var text by rememberSaveable { mutableStateOf("") }
    val hint = stringResource(id = R.string.search)
    val keyboardController = LocalSoftwareKeyboardController.current

    Box(
        modifier = modifier
            .clip(CircleShape)
            .border(
                BorderStroke(MaterialTheme.spacing.xxSmall, borderColor),
                shape = CircleShape
            )
            .background(containerColor)
    ) {
        val density = LocalDensity.current.density
        val updatedText by rememberUpdatedState(text)

        Box(
            modifier = Modifier.pointerInput(Unit) {
                detectTapGestures { offset ->
                    val densityOffset = offset / density
                    if (densityOffset.x < 0 || densityOffset.x > 300 || densityOffset.y < 0 || densityOffset.y > 100) {
                        keyboardController?.hide()
                    }
                }
            }
        ) {
            TextField(
                value = updatedText,
                onValueChange = {
                    text = it
                    onSearchTextChanged(it)
                },
                leadingIcon = {
                    Icon(
                        modifier = Modifier.clickable {
                            onSearchClick(text)
                            keyboardController?.hide()
                        },
                        imageVector = Icons.Default.Search,
                        contentDescription = hint
                    )
                },
                placeholder = { Text(text = hint) },
                singleLine = true,
                keyboardActions = KeyboardActions(onSearch = {
                    onSearchClick(text)
                    keyboardController?.hide()
                }),
                keyboardOptions = KeyboardOptions.Default.copy(imeAction = ImeAction.Search),
                colors = TextFieldDefaults.textFieldColors(
                    containerColor = containerColor,
                    placeholderColor = contentColor,
                    disabledLeadingIconColor = contentColor,
                    unfocusedLeadingIconColor = contentColor,
                    focusedLeadingIconColor = borderColor,
                    disabledTrailingIconColor = contentColor,
                    textColor = contentColor,
                    cursorColor = contentColor,
                ),
            )
        }
    }
}

@Preview
@Composable
fun CustomSearchViewPreview() {
    EKITheme {
        CustomSearchView()
    }
}