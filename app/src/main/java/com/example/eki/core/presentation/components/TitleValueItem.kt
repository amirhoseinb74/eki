package com.example.eki.core.presentation.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Info
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
fun TitleValueItem(
    modifier: Modifier = Modifier,
    containerColor: Color = MaterialTheme.colorScheme.primaryContainer,
    contentColor: Color = MaterialTheme.colorScheme.onPrimaryContainer,
    hasIcon: Boolean = true,
    icon: ImageVector = Icons.Default.Info,
    title: String?,
    value: String?,
) {
    if (!value.isNullOrEmpty() && value != "-") {
        Row(
            modifier = modifier
                .fillMaxWidth()
                .padding(
                    horizontal = MaterialTheme.spacing.small,
                    vertical = MaterialTheme.spacing.xxxSmall
                )
                .background(containerColor, shape = MaterialTheme.shapes.small),
            verticalAlignment = Alignment.CenterVertically
        ) {
            if (hasIcon) {
                Icon(
                    modifier = Modifier.padding(vertical = MaterialTheme.spacing.xSmall),
                    imageVector = icon,
                    contentDescription = "item icon",
                    tint = contentColor
                )
                HorizontalSpacer(width = 3)
            }
            Text(
                text = "$title :",
                style = MaterialTheme.typography.bodyMedium,
                fontWeight = FontWeight.Bold,
                color = contentColor
            )
            HorizontalSpacer(width = 6)
            Text(
                text = value,
                style = MaterialTheme.typography.bodyMedium,
                color = contentColor
            )

        }

    }
}


@Preview
@Composable
fun TitleValueItemPreview() {
    EKITheme {
        TitleValueItem(title = "عنوان", value = "مقدار")
    }
}