package com.example.eki.core.util

import androidx.annotation.StringRes
import com.example.eki.EKIApplication

object Strings {
    fun get(@StringRes stringRes: Int, vararg formatArgs: Any = emptyArray()) =
        EKIApplication.instance.getString(stringRes, *formatArgs)
}