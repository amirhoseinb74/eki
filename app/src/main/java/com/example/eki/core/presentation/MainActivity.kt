package com.example.eki.core.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.navigation
import androidx.navigation.compose.rememberNavController
import com.example.eki.feature_cartable.presentation.CartableScreens
import com.example.eki.feature_cartable.presentation.financial_history.FinancialHistoryScreen
import com.example.eki.feature_cartable.presentation.functionality_history.FunctionalityHistoryScreen
import com.example.eki.feature_cartable.presentation.rescuer_history.RescuerHistoryScreen
import com.example.eki.feature_cartable.presentation.sale_history.SaleHistoryScreen
import com.example.eki.feature_cartable.presentation.shift_history.ShiftHistoryScreen
import com.example.eki.feature_main.presentation.MainScreens
import com.example.eki.feature_main.presentation.home.HomeScreen
import com.example.eki.feature_main.presentation.login.LoginScreen
import com.example.eki.feature_main.presentation.splash.SplashScreen
import com.example.eki.feature_news.presentation.NewsScreens
import com.example.eki.feature_news.presentation.news_detail.NewsDetailScreen
import com.example.eki.feature_news.presentation.news_list.NewsListScreen
import com.example.eki.feature_profile.presentation.ProfileScreens
import com.example.eki.feature_profile.presentation.profile.ProfileScreen
import com.example.eki.feature_rescuer.presentation.RescuerScreens
import com.example.eki.feature_rescuer.presentation.rescuer.RescuerScreen
import com.example.eki.ui.theme.EKITheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EKITheme {
                val navController = rememberNavController()

                NavHost(
                    navController = navController,
                    startDestination = MainScreens.Home.route
                ) {
                    composable(route = MainScreens.Splash.route) {
                        SplashScreen(navController)
                    }
                    composable(route = MainScreens.Login.route) {
                        LoginScreen(navController)
                    }
                    composable(route = MainScreens.Home.route) {
                        HomeScreen(navController)
                    }
                    composable(route = ProfileScreens.Profile.route) {
                        ProfileScreen(navController)
                    }

                    //nested navigation for cartable screens
                    navigation(
                        startDestination = CartableScreens.RescuerHistory.route,
                        route = CartableScreens.Cartable.route
                    ) {
                        composable(route = CartableScreens.RescuerHistory.route) {
                            RescuerHistoryScreen(navController)
                        }
                        composable(route = CartableScreens.ShiftHistory.route) {
                            ShiftHistoryScreen(navController)
                        }
                        composable(route = CartableScreens.FinancialHistory.route) {
                            FinancialHistoryScreen(navController)
                        }
                        composable(route = CartableScreens.SaleHistory.route) {
                            SaleHistoryScreen(navController)
                        }
                        composable(route = CartableScreens.FunctionalityHistory.route) {
                            FunctionalityHistoryScreen(navController)
                        }
                    }

                    //nested navigation for news screens
                    navigation(
                        startDestination = NewsScreens.AllNews.route,
                        route = NewsScreens.News.route
                    ) {
                        composable(route = NewsScreens.AllNews.route) {
                            NewsListScreen(navController = navController)
                        }

                        composable(
                            route = "${NewsScreens.NewsDetail.route}/{${NewsScreens.PARAM_NEWS_ID}}"
                        ) {
                            NewsDetailScreen()
                        }
                    }

                    composable(
                        route =
                        "${RescuerScreens.Rescuer.route}/{${RescuerScreens.PARAM_INITIAL_STATUS}}"
                    ) {
                        RescuerScreen(navController)
                    }


                }
            }
        }
    }
}