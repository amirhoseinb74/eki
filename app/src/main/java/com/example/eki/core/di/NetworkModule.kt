package com.example.eki.core.di

import android.app.Application
import android.util.Base64
import com.example.eki.BuildConfig
import com.example.eki.core.util.Constants
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.security.SecureRandom
import java.security.cert.CertificateException
import java.security.cert.X509Certificate
import java.util.concurrent.TimeUnit
import javax.inject.Provider
import javax.inject.Singleton
import javax.net.ssl.HostnameVerifier
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun provideGson() = Gson()

    @Provides
    @Singleton
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory =
        GsonConverterFactory.create(gson)


    @Provides
    fun provideTrustManagerCertificates(): Array<TrustManager> {
        return arrayOf(
            object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {
                }

                override fun getAcceptedIssuers(): Array<X509Certificate> {
                    return arrayOf()
                }
            }
        )
    }

    @Provides
    fun provideSSLContext(cert: Array<TrustManager>): SSLContext {
        val ctx = SSLContext.getInstance("SSL")
        ctx.init(null, cert, SecureRandom())

        return ctx
    }


    @Provides
    fun provideHostnameVerifier() = HostnameVerifier { _, _ -> true }

    @Provides
    fun provideOkHttpClientCache(application: Application): Cache {
        val cacheSize = 10 * 1024 * 1024 // 10 MiB
        return Cache(application.cacheDir, cacheSize.toLong())
    }

    @Provides
    fun provideClientInterceptor(): HttpLoggingInterceptor {
        val interceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) interceptor.setLevel(HttpLoggingInterceptor.Level.BODY) else interceptor.setLevel(
            HttpLoggingInterceptor.Level.NONE
        )
        return interceptor
    }

    @Provides
    @Singleton
    fun provideInterceptor(
        rescuerRepository: Provider<RescuerRepository>,
        @ApplicationScope applicationScope: CoroutineScope,
    ): Interceptor {
        var token = ""
        var userId = ""
        applicationScope.launch {
            token = rescuerRepository.get().getToken()
            userId = rescuerRepository.get().getUserId()
        }
        return Interceptor { chain: Interceptor.Chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .header(Constants.NAME_CONTENT_TYPE, "application/json")
                .header(Constants.NAME_ACCEPT, "application/json")
                .header(Constants.HEADER_ORG_ID, userId)
                .header(Constants.TOKEN_HEADER, token)
                .header(
                    Constants.NAME_AUTHORIZATION,
                    "Basic " + Base64.encodeToString(
                        "devglan-client:devglan-secret".toByteArray(),
                        Base64.NO_WRAP
                    )
                )
                .header("lang", "per")

            val request: Request = requestBuilder.build()
            chain.proceed(request)
        }
    }

    @Provides
    fun provideOkHttpClient(
        cache: Cache,
        httpLoggingInterceptor: HttpLoggingInterceptor,
        sslContext: SSLContext,
        trustManagers: Array<TrustManager>,
        hostnameVerifier: HostnameVerifier,
        interceptor: Interceptor,
    ): OkHttpClient {
        val timeoutSeconds = /*if (BuildConfig.DEBUG) 5L else */240L
        return OkHttpClient.Builder()
            .cache(cache)
            .readTimeout(timeoutSeconds, TimeUnit.SECONDS)
            .writeTimeout(timeoutSeconds, TimeUnit.SECONDS)
            .connectTimeout(timeoutSeconds, TimeUnit.SECONDS)
            .retryOnConnectionFailure(true)
            .addInterceptor(interceptor)
            .addInterceptor(httpLoggingInterceptor)
            .sslSocketFactory(sslContext.socketFactory, trustManagers[0] as X509TrustManager)
            .hostnameVerifier(hostnameVerifier)
            .build()
    }


    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient, factory: GsonConverterFactory): Retrofit =
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(factory)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
}