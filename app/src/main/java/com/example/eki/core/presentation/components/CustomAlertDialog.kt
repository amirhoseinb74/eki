package com.example.eki.core.presentation.components

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.ui.theme.EKITheme

@Composable
fun CustomAlertDialog(
    modifier: Modifier = Modifier,
    title: String,
    message: String,
    onConfirm: () -> Unit,
    onDismiss: () -> Unit,
    onDismissRequest: () -> Unit = {},
) {
    AlertDialog(
        modifier = modifier,
        containerColor = MaterialTheme.colorScheme.surface,
        onDismissRequest = onDismissRequest,
        title = {
            Text(
                text = title,
                style = MaterialTheme.typography.headlineMedium,
                color = MaterialTheme.colorScheme.onSurface,
                modifier = Modifier.fillMaxWidth()
            )
        },
        text = {
            Text(
                text = message,
                style = MaterialTheme.typography.bodyMedium,
                color = MaterialTheme.colorScheme.onSurface,
                modifier = Modifier.fillMaxWidth()
            )
        },
        confirmButton = {
            CustomButton(
                text = stringResource(id = R.string.confirm),
                onClick = onConfirm
            )
        },
        dismissButton = {
            CustomButton(text = stringResource(id = R.string.dismiss), onClick = onDismiss)
        }
    )
}

@Preview
@Composable
fun PreviewCustomAlertDialog() {
    EKITheme {
        CustomAlertDialog(
            title = "عنوان",
            message = "پیام",
            onConfirm = {},
            onDismiss = {})
    }
}
