package com.example.eki.core.util

import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

object EncryptionUtils {

    fun getMD5(message: String): String {
        return try {
            val md = MessageDigest.getInstance("MD5")
            val array = md.digest(message.toByteArray())
            val sb = StringBuffer()
            for (i in array.indices) {
                sb.append(
                    Integer.toHexString(array[i].toInt() and 0xFF or 0x100).substring(1, 3)
                )
            }
            sb.toString()
        } catch (e: NoSuchAlgorithmException) {
            message
        }
    }

}