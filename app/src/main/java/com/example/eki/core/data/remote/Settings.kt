package com.example.eki.core.data.remote

data class Settings(
    val message: String?,
    val success: String?,
)