package com.example.eki.core.util.mapbox

import android.graphics.drawable.Icon
import com.mapbox.mapboxsdk.annotations.Marker
import com.mapbox.mapboxsdk.annotations.MarkerOptions
import com.mapbox.mapboxsdk.camera.CameraPosition
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapboxMap

fun MapboxMap.moveCameraToLocation(latLng: LatLng, zoom: Int, haveAnimation: Boolean = false) {
    val cameraPosition = CameraPosition.Builder()
        .target(LatLng(latLng.latitude, latLng.longitude))
        .zoom(zoom.toDouble())
        .build()

    val cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition)
    if (haveAnimation) animateCamera(cameraUpdate) else moveCamera(cameraUpdate)
}

fun MapboxMap.addMarker(latLng: LatLng, title: String? = null, snippet: String? = null) {

}
