package com.example.eki.core.data.remote

class BaseResponseSingleData<T>(
    val data: T?,
    val settings: Settings?,
) {
    fun isSuccessful() =
        settings?.success?.let { success ->
            success == "1"
        } ?: false
}