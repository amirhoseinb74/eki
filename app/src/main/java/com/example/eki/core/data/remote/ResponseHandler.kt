package com.example.eki.core.data.remote

import com.example.eki.R
import com.example.eki.core.util.Strings
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

sealed class ResponseHandler<T>(val data: T? = null, val message: String? = null) {
    class Success<T>(data: T) : ResponseHandler<T>(data)
    class Error<T>(message: String, data: T? = null) : ResponseHandler<T>(data, message)
    class Loading<T>(data: T? = null) : ResponseHandler<T>(data)

    companion object {
        val generalErrorMessage = Strings.get(R.string.something_went_wrong)
        fun <Res> responseHandlerFlow(
            dataNeeded: Boolean = true,
            onSuccessExtra: suspend (List<Res>) -> Unit = {},
            callApi: suspend () -> BaseResponse<Res>,
        ): Flow<ResponseHandler<List<Res>>> = flow {
            try {
                emit(Loading())
                val response = callApi()
                if (response.isSuccessful()) {
                    response.data?.isNotEmpty()?.let {
                        emit(Success(response.data))
                        onSuccessExtra(response.data)
                    } ?: run {
                        if (dataNeeded)
                            emit(Error(Strings.get(R.string.there_is_no_data)))
                        else
                            emit(Success(emptyList()))
                    }


                } else {
                    emit(
                        Error(
                            response.settings?.message ?: generalErrorMessage
                        )
                    )
                }
            } catch (e: HttpException) {
                // If we get response with code that dose not start with 2 :)
                emit(Error(e.localizedMessage ?: Strings.get(R.string.unexpected_error)))
            } catch (e: IOException) {
                // If our repo or api can not talk to actual remote api; like when internet is not connected
                emit(Error(Strings.get(R.string.check_internet_connection)))
            } catch (e: SocketTimeoutException) {
                // If the request times out (can also be due to no internet connection)
                emit(Error(Strings.get(R.string.check_internet_connection)))
            }
        }

        fun <Res> responseHandlerFlowSingleData(
            callApi: suspend () -> BaseResponseSingleData<Res>,
        ): Flow<ResponseHandler<Res>> = flow {
            try {
                emit(Loading())
                val response = callApi()
                if (response.isSuccessful()) {
                    response.data?.let { data ->
                        emit(Success(data))
                    } ?: emit(Error(Strings.get(R.string.there_is_no_data)))
                } else {
                    emit(
                        Error(
                            response.settings?.message ?: Strings.get(R.string.something_went_wrong)
                        )
                    )
                }
            } catch (e: HttpException) {
                // If we get response with code that dose not start with 2 :)
                emit(Error(e.localizedMessage ?: Strings.get(R.string.unexpected_error)))
            } catch (e: IOException) {
                // If our repo or api can not talk to actual remote api; like when internet is not connected
                emit(Error(Strings.get(R.string.check_internet_connection)))
            } catch (e: SocketTimeoutException) {
                // If the request times out (can also be due to no internet connection)
                emit(Error(Strings.get(R.string.check_internet_connection)))
            }
        }
    }
}

