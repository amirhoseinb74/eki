package com.example.eki.core.util.downloader

enum class FileType(val value: String) {
    PDF("application/pdf"),
    APK("application/vnd.android.package-archive"),
    IMAGE("image/png"),
    VIDEO("video/mp4"),
    OTHER("")
}