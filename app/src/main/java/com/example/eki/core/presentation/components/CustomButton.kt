package com.example.eki.core.presentation.components

import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Build
import androidx.compose.material3.Button
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.util.showShortToast
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
fun CustomButton(
    modifier: Modifier = Modifier,
    text: String,
    containerColor: Color = MaterialTheme.colorScheme.primaryContainer,
    contentColor: Color = MaterialTheme.colorScheme.onPrimaryContainer,
    imageResource: Int? = null,
    imageVector: ImageVector? = null,
    onClick: () -> Unit,
) {
    Button(
        modifier = modifier,
        onClick = onClick,
        colors = ButtonDefaults.buttonColors(containerColor = containerColor)
    ) {
        Text(
            modifier = Modifier.padding(end = MaterialTheme.spacing.small),
            text = text,
            style = MaterialTheme.typography.bodyMedium,
            color = contentColor
        )
        imageResource?.let { imageResource ->
            Icon(
                painterResource(id = imageResource),
                tint = contentColor,
                contentDescription = null
            )
        }
        imageVector?.let {
            Icon(
                imageVector = imageVector,
                tint = contentColor,
                contentDescription = "beside button",
            )
        }

    }
}

@Preview
@Composable
fun CustomButtonPreview() {
    val context = LocalContext.current
    EKITheme {
        CustomButton(
            text = "Click",
            imageVector = Icons.Default.Build,
            imageResource = R.drawable.ic_boy
        ) {
            context.showShortToast("Clicked")
        }
    }
}