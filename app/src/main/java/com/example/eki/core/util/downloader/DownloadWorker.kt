package com.example.eki.core.util.downloader

import android.Manifest
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import androidx.work.workDataOf
import com.example.eki.R
import com.example.eki.core.util.FileUtils
import com.example.eki.core.util.Strings

class DownloadWorker(
    private val context: Context,
    workerParameters: WorkerParameters,
) : CoroutineWorker(context, workerParameters) {

    companion object {
        private const val TAG = "DownloadWorker"
        const val KEY_FILE_URL = "key_file_url"
        const val KEY_FILE_TYPE = "key_file_type"
        const val KEY_FILE_NAME = "key_file_name"
        const val KEY_FILE_URI = "key_file_uri"
        const val CHANNEL_NAME = "download_file_channel"
        const val CHANNEL_DESCRIPTION = "download_file_description"
        const val CHANNEL_ID = "download_file_id"
        const val NOTIFICATION_ID = 1
    }

    override suspend fun doWork(): Result {

        val fileUrl = inputData.getString(KEY_FILE_URL).orEmpty()
        val fileName = inputData.getString(KEY_FILE_NAME).orEmpty()
        val fileType = inputData.getString(KEY_FILE_TYPE).orEmpty()

        Log.d(TAG, "doWork: $fileUrl | $fileName | $fileType")

        if (fileName.isEmpty()
            || fileType.isEmpty()
            || fileUrl.isEmpty()
        ) {
            return Result.failure()
        }

        showNotification()

        val uri = FileUtils.getSavedFileUri(
            fileName = fileName,
            fileType = enumValueOf(fileType),
            fileUrl = fileUrl,
            context = context
        )


        return if (uri != null) {
            NotificationManagerCompat.from(context).cancel(NOTIFICATION_ID)
            Result.success(workDataOf(KEY_FILE_URI to uri.toString()))
        } else {
            Result.failure()
        }
    }

    private fun showNotification() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = CHANNEL_NAME
            val description = CHANNEL_DESCRIPTION
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, name, importance)
            channel.description = description

            val notificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager?

            notificationManager?.createNotificationChannel(channel)
        }

        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_launcher_foreground)
            .setContentTitle(Strings.get(R.string.downloading))
            .setOngoing(false)
            .setProgress(0, 0, true)

        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.POST_NOTIFICATIONS
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            NotificationManagerCompat.from(context)
                .notify(NOTIFICATION_ID, builder.build())
        }
    }
}

