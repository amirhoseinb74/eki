package com.example.eki.core.util.calender

import android.os.Bundle
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.Saver
import com.example.eki.core.util.addZeroBehind
import com.example.eki.core.util.safeLetThree
import com.razaghimahdi.compose_persian_date.core.PersianDatePickerController

object CalenderUtils {

    fun getDateAndTime(controller: PersianDatePickerController, time: String = "00:00") =
        with(controller) {
            "${getPersianYear()}-${getPersianMonth().addZeroBehind()}-${getPersianDay().addZeroBehind()} $time"
        }

    fun PersianDatePickerController.updateController(controllerToUpdateWith: PersianDatePickerController) {
        with(controllerToUpdateWith) {
            this.updateDate(getPersianYear(), getPersianMonth(), getPersianDay())
        }
    }

    val persianDatePickerSaver =
        Saver<State<PersianDatePickerController>, Bundle>(save = { controller ->
            val bundle = Bundle()
            val dateBundle = Bundle()
            dateBundle.putInt("year", controller.value.getPersianYear())
            dateBundle.putInt("month", controller.value.getPersianMonth())
            dateBundle.putInt("day", controller.value.getPersianDay())
            bundle.putBundle("date", dateBundle)
            bundle
        }, restore = { bundle ->
            val controller = PersianDatePickerController()
            val dateBundle = bundle.getBundle("date") ?: return@Saver mutableStateOf(controller)

            safeLetThree(
                dateBundle.getInt("year"), dateBundle.getInt("month"), dateBundle.getInt("day")
            ) { year, month, day ->
                controller.updateDate(year, month, day)
            }

            mutableStateOf(controller)
        })
}
