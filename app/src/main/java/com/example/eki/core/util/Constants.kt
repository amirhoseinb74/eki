package com.example.eki.core.util

object Constants {

    const val BASE_URL = "https://096440.com/api/"
//    const val BASE_URL = "http://172.26.200.217:8080/api/" // ansari

    //headers for all apis
    const val NAME_ACCEPT = "NAME_ACCEPT"
    const val NAME_CONTENT_TYPE = "NAME_CONTENT_TYPE"
    const val NAME_AUTHORIZATION = "NAME_AUTHORIZATION_ID"
    const val HEADER_ORG_ID = "org_id"
    const val TOKEN_HEADER = "token"

    //api input constants
    const val APP_TYPE = "EKI"

    //log tags
    const val TEST_TAG = "\nTEST_EKI:\n"

    //map
    const val MAPBOX_TOKEN = "84785dc1-9106-4bd2-a400-770acb187fa4"
    const val PARSIMAP_STYLE_STREET = "https://www.parsimap.com/styles/street.json"
}