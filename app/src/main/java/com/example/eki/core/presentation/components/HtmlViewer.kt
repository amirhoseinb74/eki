package com.example.eki.core.presentation.components

import android.os.Build
import android.text.Html
import android.text.SpannableString
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.style.ForegroundColorSpan
import android.text.style.URLSpan
import android.text.util.Linkify
import android.view.View
import android.widget.TextView
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.viewinterop.AndroidView
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
fun HtmlViewer(
    modifier: Modifier = Modifier,
    content: String,
    contentTextSize: Dp = MaterialTheme.spacing.medium,
    textColor: Color = Color.Black,
    linkTextColor: Color = Color.Blue,
) {

    AndroidView(
        modifier = modifier,
        factory = { context ->

            TextView(context).apply {
                text = getParsedLinks(content, textColor)
                movementMethod = LinkMovementMethod.getInstance()
                textAlignment = View.TEXT_ALIGNMENT_TEXT_START
                linksClickable = true
                setTextColor(textColor.toArgb())
                setLinkTextColor(linkTextColor.toArgb())
                textSize = contentTextSize.value
            }
        }
    )
}

private fun getParsedLinks(txt: String?, textColor: Color): SpannableString {
    val span: Spanned = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) Html.fromHtml(
        txt,
        Html.FROM_HTML_MODE_COMPACT
    ) else Html.fromHtml(txt)
    val urlSpans = span.getSpans(0, span.length, URLSpan::class.java)
    val s = SpannableString(span)
    s.setSpan(ForegroundColorSpan(textColor.toArgb()), 0, s.length, 0)
    Linkify.addLinks(s, Linkify.ALL)
    for (urlSpan in urlSpans) {
        s.setSpan(urlSpan, span.getSpanStart(urlSpan), span.getSpanEnd(urlSpan), 0)
    }
    return s
}

@Preview(showBackground = true)
@Composable
fun HtmlViewerPreview() {
    EKITheme {
        HtmlViewer(
            content = "\n<p style=\"text-align: justify;\">\nبا سلام و احترام امدادگران همواره \n<b>\nپرچمدار ارائه خدمت به مشتریان\n</b>\n و یکی از قویترین عناصر معرفی خدمات و اشتراکهای امداد خودرو ایران هستند . \n<br>\nاز همین رو امدادگران ظرفیت و قابلیت بالایی در راستای افزایش فروش اشتراک های طلایی و سیار را دارند. از سوی دیگر گستردگی امدادگران در سراسر کشور و ارتباط مستقیم آنان با مردم، موجب می شود برداشت ایشان از جامعه عینیت بیشتری داشته و خواستههای مشتری از سوی ایشان بیش از دیگران قابل استناد باشد. با توجه توضیحات فوق و دریافت پیشنهادهای مختلف امدادگران، تصمیم گرفتیم ابتدا ظرفیت های تبلیغاتی مشترک امداد از جمله بارگذاری عکس و ویدیو در پیام رسان های مورد استفاده امدادگران و فضاهای محیطی جهت انجام امور تبلیغات را به کمک ذی النفعان ( ارتباطات ، امور امدادگران ، .... ) استعلام و بررسی نماییم ؛ سپس برنامه ای مدون و هدفمند تدوین و اجرایی شود . پیشنهاد می شود نحوه معرفی امدادخودروایران توسط امدادگران تا تاریخ 20-07-99 از طریق گوگل فرم به این واحد اعلام شده تا نسبت به جمع بندی این مهم اقدام آتی صورت پذیرد . لازم به توضیح است برخی از نمونه های مورد نظر این طرح به شرح زیر است :\n\n<br>\n\n<a href=\"https://docs.google.com/forms/d/e/1FAIpQLSdC9XcQfeAWFiBC4OCanY1jDC-op07UbV1xcK8W4nPJgwKE-w/viewform\" > برای مشاهده نمونه کلیک  نمایید</a>\n</p>"
        )
    }
}