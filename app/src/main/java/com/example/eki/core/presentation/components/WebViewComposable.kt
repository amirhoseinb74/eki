package com.example.eki.core.presentation.components

import android.net.http.SslError
import android.view.ViewGroup
import android.webkit.SslErrorHandler
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.viewinterop.AndroidView

@Composable
fun WebViewComposable(modifier: Modifier = Modifier, url: String) {
    val context = LocalContext.current
    val webView = remember { WebView(context) }
    var canGoBack by remember { mutableStateOf(false) }

    AndroidView(
        modifier = modifier,
        factory = {
            webView.apply {
                layoutParams = ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT
                )
                settings.javaScriptEnabled = true

                webViewClient = object : WebViewClient() {
                    override fun onReceivedSslError(
                        view: WebView?, handler: SslErrorHandler?,
                        error: SslError?,
                    ) {
                        handler?.proceed() ?: super.onReceivedSslError(view, handler, error)
                    }

                    override fun onPageFinished(view: WebView?, url: String?) {
                        canGoBack = webView.canGoBack()
                    }
                }
                loadUrl(url)
            }
        }
    ) { it.loadUrl(url) }

    BackHandler(enabled = canGoBack) {
        webView.goBack()
    }
}
