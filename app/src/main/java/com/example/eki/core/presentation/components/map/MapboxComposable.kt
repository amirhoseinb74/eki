package com.example.eki.core.presentation.components.map

import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.viewinterop.AndroidView
import com.example.eki.core.util.Constants
import com.example.eki.core.util.mapbox.moveCameraToLocation
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.Style

@Composable
fun MapboxComposable(
    modifier: Modifier = Modifier,
    latLng: LatLng? = null,
) {
    AndroidView(
        modifier = modifier,
        factory = { ctx ->
            Mapbox.getInstance(ctx, Constants.MAPBOX_TOKEN)
            MapView(ctx).apply {
                getMapAsync { mapboxMap ->
                    mapboxMap.setStyle(
                        Style.Builder().fromUri(Constants.PARSIMAP_STYLE_STREET)
                    )

                    latLng?.let {
                        mapboxMap.moveCameraToLocation(it, 14)
                    }
                }
            }
        }
    )
}