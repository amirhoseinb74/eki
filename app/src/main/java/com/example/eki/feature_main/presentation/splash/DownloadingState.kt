package com.example.eki.feature_main.presentation.splash

data class DownloadingState(
    val isDownloading: Boolean = false,
    val progress: Float = 0f,
)
