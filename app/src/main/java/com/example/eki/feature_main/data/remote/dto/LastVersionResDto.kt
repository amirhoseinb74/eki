package com.example.eki.feature_main.data.remote.dto

import com.example.eki.feature_main.domain.model.LastVersionRes

data class LastVersionResDto(
    val appType: String,
    val forceUpdate: Boolean,
    val id: Int,
    val isActive: Int,
    val liteFieldsList: String,
    val versionDate: String,
    val versionName: String,
    val versionNumber: Int,
    val versionURL: String,
)

fun LastVersionResDto.toLastVersionRes() = LastVersionRes(
    forceUpdate, versionName, versionNumber, versionURL
)