package com.example.eki.feature_main.domain.use_case.logout

import android.content.Context
import android.content.Intent
import com.example.eki.feature_main.domain.model.StoredLogin
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.example.eki.feature_main.presentation.last_work_order_service.LastWorkOrderService
import javax.inject.Inject

class LogoutUseCase @Inject constructor(
    private val rescuerRepository: RescuerRepository,
    private val context: Context,
) {
    suspend operator fun invoke() {
        rescuerRepository.apply {
            saveToken("")
            saveStoredLogin(StoredLogin())
            saveEncryptedPersonalIdToDataStore(null)
            saveUserId("")
        }
        Intent(context, LastWorkOrderService::class.java).also { intent ->
            intent.action = LastWorkOrderService.Action.STOP.toString()
            context.startService(intent)
        }
    }
}