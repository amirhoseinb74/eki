package com.example.eki.feature_main.presentation.bottom_navigation

import com.example.eki.R
import com.example.eki.feature_cartable.presentation.CartableScreens
import com.example.eki.feature_main.presentation.MainScreens
import com.example.eki.feature_profile.presentation.ProfileScreens

sealed class MainBottomNavItem(
    var title: String,
    var icon: Int,
    var selected: Boolean = false,
    val direction: String,
) {

    object Home : MainBottomNavItem(
        "Home",
        R.drawable.ic_home,
        true,
        direction = MainScreens.Home.route
    )

    object Profile : MainBottomNavItem(
        "Profile",
        R.drawable.ic_person,
        direction = ProfileScreens.Profile.route
    )

    object Cartable : MainBottomNavItem(
        "Cartable",
        R.drawable.ic_case,
        direction = CartableScreens.Cartable.route
    )
}