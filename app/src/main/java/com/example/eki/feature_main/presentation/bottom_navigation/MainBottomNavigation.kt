package com.example.eki.feature_main.presentation.bottom_navigation

import androidx.compose.foundation.layout.height
import androidx.compose.material3.BottomAppBar
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.NavigationBarItem
import androidx.compose.material3.NavigationBarItemDefaults
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.rememberNavController
import com.example.eki.ui.theme.EKITheme

@Composable
fun MainBottomNavigation(navController: NavController) {

    val screenHeight = LocalConfiguration.current.screenHeightDp.dp

    val items = listOf(
        MainBottomNavItem.Profile,
        MainBottomNavItem.Home,
        MainBottomNavItem.Cartable,
    )
    val contentColor = MaterialTheme.colorScheme.primaryContainer
    BottomAppBar(
        containerColor = MaterialTheme.colorScheme.onPrimaryContainer,
        contentColor = contentColor,
        modifier = Modifier.height(screenHeight.div(11))
    ) {
        items.forEach { item ->
            NavigationBarItem(
                icon = {
                    Icon(
                        painterResource(id = item.icon),
                        contentDescription = item.title
                    )
                },
                colors = NavigationBarItemDefaults.colors(
                    selectedIconColor = contentColor,
                    selectedTextColor = contentColor,
                    unselectedIconColor = contentColor.copy(0.4f),
                    unselectedTextColor = contentColor.copy(0.4f)
                ),
                selected = item.selected,
                onClick = {
                    if (!item.selected) {
                        navController.navigate(item.direction)
                    }
                }
            )
        }
    }

    navController.addOnDestinationChangedListener { _, destination, _ ->
        items.forEach { item ->
            item.selected = item.direction == destination.route
        }
    }
}

@Preview
@Composable
fun PreviewBottomNavigation() {
    EKITheme {
        MainBottomNavigation(rememberNavController())
    }
}