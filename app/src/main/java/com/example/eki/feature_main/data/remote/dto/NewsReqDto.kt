package com.example.eki.feature_main.data.remote.dto

import com.google.gson.annotations.SerializedName

data class NewsReqDto(
    @SerializedName("personType")
    private var personType: String? = null,
    @SerializedName("newsTypeId")
    private val newsType: String? = null,
    @SerializedName("title")
    private val searchPhrase: String? = null,
)