package com.example.eki.feature_main.domain.model

data class StoredLogin(
    val userName: String = "",
    val password: String = "",
    val isLogin: Boolean = false,
)
