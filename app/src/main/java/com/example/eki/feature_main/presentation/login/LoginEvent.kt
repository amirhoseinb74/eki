package com.example.eki.feature_main.presentation.login

sealed class LoginEvent {
    data class LoginClicked(val username: String, val password: String) : LoginEvent()
}