package com.example.eki.feature_main.domain.model

data class LastVersionRes(
    val forceUpdate: Boolean,
    val versionName: String,
    val versionNumber: Int,
    val versionURL: String,
)
