package com.example.eki.feature_main.data.repository

import com.example.eki.core.data.remote.BaseResponseSingleData
import com.example.eki.feature_main.data.remote.MainApi
import com.example.eki.feature_main.data.remote.dto.NewsReqDto
import com.example.eki.feature_main.data.remote.dto.NewsResDto
import com.example.eki.feature_main.domain.repository.NewsRepository
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val mainApi: MainApi,
) : NewsRepository {

    override suspend fun getLastNews(
        newsReqDto: NewsReqDto,
        token: String,
    ): BaseResponseSingleData<NewsResDto> {
        return mainApi.getLastNews(
            newsReqDto,
            token
        )
    }
}