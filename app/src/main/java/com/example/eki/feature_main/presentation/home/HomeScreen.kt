package com.example.eki.feature_main.presentation.home

import androidx.activity.ComponentActivity
import androidx.activity.compose.BackHandler
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.eki.R
import com.example.eki.core.presentation.components.ScaffoldState
import com.example.eki.core.presentation.components.animation.LoadingAnimation
import com.example.eki.core.util.safeLet
import com.example.eki.feature_main.domain.model.Feature
import com.example.eki.feature_main.domain.model.NewsRes
import com.example.eki.feature_main.presentation.MainScreens
import com.example.eki.feature_main.presentation.bottom_navigation.MainBottomNavigation
import com.example.eki.feature_main.presentation.home.components.ExitOrLogoutDialog
import com.example.eki.feature_main.presentation.home.components.FeaturePart
import com.example.eki.feature_main.presentation.home.components.LastNewsPart
import com.example.eki.feature_news.presentation.NewsScreens
import com.example.eki.ui.theme.EKITheme
import kotlinx.coroutines.flow.collectLatest

@Composable
fun HomeScreen(
    navController: NavController,
    viewModel: HomeViewModel = hiltViewModel(),
) {
    ScaffoldState(
        bottomBar = { MainBottomNavigation(navController) },
    ) { showMessage ->
        HomeScreenContent(
            viewModel.newsState.value,
            viewModel::onEvent,
            viewModel.rescuerHasActiveWorkOrder.observeAsState(false).value,
            onFeatureClick = { feature ->
                feature.route?.let { route ->
                    var finalRoute = route
                    if (feature.titleId == R.string.rescuer)
                        finalRoute += "/${viewModel.lastRescuerWorkOrderStatus.value}"
                    navController.navigate(finalRoute)
                }
            },
            onNewsClick = {
                navController.navigate(NewsScreens.News.route)
            }
        )

        LaunchedEffect(key1 = true) {
            viewModel.eventFlow.collectLatest { event ->
                when (event) {
                    HomeViewModel.HomeUIEvent.NavigateToLogin -> {
                        navController.navigate(MainScreens.Login.route)
                    }

                    is HomeViewModel.HomeUIEvent.ShowError -> {
                        showMessage(event.message)
                    }
                }
            }
        }
    }
}

@Composable
@Preview(device = Devices.NEXUS_7)
fun HomeScreenContent(
    newsState: NewsState = NewsState(title = "عنوان", summary = "خلاصه خیر"),
    sendVmEvent: (HomeEvents) -> Unit = {},
    rescuerHasActiveWorkOrderState: Boolean = false,
    onFeatureClick: (Feature) -> Unit = {},
    onNewsClick: () -> Unit = {},
) {
    val context = LocalContext.current

    EKITheme {
        var showExitOrLogoutDialog by remember {
            mutableStateOf(false)
        }
        Box(modifier = Modifier.fillMaxSize()) {
            Image(
                painter = painterResource(id = R.drawable.main_background),
                contentDescription = "main background",
                modifier = Modifier.fillMaxSize(),
                contentScale = ContentScale.FillBounds
            )

            FeaturePart(
                modifier = Modifier
                    .align(Alignment.Center),
                features = Feature.getCurrentFeatures(rescuerHasActiveWorkOrderState),
                onFeatureClick = onFeatureClick
            )

            safeLet(newsState.title, newsState.summary) { title, summary ->
                LastNewsPart(
                    modifier = Modifier
                        .align(Alignment.BottomCenter),
                    news = NewsRes(title = title, summary = summary),
                    onNewsClick = { onNewsClick() }
                )
            }

            if (showExitOrLogoutDialog) {
                ExitOrLogoutDialog(
                    onExitClick = {
                        val activity = (context as? ComponentActivity)
                        activity?.finishAffinity()
                    },
                    onLogoutClick = { sendVmEvent(HomeEvents.Logout) },
                    onDismiss = { showExitOrLogoutDialog = false })
            }

            if (newsState.isLoading) {
                LoadingAnimation()
            }
        }
        BackHandler(enabled = true) {
            showExitOrLogoutDialog = true
        }
    }
}
