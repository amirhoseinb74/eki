package com.example.eki.feature_main.presentation.login.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.scale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.ui.theme.TextWhite


@Composable
@Preview
fun Logo() {
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Image(
            painter = painterResource(id = R.drawable.emdad_logo),
            contentDescription = "emdadLogo",
            Modifier.scale(0.75f)
        )
        Text(
            text = stringResource(R.string.eki),
            style = MaterialTheme.typography.headlineSmall,
            color = TextWhite
        )
        VerticalSpacer(2)
        Text(
            text = stringResource(R.string.org_app),
            style = MaterialTheme.typography.headlineSmall,
            color = TextWhite
        )
    }
}