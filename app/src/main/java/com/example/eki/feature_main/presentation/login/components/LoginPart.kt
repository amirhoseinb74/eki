package com.example.eki.feature_main.presentation.login.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.ui.theme.EKITheme

@Composable
fun LoginPart(
    modifier: Modifier = Modifier,
    onClickLogin: (String, String) -> Unit,
) {
    val userName = rememberSaveable {
        mutableStateOf("")
    }
    val password = rememberSaveable {
        mutableStateOf("")
    }
    Column(
        modifier = modifier,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        LoginInputs(
            userName = userName,
            password = password,
            onUsernameChanged = { userName.value = it },
            onPasswordChanged = { password.value = it })
        VerticalSpacer(10)
        CustomButton(
            modifier = Modifier.fillMaxWidth(),
            text = stringResource(id = R.string.enter),
            onClick = { onClickLogin(userName.value, password.value) }
        )
    }
}

@Preview
@Composable
fun PreviewLoginPart() {
    EKITheme {
        LoginPart(onClickLogin = { _, _ -> })
    }
}