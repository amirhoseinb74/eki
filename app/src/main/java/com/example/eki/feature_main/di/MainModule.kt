package com.example.eki.feature_main.di

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import com.example.eki.core.di.FakeRepository
import com.example.eki.feature_main.data.remote.MainApi
import com.example.eki.feature_main.data.repository.AppVersionRepositoryImpl
import com.example.eki.feature_main.data.repository.NewsRepositoryImpl
import com.example.eki.feature_main.data.repository.RescuerRepositoryImpl
import com.example.eki.feature_main.data.repository.fake.FakeAppVersionRepo
import com.example.eki.feature_main.data.repository.fake.FakeNewsRepo
import com.example.eki.feature_main.data.repository.fake.FakeRescuerRepo
import com.example.eki.feature_main.domain.repository.AppVersionRepository
import com.example.eki.feature_main.domain.repository.NewsRepository
import com.example.eki.feature_main.domain.repository.RescuerRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MainModule {

    @Provides
    @Singleton
    fun provideMainApi(retrofit: Retrofit): MainApi = retrofit.create(MainApi::class.java)

    @Provides
    @Singleton
    fun provideNewsRepository(mainApi: MainApi): NewsRepository = NewsRepositoryImpl(mainApi)

    @Provides
    @Singleton
    @FakeRepository
    fun provideFakeNewsRepository(): NewsRepository = FakeNewsRepo()

    @Provides
    @Singleton
    fun provideAppVersionRepository(mainApi: MainApi): AppVersionRepository =
        AppVersionRepositoryImpl(mainApi)

    @Provides
    @Singleton
    @FakeRepository
    fun provideFakeAppVersionRepository(): AppVersionRepository = FakeAppVersionRepo()

    @Provides
    @Singleton
    fun provideRescuerRepository(
        mainApi: MainApi,
        dataStore: DataStore<Preferences>,
    ): RescuerRepository = RescuerRepositoryImpl(mainApi, dataStore)

    @Provides
    @Singleton
    @FakeRepository
    fun provideFakeRescuerRepository(): RescuerRepository = FakeRescuerRepo()
}