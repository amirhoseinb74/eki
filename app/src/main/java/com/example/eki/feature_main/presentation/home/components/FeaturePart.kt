package com.example.eki.feature_main.presentation.home.components

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.example.eki.core.presentation.components.WindowInfo
import com.example.eki.core.presentation.components.rememberWindowInfo
import com.example.eki.feature_main.domain.model.Feature
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
fun FeaturePart(
    modifier: Modifier = Modifier,
    features: List<Feature>,
    innerPadding: Dp = 0.dp,
    onFeatureClick: (Feature) -> Unit,
) {
    val windowInfo = rememberWindowInfo()
    var gridCells = 3
    var bottomPadding = 0.dp
    if (windowInfo.screenHeightInfo == WindowInfo.WindowType.Compact ||
        windowInfo.screenWidthInfo == WindowInfo.WindowType.Expanded
    ) {
        gridCells = 6
        bottomPadding = innerPadding
    }
    LazyVerticalGrid(
        verticalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.medium),
        horizontalArrangement = Arrangement.spacedBy(MaterialTheme.spacing.medium),
        columns = GridCells.Fixed(gridCells),
        contentPadding = PaddingValues(horizontal = MaterialTheme.spacing.small),
        modifier = modifier
            .wrapContentHeight()
            .fillMaxWidth()
            .padding(MaterialTheme.spacing.medium)
            .padding(bottom = bottomPadding)
    ) {
        items(features.size) {
            FeatureItem(
                modifier = Modifier.wrapContentSize(),
                feature = features[it],
                onFeatureClick
            )
        }
    }
}

@Preview(device = Devices.PHONE)
@Composable
fun PreviewFeaturePart() {
    EKITheme {
        FeaturePart(
            features = Feature.getCurrentFeatures(true), onFeatureClick = {}
        )
    }
}