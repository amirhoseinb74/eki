package com.example.eki.feature_main.presentation.login.components

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.drawBehind
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.graphics.Path
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalConfiguration
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.eki.R
import com.example.eki.core.presentation.components.WindowInfo
import com.example.eki.core.presentation.components.rememberWindowInfo
import com.example.eki.core.util.standardQuadFromTo
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@Composable
@Preview(device = Devices.TABLET)
fun LoginBackground() {

    EKITheme {
        val configuration = LocalConfiguration.current
        val screenWidth = configuration.screenWidthDp.dp
        val screenHeight = configuration.screenHeightDp.dp

        val backgroundTopColor = MaterialTheme.colorScheme.inverseSurface
        val topTextColor = MaterialTheme.colorScheme.inverseOnSurface
        val backgroundBottomColor = MaterialTheme.colorScheme.surface

        val isCompactHeight = rememberWindowInfo().screenHeightInfo == WindowInfo.WindowType.Compact

        Box(
            modifier = Modifier
                .fillMaxSize()
                .drawBehind {
                    drawRect(color = backgroundBottomColor)

                    val width = screenWidth.toPx()
                    val height = screenHeight.toPx()

                    //wave points
                    val upWavePoint1 = Offset(0f, height * 0.4f)
                    val upWavePoint2 = Offset(width * 0.25f, height * 0.33f)
                    val upWavePoint3 = Offset(width * 0.5f, height * 0.4f)
                    val upWavePoint4 = Offset(width * 0.75f, height * 0.28f)
                    val upWavePoint5 = Offset(width * 1.4f, -height * 1f)

                    //wave path
                    Path()
                        .apply {
                            moveTo(0f, 0f)
                            lineTo(upWavePoint1.x, upWavePoint1.y)
                            standardQuadFromTo(upWavePoint1, upWavePoint2)
                            standardQuadFromTo(upWavePoint2, upWavePoint3)
                            standardQuadFromTo(upWavePoint3, upWavePoint4)
                            standardQuadFromTo(upWavePoint4, upWavePoint5)
                            lineTo(width, 0f)
                            close()
                        }
                        .also {
                            drawPath(it, backgroundTopColor)
                        }
                }
        ) {

            Image(
                painter = painterResource(id = R.drawable.emdad_logo),
                contentDescription = null,
                modifier = Modifier
                    .padding(top = screenHeight / 5)
                    .size(
                        width = screenWidth.times(0.9f),
                        height = screenHeight.times(0.3f)
                    )
                    .align(Alignment.TopCenter),
                contentScale = ContentScale.FillBounds
            )
            Image(
                painter = painterResource(id = R.drawable.samand_emdad),
                contentDescription = null,
                modifier = Modifier
                    .padding(top = screenHeight / 5, start = screenWidth / 4)
                    .size(
                        width = screenWidth.times(0.5f),
                        height = screenHeight.times(0.3f)
                    )
                    .align(Alignment.TopCenter),
                contentScale = ContentScale.FillBounds
            )

            if (isCompactHeight) {
                Row(
                    modifier = Modifier
                        .fillMaxWidth()
                        .padding(MaterialTheme.spacing.medium)
                        .padding(horizontal = MaterialTheme.spacing.medium),
                    horizontalArrangement = Arrangement.SpaceBetween
                ) {
                    Text(
                        text = stringResource(R.string.eki),
                        style = MaterialTheme.typography.headlineMedium,
                        color = topTextColor
                    )
                    Text(
                        text = stringResource(R.string.org_app),
                        style = MaterialTheme.typography.headlineMedium,
                        color = topTextColor
                    )
                }
            } else {
                Column(
                    modifier = Modifier
                        .align(Alignment.TopCenter)
                        .padding(top = MaterialTheme.spacing.small),
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        text = stringResource(R.string.eki),
                        style = MaterialTheme.typography.headlineMedium,
                        color = topTextColor
                    )
                    Text(
                        text = stringResource(R.string.org_app),
                        style = MaterialTheme.typography.headlineSmall,
                        color = topTextColor
                    )
                }
            }

        }
    }
}