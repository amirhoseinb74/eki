package com.example.eki.feature_main.data.repository.fake

import android.util.Log
import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.BaseResponseSingleData
import com.example.eki.core.data.remote.Settings
import com.example.eki.core.util.Constants
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderReqDto
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderResDto
import com.example.eki.feature_main.data.remote.dto.LoginReqDto
import com.example.eki.feature_main.data.remote.dto.LoginResDto
import com.example.eki.feature_main.domain.model.StoredLogin
import com.example.eki.feature_main.domain.repository.RescuerRepository

class FakeRescuerRepo : RescuerRepository {
    override suspend fun login(loginReq: LoginReqDto): BaseResponse<LoginResDto> {
        return BaseResponse(
            data = listOf(
                LoginResDto(
                    "fa104d25-87fc-438c-8abc-9d7bd65ffaa9",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    ""
                )
            ), settings = Settings("ok", "1")
        )
    }

    override suspend fun saveToken(token: String) {
        Log.d(Constants.TEST_TAG, "saveToken: $token")
    }

    override suspend fun getToken(): String {
        return "fa104d25-87fc-438c-8abc-9d7bd65ffaa9"
    }

    override suspend fun saveUserId(userId: String) {
        Log.d(Constants.TEST_TAG, "saveUserId: $userId")
    }

    override suspend fun getUserId(): String {
        return "-29298"
    }

    override suspend fun saveStoredLogin(storedLogin: StoredLogin) {
        Log.d(Constants.TEST_TAG, "saveStoredLogin: $storedLogin")
    }

    override suspend fun getStoredLogin(): StoredLogin {
        return StoredLogin("emdadgaran", "123456", isLogin = true)
    }

    override suspend fun getLastWorkOrder(
        lastWorkOrderReqDto: LastWorkOrderReqDto,
    ): BaseResponse<LastWorkOrderResDto> {
        return BaseResponse(
            data = listOf(
                LastWorkOrderResDto(
                    "33466464",
                    "35.72406769",
                    "51.132061",
                    71,
                    "1"
                )
            ), settings = Settings("ok", "1")
        )
    }

    override suspend fun getEncryptedPersonalIdFromApi(personalId: String): BaseResponseSingleData<String> {
        return BaseResponseSingleData(
            data = "F4FRQCZbKq7QaFk0YQWcBCycFrVaqLxz",
            settings = Settings("ok", "1")
        )
    }

    override suspend fun saveEncryptedPersonalIdToDataStore(encryptedPersonalId: String?) {
        Log.d(Constants.TEST_TAG, "saveEncryptedPersonalId: $encryptedPersonalId")
    }

    override suspend fun getEncryptedPersonalIdFromDataStore(): String {
        return "F4FRQCZbKq7QaFk0YQWcBCycFrVaqLxz"
    }
}