package com.example.eki.feature_main.domain.model


data class LoginRes(
    val accessToken: String,
    val iOrgId: String,
    val firstName: String,
    val lastName: String,
    val opsRoleId: String,
    val userEntityId: String,
    val userId: String,
    val personalId: String,
)