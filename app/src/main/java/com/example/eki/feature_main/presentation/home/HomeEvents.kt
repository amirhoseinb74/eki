package com.example.eki.feature_main.presentation.home

sealed class HomeEvents {
    object Logout : HomeEvents()
}
