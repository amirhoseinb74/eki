package com.example.eki.feature_main.domain.repository

import com.example.eki.core.data.remote.BaseResponseSingleData
import com.example.eki.feature_main.data.remote.dto.NewsReqDto
import com.example.eki.feature_main.data.remote.dto.NewsResDto

interface NewsRepository {
    suspend fun getLastNews(
        newsReqDto: NewsReqDto,
        token: String,
    ): BaseResponseSingleData<NewsResDto>
}