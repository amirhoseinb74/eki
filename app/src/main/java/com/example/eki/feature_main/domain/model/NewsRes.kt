package com.example.eki.feature_main.domain.model

data class NewsRes(
    val newsId: String = "",
    val newsType: String = "",
    val title: String,
    val summary: String,
    val dtAddAt: String = "",
)