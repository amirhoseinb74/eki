package com.example.eki.feature_main.presentation.splash

//import com.example.eki.core.domain.commented_downloader.AndroidDownloader
import android.annotation.SuppressLint
import android.net.Uri
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.core.net.toUri
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.BuildConfig.VERSION_CODE
import com.example.eki.R
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.Strings
import com.example.eki.core.util.downloader.DownloadFile
import com.example.eki.core.util.downloader.Downloader
import com.example.eki.core.util.downloader.FileType
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_main.data.remote.dto.toLastVersionRes
import com.example.eki.feature_main.domain.use_case.last_version.GetLastVersionUseCase
import com.example.eki.feature_main.domain.use_case.login.LoginUseCase
import com.example.eki.feature_main.domain.use_case.store_login_data.GetStoredLoginUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject


@SuppressLint("HardwareIds")
@HiltViewModel
class SplashViewModel @Inject constructor(
    private val getLastVersionUseCase: GetLastVersionUseCase,
    private val getStoredLoginUseCase: GetStoredLoginUseCase,
    private val loginUseCase: LoginUseCase,
) : ViewModel() {

    private val _getLastVersionState = mutableStateOf(GetLastVersionState())
    val getLastVersionState: State<GetLastVersionState> = _getLastVersionState

    private val _downloadingState = mutableStateOf(DownloadingState())
    val downloadingState: State<DownloadingState> = _downloadingState

    var hasUserLoggedIn: Boolean = false

    private val _isLoadingState = mutableStateOf(true)
    val isLoadingState: State<Boolean> = _isLoadingState

    private var getLastVersionJob: Job? = null
    private var loginJob: Job? = null

    private var versionUrl: String? = null
    private var forceUpdate = false

    private val _eventFlow = MutableSharedFlow<SplashUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var storedUserName = ""
    private var storedPassword = ""

    init {
        getLastVersion()

        viewModelScope.launch {
            getStoredLoginUseCase()?.let { storedLogin ->
                hasUserLoggedIn = storedLogin.isLogin
                storedUserName = storedLogin.userName
                storedPassword = storedLogin.password
            } ?: run {
                hasUserLoggedIn = false
            }
        }
    }

    sealed class SplashUIEvent {
        data class ShowError(val message: String) : SplashUIEvent()
        object ShowUpdateDialog : SplashUIEvent()
        data class LoginOrNavigateToLoginScreen(val userName: String, val password: String) :
            SplashUIEvent()

        data class OpenDownloadedFile(val downloadFile: DownloadFile, val uri: Uri) :
            SplashUIEvent()

        object NavigateToHome : SplashUIEvent()
    }

    fun onEvent(events: SplashEvents) {
        when (events) {
            is SplashEvents.Update -> {
                versionUrl?.let { url ->
                    downloadFile(url, events.lifecycleOwner)
                }
            }

            is SplashEvents.Login -> login(events.userName, events.password)

            SplashEvents.DismissUpdate -> {
                viewModelScope.launch {
                    if (forceUpdate) {
                        showErrorInUI(
                            Strings.get(
                                R.string.have_to_update,
                                getLastVersionState.value.versionName
                            )
                        )
                        delay(1000L)
                        _eventFlow.emit(SplashUIEvent.ShowUpdateDialog)
                    } else {
                        loginOrNavigateToLoginScreen()
                    }
                }
            }

            SplashEvents.GetLastVersion -> getLastVersion()
        }
    }

    private fun downloadFile(url: String, lifecycleOwner: LifecycleOwner) {
        val file = DownloadFile(
            "EKI ${getLastVersionState.value.versionName}",
            FileType.APK,
            url,
        )

        Downloader.downloadFile(
            file = file,
            lifecycleOwner = lifecycleOwner,
            success = { uriString ->
                _downloadingState.value = DownloadingState(false)
                viewModelScope.launch {
                    _eventFlow.emit(SplashUIEvent.OpenDownloadedFile(file, uriString.toUri()))
                }
            },
            failed = {
                _downloadingState.value = DownloadingState(false)
                viewModelScope.launch {
                    showErrorInUI(Strings.get(R.string.downloading_failed))
                }
            },
            running = {
                _downloadingState.value = DownloadingState(true)
            },
        )
    }

    private fun login(userName: String, password: String) {
        loginJob?.cancel()
        loginJob = loginUseCase(userName, password).onEach { result ->
            when (result) {
                is ResponseHandler.Success -> {
                    _isLoadingState.value = false
                    _eventFlow.emit(SplashUIEvent.NavigateToHome)
                }

                is ResponseHandler.Error -> {
                    _isLoadingState.value = false
                    showErrorInUI(result.message)
                    delay(1000L)
                    hasUserLoggedIn = false
                    loginOrNavigateToLoginScreen()
                }

                is ResponseHandler.Loading -> {
                    _isLoadingState.value = true
                }
            }.exhaustive
        }.launchIn(viewModelScope)
    }

    private fun getLastVersion() {
        _getLastVersionState.value = GetLastVersionState()
        getLastVersionJob?.cancel()
        getLastVersionJob = getLastVersionUseCase().onEach { result ->
            when (result) {
                is ResponseHandler.Success -> {
                    _isLoadingState.value = false
                    result.data?.getOrNull(0)?.toLastVersionRes()?.let { lastVersionRes ->
                        _getLastVersionState.value = GetLastVersionState(
                            versionName = lastVersionRes.versionName
                        )
//                        forceUpdate = lastVersionRes.forceUpdate
                        versionUrl = lastVersionRes.versionURL

                        if (lastVersionRes.versionNumber > VERSION_CODE) {
                            _eventFlow.emit(SplashUIEvent.ShowUpdateDialog)
                        } else {
                            loginOrNavigateToLoginScreen()
                        }
                    } ?: loginOrNavigateToLoginScreen()
                }

                is ResponseHandler.Error -> {
                    _isLoadingState.value = false
                    _getLastVersionState.value = GetLastVersionState(showRetryButton = true)
                    showErrorInUI(result.message)
                }

                is ResponseHandler.Loading -> {
                    _isLoadingState.value = true
                }
            }.exhaustive
        }.launchIn(viewModelScope)
    }

    private suspend fun showErrorInUI(message: String?) {
        _eventFlow.emit(SplashUIEvent.ShowError(message ?: ResponseHandler.generalErrorMessage))
    }

    private suspend fun loginOrNavigateToLoginScreen() {
        _eventFlow.emit(
            SplashUIEvent.LoginOrNavigateToLoginScreen(
                storedUserName,
                storedPassword
            )
        )
    }
}