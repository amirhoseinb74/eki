package com.example.eki.feature_main.presentation.home

data class NewsState(
    val isLoading: Boolean = false,
    val title: String? = null,
    val summary: String? = null,
)
