package com.example.eki.feature_main.domain.model

data class LastWorkOrderRes(
    val workOrderId: String,
    val status: String,
)
