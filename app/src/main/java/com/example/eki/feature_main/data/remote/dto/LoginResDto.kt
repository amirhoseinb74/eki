package com.example.eki.feature_main.data.remote.dto

import com.example.eki.feature_main.domain.model.LoginRes
import com.google.gson.annotations.SerializedName

data class LoginResDto(
    @SerializedName("access_token")
    val accessToken: String,
    val iOrgId: String,
    @SerializedName("iorgId")
    val orgId: String,
    val personalId: String,
    val userId: String,
    val vFirstName: String,
    val vLastName: String,
    val vOpsRoleId: String,
    val vUserEntityId: String,
    @SerializedName("vfirstName")
    val firstName: String,
    @SerializedName("vlastName")
    val lastName: String,
    @SerializedName("vopsRoleId")
    val opsRoleId: String,
    @SerializedName("vuserEntityId")
    val userEntityId: String,
)

fun LoginResDto.toLoginRes() = LoginRes(
    accessToken,
    iOrgId,
    firstName,
    lastName,
    opsRoleId,
    userEntityId,
    userId,
    personalId
)