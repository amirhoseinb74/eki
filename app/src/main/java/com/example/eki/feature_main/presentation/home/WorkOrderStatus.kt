package com.example.eki.feature_main.presentation.home

enum class WorkOrderStatus(val id: String, val pName: String) {
    NOT_STARTED("0", "شروع نشده"),
    STARTED("1", "شروع شده"),
    FINISHED("2", "پایان یافته"),
    REJECTED("3", "لغو شده"),
    CANCELED("4", "کنسل شده"),
    SIGN("19", "امضا"),
    PAYMENT("20", "پرداخت");

    companion object {
        private val map = values().associateBy(WorkOrderStatus::id)
        fun fromString(id: String): WorkOrderStatus? = map[id]
    }
}

