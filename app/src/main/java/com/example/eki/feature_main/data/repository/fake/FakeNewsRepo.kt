package com.example.eki.feature_main.data.repository.fake

import com.example.eki.core.data.remote.BaseResponseSingleData
import com.example.eki.core.data.remote.Settings
import com.example.eki.feature_main.data.remote.dto.NewsReqDto
import com.example.eki.feature_main.data.remote.dto.NewsResDto
import com.example.eki.feature_main.domain.repository.NewsRepository

class FakeNewsRepo : NewsRepository {
    override suspend fun getLastNews(
        newsReqDto: NewsReqDto,
        token: String,
    ): BaseResponseSingleData<NewsResDto> {
        return BaseResponseSingleData(
            data = NewsResDto(
                title = "عنوان تستی",
                summary = "یک سری مطالب در قالب خبر در این بخش قابل نمایش هستند که میتوان مشاهده کرد در صفحه اصلی",
                newsId = "",
                newsType = "",
                dtAddAt = "",
                id = "",
                receiverId = "",
                seenTime = "",
                userName = ""
            ), settings = Settings(success = "1", message = "ok")
        )
    }
}