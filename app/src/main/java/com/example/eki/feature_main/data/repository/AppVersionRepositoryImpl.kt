package com.example.eki.feature_main.data.repository

import com.example.eki.feature_main.data.remote.MainApi
import com.example.eki.feature_main.data.remote.dto.LastVersionReqDto
import com.example.eki.feature_main.domain.repository.AppVersionRepository
import javax.inject.Inject

class AppVersionRepositoryImpl @Inject constructor(
    private val mainApi: MainApi,
) : AppVersionRepository {

    override suspend fun getLastVersion(appVersionReq: LastVersionReqDto) =
        mainApi.getLastVersion(appVersionReq)
}