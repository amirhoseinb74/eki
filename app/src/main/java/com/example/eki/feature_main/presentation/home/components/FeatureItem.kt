package com.example.eki.feature_main.presentation.home.components

import androidx.compose.animation.animateColor
import androidx.compose.animation.core.RepeatMode
import androidx.compose.animation.core.infiniteRepeatable
import androidx.compose.animation.core.rememberInfiniteTransition
import androidx.compose.animation.core.tween
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.Card
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.CompositionLocalProvider
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalLayoutDirection
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.LayoutDirection
import com.example.eki.R
import com.example.eki.feature_main.domain.model.Feature
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.BlueBespor
import com.example.eki.ui.theme.EKITheme
import com.example.eki.ui.theme.MediumGray
import com.example.eki.ui.theme.Orange
import com.example.eki.ui.theme.Yellow80

@Composable
fun FeatureItem(
    modifier: Modifier = Modifier,
    feature: Feature,
    onFeatureClicked: (Feature) -> Unit,
) {
    val infiniteTransition = rememberInfiniteTransition()
    val currentlyRunningBackgroundColor by infiniteTransition.animateColor(
        initialValue = Orange,
        targetValue = Yellow80,
        animationSpec = infiniteRepeatable(
            animation = tween(500),
            repeatMode = RepeatMode.Reverse
        )
    )

    val backgroundColor =
        if (feature.isActive && feature.currentlyRunning) currentlyRunningBackgroundColor else Yellow80
    val contentColor = if (feature.isActive) {
        if (feature.currentlyRunning) BlueBespor else Orange
    } else MediumGray

    Card(
        modifier = modifier
            .wrapContentSize()
            .clickable(enabled = feature.isActive) { onFeatureClicked(feature) },
        shape = MaterialTheme.shapes.extraLarge
    ) {
        Box(
            modifier = modifier
                .background(backgroundColor)
                .fillMaxSize()
        )
        {
            Column(
                modifier = Modifier
                    .align(Alignment.Center)
                    .padding(
                        horizontal = MaterialTheme.spacing.small,
                        vertical = MaterialTheme.spacing.xSmall
                    )
                    .fillMaxSize(),
                verticalArrangement = Arrangement.SpaceAround,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                CompositionLocalProvider(
                    LocalLayoutDirection provides LayoutDirection.Ltr
                ) {
                    Icon(
                        modifier = Modifier.aspectRatio(2f),
                        painter = painterResource(id = feature.painterId),
                        tint = contentColor,
                        contentDescription = feature.contentDescription,
                    )
                }
                Text(text = stringResource(id = feature.titleId), color = contentColor)
            }
        }
    }
}

@Composable
@Preview
fun FeatureItemPreview() {
    EKITheme {
        FeatureItem(
            feature = Feature(
                titleId = R.string.agency,
                painterId = R.drawable.ic_technician,
                isActive = false,
                currentlyRunning = false
            ), onFeatureClicked = {}
        )
    }
}