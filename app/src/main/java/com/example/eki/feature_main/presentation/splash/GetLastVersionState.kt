package com.example.eki.feature_main.presentation.splash

data class GetLastVersionState(
    val versionName: String = "",
    val showRetryButton: Boolean = false,
)
