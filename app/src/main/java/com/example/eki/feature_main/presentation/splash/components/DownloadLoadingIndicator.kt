package com.example.eki.feature_main.presentation.splash.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.Blue50
import com.example.eki.ui.theme.EKITheme

@Composable
fun DownloadLoadingIndicator(
    loadingColor: Color = Blue50,
) {
    Box(
        contentAlignment = Alignment.Center,
        modifier = Modifier.fillMaxSize()
    ) {
        Column(
            modifier = Modifier
                .background(Color.Black.copy(0.2f))
                .padding(MaterialTheme.spacing.medium)
                .fillMaxWidth(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            CircularProgressIndicator(
                color = loadingColor,
                strokeWidth = MaterialTheme.spacing.xSmall,
                modifier = Modifier.size(MaterialTheme.spacing.xLarge)
            )
            VerticalSpacer(height = 8)
            Text(
                text = stringResource(id = R.string.downloading),
                style = MaterialTheme.typography.bodyMedium,
                color = loadingColor
            )
        }
    }
}

@Preview
@Composable
fun PreviewDownloadLoadingIndicator() {
    EKITheme {
        DownloadLoadingIndicator()
    }
}