package com.example.eki.feature_main.presentation.last_work_order_service

import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.example.eki.BuildConfig
import com.example.eki.R
import com.example.eki.core.di.ApplicationScope
import com.example.eki.core.presentation.MainActivity
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderReqDto
import com.example.eki.feature_main.data.remote.dto.toLastWorkOrderRes
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.example.eki.feature_main.domain.use_case.login.LoginUseCase
import com.example.eki.feature_main.presentation.home.WorkOrderStatus
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class LastWorkOrderService : Service() {

    @Inject
    lateinit var rescuerRepository: RescuerRepository

    @Inject
    @ApplicationScope
    lateinit var applicationScope: CoroutineScope

    private var token = ""
    private var stopped: Boolean = false

    //for notification
    private var lastId = ""
    private var lastStatus = ""

    companion object {
        const val NOTIFICATION_CHANNEL_ID = "last_work_order_channel_id"
        const val NOTIFICATION_CHANNEL_NAME = "last_work_order_channel_name"
        const val ACTION_BROADCAST_LAST_WORK_ORDER_DATA = "com.example.eki.currentWorkOrder"
        const val KEY_LAST_WORK_ORDER_STATUS = "last_work_order_status"
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action) {
            Action.START.toString() -> start()
            Action.STOP.toString() -> {
                stopped = true
                stopSelf()
            }
        }
        intent?.extras?.getString(LoginUseCase.TOKEN_EXTRA)?.let { loginToken ->
            token = loginToken
        }
        return super.onStartCommand(intent, flags, startId)
    }

    private fun start() {
        applicationScope.launch {
            while (!stopped) {
                val lastWorkOrderReqDto = LastWorkOrderReqDto(
                    "",
                    token,
                    "",
                    ""
                )

                val result = rescuerRepository.getLastWorkOrder(lastWorkOrderReqDto)
                if (result.isSuccessful()) {
                    result.data?.getOrNull(0)?.toLastWorkOrderRes()?.let { apiRes ->
                        WorkOrderStatus.fromString(apiRes.status)?.apply {
                            val idOrStatusChanges =
                                lastId != apiRes.workOrderId || lastStatus != pName

                            if (idOrStatusChanges) {
                                showNotification(apiRes.workOrderId, pName)
                                broadcastLastWorkOrderStatus(name)
                            }

                            lastId = apiRes.workOrderId
                            lastStatus = pName
                        }
                    }
                }
                val delayTime = if (BuildConfig.DEBUG) 10000L else 3000L
                delay(delayTime)
            }
        }
    }

    private fun showNotification(
        workOrderId: String,
        status: String,
    ) {
        val notificationIntent = Intent(this, MainActivity::class.java)
        notificationIntent.flags = Intent.FLAG_FROM_BACKGROUND
        val pendingIntent = PendingIntent.getActivity(
            this,
            0,
            notificationIntent,
            PendingIntent.FLAG_IMMUTABLE
        )
        val contentTitle = applicationContext.getString(R.string.last_work_order, workOrderId)
        val contentText = applicationContext.getString(R.string.status, status)

        val notification = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_technician)
            .setContentTitle(contentTitle)
            .setContentIntent(pendingIntent)
            .setContentText(contentText)
            .build()
        startForeground(1, notification)
    }

    private fun broadcastLastWorkOrderStatus(name: String) {
        Intent(ACTION_BROADCAST_LAST_WORK_ORDER_DATA).also { intent ->
            intent.putExtra(
                KEY_LAST_WORK_ORDER_STATUS,
                name
            )
            sendBroadcast(intent)
        }
    }

    enum class Action {
        START, STOP
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }
}