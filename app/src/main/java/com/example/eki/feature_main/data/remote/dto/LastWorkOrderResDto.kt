package com.example.eki.feature_main.data.remote.dto

import com.example.eki.feature_main.domain.model.LastWorkOrderRes

data class LastWorkOrderResDto(
    val id: String,
    val lat: String,
    val lng: String,
    val rank: Int,
    val status: String,
)

fun LastWorkOrderResDto.toLastWorkOrderRes() = LastWorkOrderRes(
    workOrderId = id,
    status = status
)