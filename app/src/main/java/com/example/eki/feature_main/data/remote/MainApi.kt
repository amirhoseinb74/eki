package com.example.eki.feature_main.data.remote

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.BaseResponseSingleData
import com.example.eki.feature_main.data.remote.dto.LastVersionReqDto
import com.example.eki.feature_main.data.remote.dto.LastVersionResDto
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderReqDto
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderResDto
import com.example.eki.feature_main.data.remote.dto.LoginReqDto
import com.example.eki.feature_main.data.remote.dto.LoginResDto
import com.example.eki.feature_main.data.remote.dto.NewsReqDto
import com.example.eki.feature_main.data.remote.dto.NewsResDto
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface MainApi {

    @POST("org_login")
    suspend fun login(@Body body: LoginReqDto): BaseResponse<LoginResDto>

    @POST("org_last_version")
    suspend fun getLastVersion(@Body body: LastVersionReqDto): BaseResponse<LastVersionResDto>

    @POST("last_news")
    suspend fun getLastNews(
        @Body body: NewsReqDto,
        @Query("access_token") token: String,
    ): BaseResponseSingleData<NewsResDto>

    @POST("app/expert/notification/show")
    suspend fun getLastWorkorder(
        @Body body: LastWorkOrderReqDto,
        @Query("access_token") token: String,
    ): BaseResponse<LastWorkOrderResDto>

    @GET("encrypt")
    suspend fun getEncryptedPersonalId(
        @Query("textToEncrypt") personalId: String,
    ): BaseResponseSingleData<String>
}