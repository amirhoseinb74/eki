package com.example.eki.feature_main.presentation.login

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.eki.core.presentation.components.DoubleBackPressToExit
import com.example.eki.core.presentation.components.ScaffoldState
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.core.presentation.components.animation.LoadingAnimation
import com.example.eki.feature_main.presentation.MainScreens
import com.example.eki.feature_main.presentation.login.components.LoginBackground
import com.example.eki.feature_main.presentation.login.components.LoginPart
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme
import kotlinx.coroutines.flow.collectLatest

@Composable
@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
fun LoginScreen(
    navController: NavController,
    viewModel: LoginViewModel = hiltViewModel(),
) {
    ScaffoldState { showSnackBar ->
        LoginScreenContent(
            isLoadingState = viewModel.isLoadingState.value,
            sendVMEvent = viewModel::onEvent
        )
        LaunchedEffect(key1 = true) {
            viewModel.eventFlow.collectLatest { event ->
                when (event) {
                    LoginViewModel.LoginUIEvent.NavigateToHome -> {
                        navController.navigate(MainScreens.Home.route)
                    }

                    is LoginViewModel.LoginUIEvent.ShowError -> showSnackBar(event.message)
                }
            }
        }
    }
    DoubleBackPressToExit()
}

@Preview(device = Devices.PHONE)
@Composable
fun LoginScreenContent(
    isLoadingState: Boolean = false,
    sendVMEvent: (LoginEvent) -> Unit = {},
) {
    EKITheme {
        LoginBackground()
        Box(modifier = Modifier.fillMaxSize()) {
            Column(
                horizontalAlignment = Alignment.CenterHorizontally,
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(horizontal = MaterialTheme.spacing.xLarge)
                    .padding(bottom = MaterialTheme.spacing.large)
            ) {
                VerticalSpacer(10)
                LoginPart { username, password ->
                    sendVMEvent(LoginEvent.LoginClicked(username, password))
                }
            }

            if (isLoadingState) {
                LoadingAnimation()
            }
        }
    }
}