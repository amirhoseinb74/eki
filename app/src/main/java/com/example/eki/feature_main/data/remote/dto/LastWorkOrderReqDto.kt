package com.example.eki.feature_main.data.remote.dto

data class LastWorkOrderReqDto(
    val isActive: String,
    val token: String,
    val userLatitude: String,
    val userLongitude: String,
)