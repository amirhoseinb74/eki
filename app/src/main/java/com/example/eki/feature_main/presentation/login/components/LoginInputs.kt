package com.example.eki.feature_main.presentation.login.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.OutlinedTextField
import androidx.compose.material3.Text
import androidx.compose.material3.TextFieldDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.ExperimentalComposeUiApi
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.HorizontalSpacer
import com.example.eki.core.presentation.components.VerticalSpacer
import com.example.eki.core.presentation.components.WindowInfo
import com.example.eki.core.presentation.components.rememberWindowInfo
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.DarkerYellow50
import com.example.eki.ui.theme.EKITheme

@OptIn(ExperimentalComposeUiApi::class, ExperimentalMaterial3Api::class)
@Composable
fun LoginInputs(
    modifier: Modifier = Modifier,
    userName: MutableState<String>,
    password: MutableState<String>,
    onUsernameChanged: (String) -> Unit,
    onPasswordChanged: (String) -> Unit,
) {

    val keyboardController = LocalSoftwareKeyboardController.current
    val windowInfo = rememberWindowInfo().screenWidthInfo

    if (windowInfo == WindowInfo.WindowType.Compact) {
        Column(
            modifier = modifier,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            OutlinedTextField(
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
                textStyle = MaterialTheme.typography.bodyMedium,
                value = userName.value,
                onValueChange = { onUsernameChanged(it) },
                label = {
                    Text(text = stringResource(R.string.username))
                },
                singleLine = true,
                modifier = Modifier
                    .fillMaxWidth()
                    .defaultMinSize(minHeight = MaterialTheme.spacing.large),
                shape = MaterialTheme.shapes.extraLarge,
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = DarkerYellow50,
                    unfocusedBorderColor = Color.White
                )
            )
            VerticalSpacer(5)
            OutlinedTextField(
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(onDone = {
                    keyboardController?.hide()
                }),
                textStyle = MaterialTheme.typography.bodyMedium,
                value = password.value,
                onValueChange = { onPasswordChanged(it) },
                label = {
                    Text(text = stringResource(R.string.password))
                },
                singleLine = true,
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = DarkerYellow50, unfocusedBorderColor = Color.White
                ),
                modifier = Modifier
                    .fillMaxWidth()
                    .defaultMinSize(minHeight = MaterialTheme.spacing.large),
                shape = MaterialTheme.shapes.extraLarge,
                visualTransformation = PasswordVisualTransformation()
            )
        }
    } else {
        Row(
            modifier = modifier,
            verticalAlignment = Alignment.CenterVertically
        ) {
            OutlinedTextField(
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Next),
                textStyle = MaterialTheme.typography.bodyMedium,
                value = userName.value,
                onValueChange = { onUsernameChanged(it) },
                label = {
                    Text(text = stringResource(R.string.username))
                },
                singleLine = true,
                modifier = Modifier
                    .weight(1f, true)
                    .defaultMinSize(minHeight = MaterialTheme.spacing.large),
                shape = MaterialTheme.shapes.extraLarge,
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = DarkerYellow50,
                    unfocusedBorderColor = Color.White
                )
            )
            HorizontalSpacer(20)
            OutlinedTextField(
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions = KeyboardActions(onDone = {
                    keyboardController?.hide()
                }),
                textStyle = MaterialTheme.typography.bodyMedium,
                value = password.value,
                onValueChange = { onPasswordChanged(it) },
                label = {
                    Text(text = stringResource(R.string.password))
                },
                singleLine = true,
                colors = TextFieldDefaults.outlinedTextFieldColors(
                    focusedBorderColor = DarkerYellow50, unfocusedBorderColor = Color.White
                ),
                modifier = Modifier
                    .weight(1f, true)
                    .defaultMinSize(minHeight = MaterialTheme.spacing.large),
                shape = MaterialTheme.shapes.extraLarge,
                visualTransformation = PasswordVisualTransformation()
            )
        }
    }
}

@Composable
@Preview(device = Devices.PHONE)
fun PreviewLoginInputs() {
    val userName = remember {
        mutableStateOf("")
    }
    val password = remember {
        mutableStateOf("")
    }
    EKITheme {
        LoginInputs(userName = userName,
            password = password,
            onUsernameChanged = {},
            onPasswordChanged = {})
    }
}