package com.example.eki.feature_main.data.remote.dto

data class LoginReqDto(
    val eAppType: String,
    val vAndroidId: String,
    val vAppVersion: String,
    val vDeviceToken: String,
    val vPassword: String,
    val vUsername: String,
)