package com.example.eki.feature_main.presentation.splash.components

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.material3.LinearProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.input.pointer.pointerInput
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.sp
import com.example.eki.ui.spacing

@Composable
fun DownloadingProgress(progress: Float) {
    Box(
        Modifier
            .fillMaxSize()
            .background(Color.Black.copy(alpha = 0.3f))
            .pointerInput(Unit) {}
    ) {
// Convert the progress value to a percentage
        val percentage by remember {
            mutableStateOf((progress * 100).toInt())
        }
        val progress2 by remember {
            mutableStateOf(progress)
        }
// Create an annotated string with different styles for the number and the sign
        val text = buildAnnotatedString {
            withStyle(style = SpanStyle(fontSize = 24.sp)) {
                append("$percentage")
            }
            withStyle(style = SpanStyle(fontSize = 16.sp)) {
                append("%")
            }
        }
// Display the progress indicator and the text in a row
        Row(
            modifier = Modifier
                .padding(MaterialTheme.spacing.medium)
                .align(Alignment.Center)
        ) {
            LinearProgressIndicator(
                modifier = Modifier
                    .weight(1f)
                    .height(MaterialTheme.spacing.small),
                progress = progress2,
                color = MaterialTheme.colorScheme.primary
            )
            Spacer(modifier = Modifier.width(MaterialTheme.spacing.small))
            Text(text = text)
        }
    }
}

@Preview
@Composable
fun PreviewDownloadingProgress() {
    DownloadingProgress(progress = 0.35f)
}