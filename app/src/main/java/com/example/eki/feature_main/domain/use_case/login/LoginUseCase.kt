package com.example.eki.feature_main.domain.use_case.login

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.provider.Settings
import com.example.eki.BuildConfig
import com.example.eki.EKIApplication
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.data.remote.ResponseHandler.Companion.responseHandlerFlow
import com.example.eki.core.util.Constants
import com.example.eki.core.util.EncryptionUtils
import com.example.eki.feature_main.data.remote.dto.LoginReqDto
import com.example.eki.feature_main.data.remote.dto.LoginResDto
import com.example.eki.feature_main.data.remote.dto.toLoginRes
import com.example.eki.feature_main.domain.model.LoginRes
import com.example.eki.feature_main.domain.model.StoredLogin
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.example.eki.feature_main.presentation.last_work_order_service.LastWorkOrderService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class LoginUseCase @Inject constructor(
    private val repository: RescuerRepository,
    private val context: Context,
) {
    @SuppressLint("HardwareIds")
    operator fun invoke(
        username: String,
        password: String,
    ): Flow<ResponseHandler<List<LoginResDto>>> {

        val application = EKIApplication.instance
        val androidId =
            Settings.Secure.getString(application.contentResolver, Settings.Secure.ANDROID_ID)

        val loginReq = LoginReqDto(
            vUsername = username,
            vPassword = EncryptionUtils.getMD5(password),
            eAppType = Constants.APP_TYPE,
            vAndroidId = androidId,
            vAppVersion = BuildConfig.VERSION_CODE.toString(),
            vDeviceToken = ""
        )

        return responseHandlerFlow(
            onSuccessExtra = { result ->
                result.getOrNull(0)?.toLoginRes()?.let { loginRes ->
                    saveLoginDataToDataStore(loginRes, username, password)
                    startLastWorkOrderService(loginRes)
                    getEncryptedPersonalIdAndSaveItToDataStore(loginRes.personalId)
                }
            }) {
            repository.login(loginReq)
        }
    }

    private suspend fun getEncryptedPersonalIdAndSaveItToDataStore(personalId: String) {
        repository.getEncryptedPersonalIdFromApi(personalId).let { response ->
            if (response.isSuccessful()) {
                repository.saveEncryptedPersonalIdToDataStore(response.data)
            }
        }
    }

    private suspend fun saveLoginDataToDataStore(
        loginRes: LoginRes,
        username: String,
        password: String,
    ) {
        repository.saveToken(loginRes.accessToken)
        repository.saveUserId(loginRes.userId)
        repository.saveStoredLogin(StoredLogin(username, password, true))
    }

    private fun startLastWorkOrderService(loginRes: LoginRes) {
        Intent(context, LastWorkOrderService::class.java).also { intent ->
            intent.action = LastWorkOrderService.Action.START.toString()
            intent.putExtra(TOKEN_EXTRA, loginRes.accessToken)
            context.startService(intent)
        }
    }

    companion object {
        const val TOKEN_EXTRA = "token_extra"
    }
}