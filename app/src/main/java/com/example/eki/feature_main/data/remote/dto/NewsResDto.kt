package com.example.eki.feature_main.data.remote.dto

import com.example.eki.feature_main.domain.model.NewsRes

data class NewsResDto(
    val dtAddAt: String,
    val id: String,
    val newsId: String,
    val newsType: String,
    val receiverId: String,
    val seenTime: String,
    val summary: String,
    val title: String,
    val userName: String,
)

fun NewsResDto.toNewsRes() = NewsRes(
    newsId = newsId,
    newsType = newsType,
    title = title,
    summary = summary,
    dtAddAt = dtAddAt
)