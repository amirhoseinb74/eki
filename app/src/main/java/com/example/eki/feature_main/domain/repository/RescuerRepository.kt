package com.example.eki.feature_main.domain.repository

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.BaseResponseSingleData
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderReqDto
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderResDto
import com.example.eki.feature_main.data.remote.dto.LoginReqDto
import com.example.eki.feature_main.data.remote.dto.LoginResDto
import com.example.eki.feature_main.domain.model.StoredLogin

interface RescuerRepository {

    suspend fun login(loginReq: LoginReqDto): BaseResponse<LoginResDto>

    suspend fun saveToken(token: String)

    suspend fun getToken(): String

    suspend fun saveUserId(userId: String)

    suspend fun getUserId(): String

    suspend fun saveStoredLogin(storedLogin: StoredLogin)

    suspend fun getStoredLogin(): StoredLogin?

    suspend fun getLastWorkOrder(
        lastWorkOrderReqDto: LastWorkOrderReqDto,
    ): BaseResponse<LastWorkOrderResDto>

    suspend fun getEncryptedPersonalIdFromApi(personalId: String): BaseResponseSingleData<String>

    suspend fun saveEncryptedPersonalIdToDataStore(encryptedPersonalId: String?)

    suspend fun getEncryptedPersonalIdFromDataStore(): String
}