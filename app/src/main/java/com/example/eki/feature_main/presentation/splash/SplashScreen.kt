package com.example.eki.feature_main.presentation.splash

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalLifecycleOwner
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.eki.BuildConfig
import com.example.eki.R
import com.example.eki.core.presentation.components.CustomAlertDialog
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.core.presentation.components.ScaffoldState
import com.example.eki.core.presentation.components.animation.LoadingAnimation
import com.example.eki.core.util.downloader.open
import com.example.eki.feature_main.presentation.MainScreens
import com.example.eki.feature_main.presentation.splash.components.DownloadLoadingIndicator
import com.example.eki.feature_main.presentation.splash.components.SplashGif
import com.example.eki.ui.spacing
import kotlinx.coroutines.flow.collectLatest

@Composable
fun SplashScreen(
    navController: NavController,
    viewModel: SplashViewModel = hiltViewModel(),
) {

    var showUpdateDialog by remember { mutableStateOf(false) }
    val context = LocalContext.current

    ScaffoldState { showSnackBar ->
        SplashScreenContent(
            getLastVersionState = viewModel.getLastVersionState.value,
            downloadingState = viewModel.downloadingState.value,
            isLoadingState = viewModel.isLoadingState.value,
            showUpdateDialog = showUpdateDialog,
            sendViewModelEvent = viewModel::onEvent,
            hideUpdateDialog = { showUpdateDialog = false }
        )
        LaunchedEffect(key1 = true) {
            viewModel.eventFlow.collectLatest { event ->
                when (event) {
                    is SplashViewModel.SplashUIEvent.ShowError -> showSnackBar(event.message)

                    is SplashViewModel.SplashUIEvent.ShowUpdateDialog -> {
                        showUpdateDialog = true
                    }

                    is SplashViewModel.SplashUIEvent.LoginOrNavigateToLoginScreen -> {
                        if (viewModel.hasUserLoggedIn && event.userName.isNotEmpty() && event.password.isNotEmpty()) {
                            viewModel.onEvent(SplashEvents.Login(event.userName, event.password))
                        } else {
                            navController.navigate(MainScreens.Login.route)
                        }
                    }

                    is SplashViewModel.SplashUIEvent.OpenDownloadedFile -> {
                        event.downloadFile.open(context, event.uri)
                    }

                    SplashViewModel.SplashUIEvent.NavigateToHome -> {
                        navController.navigate(MainScreens.Home.route)
                    }
                }
            }
        }
    }
}

@Composable
@Preview
fun SplashScreenContent(
    getLastVersionState: GetLastVersionState = GetLastVersionState(),
    downloadingState: DownloadingState = DownloadingState(),
    isLoadingState: Boolean = false,
    showUpdateDialog: Boolean = true,
    sendViewModelEvent: (SplashEvents) -> Unit = {},
    hideUpdateDialog: () -> Unit = {},
) {
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(MaterialTheme.colorScheme.background)
    ) {
        Column(
            modifier = Modifier.align(Alignment.Center),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            SplashGif()
            Text(
                text = stringResource(id = R.string.version) + BuildConfig.VERSION_NAME,
                style = MaterialTheme.typography.bodyMedium,
                color = MaterialTheme.colorScheme.onBackground
            )
        }
        if (showUpdateDialog) {
            val lifecycleOwner = LocalLifecycleOwner.current
            CustomAlertDialog(modifier = Modifier.align(Alignment.BottomCenter),
                title = stringResource(id = R.string.update),
                message = stringResource(
                    id = R.string.new_version_available_please_update,
                    getLastVersionState.versionName
                ),
                onConfirm = {
                    sendViewModelEvent(SplashEvents.Update(lifecycleOwner = lifecycleOwner))
                },
                onDismiss = {
                    hideUpdateDialog()
                    sendViewModelEvent(SplashEvents.DismissUpdate)
                })
        }

        if (isLoadingState) {
            LoadingAnimation()
        }

        if (downloadingState.isDownloading) {
            hideUpdateDialog()
//                DownloadingProgress(progress = downloadingState.progress)
            DownloadLoadingIndicator()
        }

        if (getLastVersionState.showRetryButton) {
            CustomButton(
                modifier = Modifier
                    .align(Alignment.BottomCenter)
                    .padding(PaddingValues(MaterialTheme.spacing.medium)),
                text = stringResource(R.string.retry),
                imageVector = Icons.Default.Refresh
            ) {
                sendViewModelEvent(SplashEvents.GetLastVersion)
            }
        }
    }
}
