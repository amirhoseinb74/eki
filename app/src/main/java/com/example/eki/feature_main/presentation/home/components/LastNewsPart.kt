package com.example.eki.feature_main.presentation.home.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentSize
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ElevatedCard
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.eki.R
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.feature_main.domain.model.NewsRes
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun LastNewsPart(
    modifier: Modifier = Modifier,
    news: NewsRes,
    onNewsClick: (NewsRes) -> Unit,
) {

    ElevatedCard(
        modifier = modifier.padding(
            horizontal = MaterialTheme.spacing.medium,
            vertical = MaterialTheme.spacing.medium
        ),
        onClick = { onNewsClick(news) },
        shape = MaterialTheme.shapes.extraLarge,
        colors = CardDefaults.cardColors(
            containerColor = MaterialTheme.colorScheme.surface,
            contentColor = MaterialTheme.colorScheme.onSurface
        )
    ) {
        Column(Modifier.wrapContentSize()) {
            Text(
                text = news.title,
                style = MaterialTheme.typography.bodyMedium,
                color = MaterialTheme.colorScheme.onSurface,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(
                        start = MaterialTheme.spacing.medium,
                        top = MaterialTheme.spacing.small
                    )
            )
            Row(
                Modifier
                    .fillMaxWidth()
                    .padding(
                        start = MaterialTheme.spacing.medium,
                        bottom = MaterialTheme.spacing.small,
                        end = MaterialTheme.spacing.small
                    ),
                verticalAlignment = Alignment.CenterVertically
            ) {
                Text(
                    modifier = Modifier.weight(2f),
                    text = news.summary,
                    maxLines = 2,
                    overflow = TextOverflow.Ellipsis,
                    color = MaterialTheme.colorScheme.onSurface,
                    style = MaterialTheme.typography.bodySmall
                )
                CustomButton(
                    onClick = { onNewsClick(news) },
                    modifier = Modifier
                        .weight(1f)
                        .padding(start = MaterialTheme.spacing.small),
                    text = stringResource(id = R.string.more)
                )
            }
        }
    }
}

@Preview
@Composable
fun PreviewLastNewsPart() {
    EKITheme {
        LastNewsPart(news = NewsRes(
            title = "اطلاعیه فنی",
            summary = "پیمانکاران محترم امدادی اطلاعیه فنی قطعات زیر در صورتی قابل رویت میباشند که این حرف ها و مزخرفات برای امثال شما که میخوانید خداحافظ است.",
            newsId = "",
            newsType = "",
            dtAddAt = ""
        ), onNewsClick = {})
    }
}