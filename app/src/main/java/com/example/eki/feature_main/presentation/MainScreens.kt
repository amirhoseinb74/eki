package com.example.eki.feature_main.presentation

sealed class MainScreens(val route: String) {
    object Splash : MainScreens("splash_screen")
    object Login : MainScreens("login_screen")
    object Home : MainScreens("home_screen")
}
