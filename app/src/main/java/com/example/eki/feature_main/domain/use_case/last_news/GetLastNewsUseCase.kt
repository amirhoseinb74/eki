package com.example.eki.feature_main.domain.use_case.last_news

import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.data.remote.ResponseHandler.Companion.responseHandlerFlowSingleData
import com.example.eki.feature_main.data.remote.dto.NewsReqDto
import com.example.eki.feature_main.data.remote.dto.NewsResDto
import com.example.eki.feature_main.domain.repository.NewsRepository
import com.example.eki.feature_main.domain.repository.RescuerRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetLastNewsUseCase @Inject constructor(
    private val newsRepository: NewsRepository,
    private val rescuerRepository: RescuerRepository,
) {
    operator fun invoke(newsReqDto: NewsReqDto): Flow<ResponseHandler<NewsResDto>> =
        responseHandlerFlowSingleData {
            newsRepository.getLastNews(newsReqDto, rescuerRepository.getToken())
        }
}