package com.example.eki.feature_main.data.remote.dto

data class LastVersionReqDto(
    val androidId: String,
    val androidVersion: String,
    val appType: String,
    val appVersion: String,
    val deviceCompany: String,
    val deviceModel: String,
)