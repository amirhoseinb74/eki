package com.example.eki.feature_main.domain.model

import com.example.eki.R
import com.example.eki.feature_rescuer.presentation.RescuerScreens

data class Feature(
    val titleId: Int,
    val painterId: Int,
    val isActive: Boolean = false,
    val currentlyRunning: Boolean = false,
    val contentDescription: String? = null,
    val route: String? = null,
) {
    companion object {
        fun getCurrentFeatures(
            isRescuerRunning: Boolean = false,
        ) = listOf(
            Feature(
                titleId = R.string.agency,
                painterId = R.drawable.ic_hand_shake,
                isActive = false,
                contentDescription = "Agency"
            ),
            Feature(
                titleId = R.string.expert,
                painterId = R.drawable.ic_transport,
                isActive = true,
                contentDescription = "Expert"
            ),
            Feature(
                titleId = R.string.rescuer,
                painterId = R.drawable.ic_technician,
                isActive = true,
                currentlyRunning = isRescuerRunning,
                contentDescription = "Rescuer",
                route = RescuerScreens.Rescuer.route
            ),
            Feature(
                titleId = R.string.auto_click,
                painterId = R.drawable.ic_finished_jobs,
                isActive = true,
                contentDescription = "Service On Site"
            ),
            Feature(
                titleId = R.string.organization,
                painterId = R.drawable.ic_car_repair,
                isActive = true,
                contentDescription = "Organization"
            ),
            Feature(
                titleId = R.string.subscription,
                painterId = R.drawable.ic_sell_subscribe,
                isActive = true,
                contentDescription = "Subscription"
            )
        )
    }
}
