package com.example.eki.feature_main.data.repository.fake

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.core.data.remote.Settings
import com.example.eki.feature_main.data.remote.dto.LastVersionReqDto
import com.example.eki.feature_main.data.remote.dto.LastVersionResDto
import com.example.eki.feature_main.domain.repository.AppVersionRepository

class FakeAppVersionRepo : AppVersionRepository {
    override suspend fun getLastVersion(appVersionReq: LastVersionReqDto): BaseResponse<LastVersionResDto> {
        return BaseResponse(
            data = listOf(
                LastVersionResDto(
                    "EKI",
                    false,
                    60,
                    1,
                    "versionName,versionNumber,versionURL",
                    "1402/03/23",
                    "version 49",
                    49,
                    "http://096440.com/upload/files/Mobile/EKIV49.apk"

                )
            ), settings = Settings("ok", "1")
        )
    }
}