package com.example.eki.feature_main.data.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.feature_main.data.remote.MainApi
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderReqDto
import com.example.eki.feature_main.data.remote.dto.LastWorkOrderResDto
import com.example.eki.feature_main.data.remote.dto.LoginReqDto
import com.example.eki.feature_main.domain.model.StoredLogin
import com.example.eki.feature_main.domain.repository.RescuerRepository
import com.google.gson.Gson
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject

class RescuerRepositoryImpl @Inject constructor(
    private val mainApi: MainApi, private val dataStorePreferences: DataStore<Preferences>,
) : RescuerRepository {

    private companion object {
        val KEY_TOKEN = stringPreferencesKey(name = "token")
        val KEY_USER_ID = stringPreferencesKey(name = "userId")
        val KEY_STORED_LOGIN = stringPreferencesKey(name = "stored_login")
        val KEY_ENCRYPTED_PERSONAL_ID = stringPreferencesKey("encrypted_personal_id")
    }

    override suspend fun login(loginReq: LoginReqDto) = mainApi.login(loginReq)

    override suspend fun saveToken(token: String) {
        dataStorePreferences.edit { preferences ->
            preferences[KEY_TOKEN] = token
        }
    }

    override suspend fun getToken(): String =
        getStringValueFromDataStore(KEY_TOKEN)

    override suspend fun saveUserId(userId: String) {
        dataStorePreferences.edit { preferences ->
            preferences[KEY_USER_ID] = userId
        }
    }

    override suspend fun getUserId(): String =
        getStringValueFromDataStore(KEY_USER_ID)


    override suspend fun saveStoredLogin(storedLogin: StoredLogin) {
        val storedLoginJson = Gson().toJson(storedLogin)
        dataStorePreferences.edit { preferences ->
            preferences[KEY_STORED_LOGIN] = storedLoginJson
        }
    }

    override suspend fun getStoredLogin(): StoredLogin? {
        val storedLoginJson = dataStorePreferences.data.catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            preferences[KEY_STORED_LOGIN]
        }.firstOrNull() ?: "{}"

        return Gson().fromJson(storedLoginJson, StoredLogin::class.java)
    }

    override suspend fun getLastWorkOrder(lastWorkOrderReqDto: LastWorkOrderReqDto):
            BaseResponse<LastWorkOrderResDto> =
        mainApi.getLastWorkorder(lastWorkOrderReqDto, getToken())

    private suspend fun getStringValueFromDataStore(key: Preferences.Key<String>) =
        dataStorePreferences.data.catch { exception ->
            if (exception is IOException) {
                emit(emptyPreferences())
            } else {
                throw exception
            }
        }.map { preferences ->
            preferences[key]
        }.firstOrNull().orEmpty()

    override suspend fun getEncryptedPersonalIdFromApi(personalId: String) =
        mainApi.getEncryptedPersonalId(personalId)

    override suspend fun saveEncryptedPersonalIdToDataStore(encryptedPersonalId: String?) {
        dataStorePreferences.edit { preferences ->
            preferences[KEY_ENCRYPTED_PERSONAL_ID] = encryptedPersonalId.orEmpty()
        }
    }

    override suspend fun getEncryptedPersonalIdFromDataStore() =
        getStringValueFromDataStore(KEY_ENCRYPTED_PERSONAL_ID)

}