package com.example.eki.feature_main.presentation.login

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_main.domain.use_case.login.LoginUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val loginUseCase: LoginUseCase,
) : ViewModel() {

    private val _isLoadingState = mutableStateOf(false)
    val isLoadingState: State<Boolean> = _isLoadingState

    private val _eventFlow = MutableSharedFlow<LoginUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private var loginJob: Job? = null

    fun onEvent(event: LoginEvent) {
        when (event) {
            is LoginEvent.LoginClicked -> {
                event.apply { login(username, password) }
            }
        }.exhaustive
    }

    sealed class LoginUIEvent {
        object NavigateToHome : LoginUIEvent()
        data class ShowError(val message: String) : LoginUIEvent()
    }

    private fun login(userName: String, password: String) {
        loginJob?.cancel()
        loginJob = loginUseCase(userName, password).onEach { result ->
            when (result) {
                is ResponseHandler.Success -> {
                    _isLoadingState.value = false
                    _eventFlow.emit(LoginUIEvent.NavigateToHome)
                }

                is ResponseHandler.Error -> {
                    _eventFlow.emit(
                        LoginUIEvent.ShowError(
                            result.message ?: ResponseHandler.generalErrorMessage
                        )
                    )
                    _isLoadingState.value = false
                }

                is ResponseHandler.Loading -> {
                    _isLoadingState.value = true
                }
            }.exhaustive
        }.launchIn(viewModelScope)
    }
}