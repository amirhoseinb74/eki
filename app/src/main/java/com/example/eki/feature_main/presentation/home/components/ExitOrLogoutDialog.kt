package com.example.eki.feature_main.presentation.home.components

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Close
import androidx.compose.material3.Card
import androidx.compose.material3.CardDefaults
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.window.Dialog
import com.example.eki.R
import com.example.eki.core.presentation.components.CustomButton
import com.example.eki.ui.spacing
import com.example.eki.ui.theme.EKITheme

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun ExitOrLogoutDialog(
    onExitClick: () -> Unit,
    onLogoutClick: () -> Unit,
    onDismiss: () -> Unit,
) {
    val dialogBgColor = MaterialTheme.colorScheme.surface
    val onDialogColor = MaterialTheme.colorScheme.onSurface

    Dialog(
        onDismissRequest = { onDismiss() }
    ) {
        Box(modifier = Modifier.fillMaxSize()) {
            Column(
                modifier = Modifier
                    .fillMaxWidth()
                    .wrapContentHeight()
                    .padding(MaterialTheme.spacing.medium)
                    .align(Alignment.BottomCenter)
            ) {
                Card(
                    modifier = Modifier
                        .fillMaxWidth(),
                    colors = CardDefaults.cardColors(
                        containerColor = dialogBgColor,
                        contentColor = onDialogColor
                    ),
                    elevation = CardDefaults.cardElevation(MaterialTheme.spacing.medium)
                ) {

                    // Close button in the top-left corner
                    Box(
                        modifier = Modifier
                            .fillMaxWidth()
                    ) {
                        IconButton(
                            modifier = Modifier.align(Alignment.TopEnd),
                            onClick = { onDismiss() }
                        ) {
                            Icon(
                                imageVector = Icons.Rounded.Close,
                                contentDescription = "Dismiss",
                                tint = MaterialTheme.colorScheme.primaryContainer
                            )
                        }
                        Column(
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(MaterialTheme.spacing.medium)
                        ) {
                            Text(
                                textAlign = TextAlign.Start,
                                text = stringResource(id = R.string.choose_option),
                                style = MaterialTheme.typography.bodyMedium,
                                modifier = Modifier
                                    .padding(bottom = MaterialTheme.spacing.medium)
                            )

                            CustomButton(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(bottom = MaterialTheme.spacing.small),
                                text = stringResource(id = R.string.logout),
                                onClick = {
                                    onLogoutClick()
                                    onDismiss()
                                }
                            )

                            CustomButton(
                                modifier = Modifier
                                    .fillMaxWidth()
                                    .padding(bottom = MaterialTheme.spacing.small),
                                text = stringResource(id = R.string.exit_app),
                                onClick = {
                                    onExitClick()
                                    onDismiss()
                                }
                            )
                        }
                    }
                }
            }
        }
    }
}

@Preview
@Composable
fun ExitOrLogoutDialogPreview() {
    EKITheme {
        ExitOrLogoutDialog(onExitClick = {}, onLogoutClick = {}, onDismiss = {})
    }
}