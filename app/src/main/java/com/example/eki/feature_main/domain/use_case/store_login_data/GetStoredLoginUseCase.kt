package com.example.eki.feature_main.domain.use_case.store_login_data

import com.example.eki.feature_main.domain.model.StoredLogin
import com.example.eki.feature_main.domain.repository.RescuerRepository
import javax.inject.Inject

class GetStoredLoginUseCase @Inject constructor(
    private val repository: RescuerRepository,
) {
    suspend operator fun invoke(): StoredLogin? {
        return repository.getStoredLogin()
    }
}