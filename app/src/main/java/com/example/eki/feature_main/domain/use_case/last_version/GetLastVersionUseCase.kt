package com.example.eki.feature_main.domain.use_case.last_version

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.provider.Settings
import com.example.eki.BuildConfig
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.data.remote.ResponseHandler.Companion.responseHandlerFlow
import com.example.eki.core.util.Constants
import com.example.eki.feature_main.data.remote.dto.LastVersionReqDto
import com.example.eki.feature_main.data.remote.dto.LastVersionResDto
import com.example.eki.feature_main.domain.repository.AppVersionRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetLastVersionUseCase @Inject constructor(
    private val repository: AppVersionRepository,
    private val context: Context,
) {
    @SuppressLint("HardwareIds")
    operator fun invoke(): Flow<ResponseHandler<List<LastVersionResDto>>> {

        val appVersionReq = LastVersionReqDto(
            appVersion = BuildConfig.VERSION_CODE.toString(),
            androidVersion = Build.VERSION.SDK_INT.toString(),
            deviceModel = Build.MODEL,
            deviceCompany = Build.MANUFACTURER,
            androidId = Settings.Secure.getString(
                context.applicationContext.contentResolver,
                Settings.Secure.ANDROID_ID
            ),
            appType = Constants.APP_TYPE
        )
        return responseHandlerFlow {
            repository.getLastVersion(appVersionReq)
        }
    }
}