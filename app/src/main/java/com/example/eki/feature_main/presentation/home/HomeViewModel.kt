package com.example.eki.feature_main.presentation.home

import android.app.Application
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.core.content.ContextCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.eki.R
import com.example.eki.core.data.remote.ResponseHandler
import com.example.eki.core.util.Strings
import com.example.eki.core.util.exhaustive
import com.example.eki.feature_main.data.remote.dto.NewsReqDto
import com.example.eki.feature_main.data.remote.dto.toNewsRes
import com.example.eki.feature_main.domain.use_case.last_news.GetLastNewsUseCase
import com.example.eki.feature_main.domain.use_case.logout.LogoutUseCase
import com.example.eki.feature_main.presentation.last_work_order_service.LastWorkOrderService
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Job
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getLastNewsUseCase: GetLastNewsUseCase,
    private val logoutUseCase: LogoutUseCase,
    private val application: Application,
) : AndroidViewModel(application) {

    private val _eventFlow = MutableSharedFlow<HomeUIEvent>()
    val eventFlow = _eventFlow.asSharedFlow()

    private val _newsState = mutableStateOf(NewsState())
    val newsState: State<NewsState> = _newsState

    private var getLastVersionJob: Job? = null

    private val _rescuerHasActiveWorkOrder = MutableLiveData(false)
    val rescuerHasActiveWorkOrder: LiveData<Boolean> = _rescuerHasActiveWorkOrder

    private val _lastRescuerWorkOrderStatus = mutableStateOf<String?>(null)
    val lastRescuerWorkOrderStatus: State<String?> = _lastRescuerWorkOrderStatus

    private val receiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            _rescuerHasActiveWorkOrder.value =
                intent?.getStringExtra(LastWorkOrderService.KEY_LAST_WORK_ORDER_STATUS)
                    ?.let { workOrderStatus ->
                        _lastRescuerWorkOrderStatus.value = workOrderStatus
                        when (WorkOrderStatus.valueOf(workOrderStatus)) {
                            WorkOrderStatus.NOT_STARTED,
                            WorkOrderStatus.STARTED,
                            WorkOrderStatus.SIGN,
                            WorkOrderStatus.PAYMENT,
                            -> true

                            else -> false
                        }
                    }
        }
    }

    init {
        val filter = IntentFilter(LastWorkOrderService.ACTION_BROADCAST_LAST_WORK_ORDER_DATA)
        ContextCompat.registerReceiver(
            application,
            receiver,
            filter,
            ContextCompat.RECEIVER_NOT_EXPORTED
        )
        getLastNews()
    }

    sealed class HomeUIEvent {
        object NavigateToLogin : HomeUIEvent()
        data class ShowError(val message: String) : HomeUIEvent()
    }

    fun onEvent(events: HomeEvents) {
        when (events) {
            is HomeEvents.Logout -> logout()
        }
    }

    private fun getLastNews() {
        val newsReq = NewsReqDto("Organization")
        getLastVersionJob?.cancel()
        getLastVersionJob = getLastNewsUseCase(newsReq).onEach { result ->
            when (result) {
                is ResponseHandler.Loading -> _newsState.value = NewsState(isLoading = true)
                is ResponseHandler.Success -> {
                    result.data?.toNewsRes()?.let { newsData ->
                        _newsState.value = NewsState(
                            title = newsData.title,
                            summary = newsData.summary
                        )
                    }
                }

                is ResponseHandler.Error -> {
                    _newsState.value = NewsState()
                    _eventFlow.emit(
                        HomeUIEvent.ShowError(
                            result.message ?: Strings.get(R.string.something_went_wrong)
                        )
                    )
                }
            }.exhaustive
        }.launchIn(viewModelScope)
    }

    private fun logout() {
        viewModelScope.launch {
            logoutUseCase()
            _eventFlow.emit(HomeUIEvent.NavigateToLogin)
        }
    }

    override fun onCleared() {
        getLastVersionJob?.cancel()
        application.unregisterReceiver(receiver)
        super.onCleared()
    }
}