package com.example.eki.feature_main.domain.repository

import com.example.eki.core.data.remote.BaseResponse
import com.example.eki.feature_main.data.remote.dto.LastVersionReqDto
import com.example.eki.feature_main.data.remote.dto.LastVersionResDto

interface AppVersionRepository {

    suspend fun getLastVersion(appVersionReq: LastVersionReqDto): BaseResponse<LastVersionResDto>
}