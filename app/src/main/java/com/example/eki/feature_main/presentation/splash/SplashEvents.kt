package com.example.eki.feature_main.presentation.splash

import androidx.lifecycle.LifecycleOwner

sealed class SplashEvents {
    object GetLastVersion : SplashEvents()
    data class Update(val lifecycleOwner: LifecycleOwner) : SplashEvents()
    object DismissUpdate : SplashEvents()
    data class Login(val userName: String, val password: String) : SplashEvents()
}
