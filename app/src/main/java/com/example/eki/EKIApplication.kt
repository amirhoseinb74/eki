package com.example.eki

import android.app.Application
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import com.example.eki.feature_main.presentation.last_work_order_service.LastWorkOrderService
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class EKIApplication : Application() {
    companion object {
        lateinit var instance: EKIApplication private set
    }

    override fun onCreate() {
        super.onCreate()
        instance = this

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                LastWorkOrderService.NOTIFICATION_CHANNEL_ID,
                LastWorkOrderService.NOTIFICATION_CHANNEL_NAME,
                NotificationManager.IMPORTANCE_HIGH
            )
            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

            notificationManager.createNotificationChannel(channel)
        }
    }
}